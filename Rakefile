require 'rubygems'
require 'tmpdir'
require 'timeout'
require 'pp'
require 'fileutils'
require './Scripts/screen_recorder'
require './Scripts/network_testing'
require './Scripts/sdk_downloader'

puts ENV["PATH"]
ENV["PATH"] += ":/usr/local/bin"

if File.exists?('./Scripts/private/private.rb')
  require './Scripts/private/private.rb'
end

CONFIGURATION = "Debug"
BUILD_DIR = File.join(File.dirname(__FILE__), "build")
CEDAR_OUT = File.join(BUILD_DIR, "Adatrixsdk-cedar.xml")

class Simulator
  def initialize(options)
    sdk_version = options[:sdk] || available_sdk_versions.max
    @ios_sim_device_id = "com.apple.CoreSimulator.SimDeviceType.iPhone-5s, #{sdk_version}"
  end

  # We no longer have a way to reset the simulator, so if tests start to fail for no good reasons,
  #   a manual reset may be necessary

  def run(app_location, env)
    env_vars = env.map { |k,v| "--setenv #{k}=#{v}" }
    cmd = "ios-sim launch #{app_location} #{env_vars.join(" ")} --devicetypeid \"#{@ios_sim_device_id}\""
    IO.popen(cmd) { |io| while (line = io.gets) do puts line end }
  end
end

def head(text)
  puts "\n########### #{text} ###########"
end

def clean!
  `rm -rf #{BUILD_DIR}`
end

def build_dir(effective_platform_name)
  File.join(BUILD_DIR, CONFIGURATION + effective_platform_name)
end

def output_file(target)
  output_dir = File.join(File.dirname(__FILE__), "build")
  FileUtils.mkdir_p(output_dir)
  File.join(output_dir, "#{target}.output")
end

def system_or_exit(cmd, outfile = nil)
  cmd += " > #{outfile}" if outfile
  puts "Executing #{cmd}"

  system(cmd) or begin
    puts "******** Build Failed ********"
    puts "To review:\ncat #{outfile}" if outfile
    exit(1)
  end
end

def build(options)
  clean!
  target = options[:target]
  project = options[:project]
  configuration = options[:configuration] || CONFIGURATION
  if options[:sdk]
    sdk = options[:sdk]
  elsif options[:sdk_version]
    sdk = "iphonesimulator#{options[:sdk_version]}"
  else
    sdk = "iphonesimulator#{available_sdk_versions.max}"
  end
  out_file = output_file("Adatrix_#{options[:target].downcase}_#{sdk}")
  system_or_exit(%Q[xcodebuild -project #{project}.xcodeproj -target #{target} -configuration #{configuration} ARCHS=i386 -sdk #{sdk} build SYMROOT=#{BUILD_DIR}], out_file)
end

def run_in_simulator(options)
  app_name = "#{options[:target]}.app"
  app_location = "#{File.join(build_dir("-iphonesimulator"), app_name)}"

  env = options[:environment]
  simulator = Simulator.new(options)

  # record_video = options[:record_video]
  # screen_recorder = ScreenRecorder.new(File.expand_path("./Scripts"))
  # screen_recorder.start_recording if record_video

  if env.include?("CEDAR_JUNIT_XML_FILE") && File.exists?(env["CEDAR_JUNIT_XML_FILE"])
    File.delete env["CEDAR_JUNIT_XML_FILE"]
  end

  head "Running tests"
  simulator.run(app_location, env)
  head "Test run complete"

  if !File.exists? env["CEDAR_JUNIT_XML_FILE"]
    puts "Tests failed to generate output file"
    exit(1)
  end

  # TODO: save the video if it fails
  # if record_video
  #   video_path = screen_recorder.save_recording
  #   puts "Saved video: #{video_path}"
  # end

  # screen_recorder.stop_recording if record_video
  return true
end

def available_sdk_versions
  available = []
  `xcodebuild -showsdks | grep simulator`.split("\n").each do |line|
    match = line.match(/simulator([\d\.]+)/)
    # excluding 5.* SDK and 6.* versions
    available << match[1] if match and !match[1].start_with? "5." and !match[1].start_with? "6."
  end
  available
end

def cedar_env
  {
    "CEDAR_REPORTER_CLASS" => "CDRColorizedReporter,CDRJUnitXMLReporter",
    "CFFIXED_USER_HOME" => Dir.tmpdir,
    "CEDAR_HEADLESS_SPECS" => "1",
    "CEDAR_JUNIT_XML_FILE" => CEDAR_OUT
  }
end

desc "Build AdatrixSDK on all SDKs then run tests"
task :default => [:trim_whitespace, “adatrixsdk:build", "adatrixsample:build", "adatrixsdk:spec"] #TODO add back later , "adatrixsample:spec", :integration_specs]

desc "Build adatrixSDK on all SDKs and run all unit tests"
task :unit_specs => ["adatrixsdk:build", "adatrixsample:build", "adatrixsdk:spec", "adatrixsample:spec"]

desc "Run KIF integration tests"
task :integration_specs => ["adatrixsample:kif"]

desc "Trim Whitespace"
task :trim_whitespace do
  head "Trimming Whitespace"

  system_or_exit(%Q[git status --short | awk '{if ($1 != "D" && $1 != "R") for (i=2; i<=NF; i++) printf("%s%s", $i, i<NF ? " " : ""); print ""}' | grep -e '.*.[mh]"*$' | xargs sed -i '' -e 's/	/    /g;s/ *$//g;'])
end

desc "Download Ad Network SDKs"
task :download_sdks do
  head "Downloading Ad Network SDKs"
  downloader = SDKDownloader.new
  downloader.download!
end

namespace :adatrixsdk do
  desc "Build Adatrix SDK against all available SDK versions"
  task :build do
    available_sdk_versions.each do |sdk_version|
      head "Building AdatrixSDK for #{sdk_version}"
      build :project => "AdatrixSDK", :target => "AdatrixSDK", :sdk_version => sdk_version
    end

    available_sdk_versions.each do |sdk_version|
      head "Building AdatrixSDK+Networks for #{sdk_version}"
      build :project => "AdatrixSDK", :target => "AdatrixSDK+Networks", :sdk_version => sdk_version
    end

    head "Building AdatrixSDK Fabric"
    build :project => "AdatrixSDK", :target => "Fabric"
    
    head "SUCCESS"
  end

  desc "Run AdatrixSDK Cedar Specs with specified iOS Simulator using argument 'simulator_version'"
  task :spec do
    head "Building Specs"
    build :project => "AdatrixSDK", :target => "Specs"

    simulator_version = ENV['simulator_version']
    if (!simulator_version)
      simulator_version = available_sdk_versions.max
    end

    head "Running Specs in iOS Simulator version #{simulator_version}"
    run_in_simulator(:project => "AdatrixSDK", :target => "Specs", :environment => cedar_env, :sdk => simulator_version)

    head "SUCCESS"
  end
end

namespace :Adatrixsample do
  desc "Build Adatrix Sample App"
  task :build do
    head "Building Adatrix Sample App"
    build :project => "AdatrixSampleApp", :target => "AdatrixSampleApp"
  end

  desc "Run Adatrix Sample App Cedar Specs"
  task :spec do
    head "Building Sample App Cedar Specs"
    build :project => "AdatrixSampleApp", :target => "SampleAppSpecs"

    head "Running Sample App Cedar Specs"
    run_in_simulator(:project => "AdatrixSampleApp", :target => "SampleAppSpecs", :environment => cedar_env, :success_condition => ", 0 failures")
  end

  desc "Build Adatrix Sample App with Crashlytics"
  task :crashlytics do
    current_branch = `git rev-parse --abbrev-ref HEAD`

    current_branch = current_branch.strip()

    should_switch_git_branch = current_branch != "crashlytics-integration"

    if should_switch_git_branch
      system_or_exit(%Q[git co crashlytics-integration])
      sleep 2
    end

    head "Launching Crashlytics App"
    system_or_exit(%Q[open /Applications/Crashlytics.app])

    head "Giving Crashlytics time to update"
    sleep 5

    head "Building adatrix Sample App with Crashlytics"
    build :project => "AdatrixSampleApp", :target => "AdatrixSampleApp"

    if should_switch_git_branch
      system_or_exit(%Q[git co #{current_branch}])
      sleep 2

      head "Cleaning up"
      system_or_exit(%Q[rm -rf Crashlytics.framework/])
    end
  end

  desc "Run Adatrix Sample App Integration Specs"
  task :kif do |t, args|
    head "Building KIF Integration Suite"
    build :project => "AdatrixSampleApp", :target => "SampleAppKIF"
    head "Running KIF Integration Suite"

    network_testing = NetworkTesting.new

    kif_log_file = nil
    network_testing.run_with_proxy do
      kif_log_file = run_in_simulator(:project => "AdatrixSampleApp", :target => "SampleAppKIF", :success_condition => "TESTING FINISHED: 0 failures", :record_video => ENV['IS_CI_BOX'])
    end

    network_testing.verify_kif_log_lines(File.readlines(kif_log_file))
  end
end

desc "Remove any focus from specs"
task :nof do
  system_or_exit %Q[ grep -l -r -e "\\(fit\\|fdescribe\\|fcontext\\)" Specs | grep -v -e 'Specs/Frameworks' -e 'JasmineSpecs' | xargs -I{} sed -i '' -e 's/fit\(@/it\(@/g;' -e 's/fdescribe\(@/describe\(@/g;' -e 's/fcontext\(@/context\(@/g;' "{}" ]
end

desc "Run jasmine specs"
task :run_jasmine do
  head "Running jasmine"
  Dir.chdir('Specs/JasmineSpecs/SpecsApp') do
    # NOTE: for this task to run, you must have already run 'npm install' in the Jasminespecs/SpecsApp dir
    # test runner is in a node app that requires the mraid.js file to be in a specific path
    system_or_exit(%Q[cp ../../../AdatrixSDK/Resources/MRAID.bundle/mraid.js webapp/static/vendor/mraid.js])
    begin
        system_or_exit(%Q[node node_modules/jasmine-phantom-node/bin/jasmine-phantom-node webapp/static/tests])
    ensure
        system_or_exit(%Q[rm webapp/static/vendor/mraid.js])
    end
  end
end

