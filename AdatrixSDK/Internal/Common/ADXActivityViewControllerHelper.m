//
//  ADXActivityViewControllerHelper.m
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import "ADXActivityViewControllerHelper.h"
#import "ADXInstanceProvider.h"


#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
/**
 * ADXActivityItemProviderWithSubject subclasses UIActivityItemProvider
 * to provide a subject for email activity types.
 */

@interface ADXActivityItemProviderWithSubject : UIActivityItemProvider

@property (nonatomic, readonly) NSString *subject;
@property (nonatomic, readonly) NSString *body;

- (instancetype)initWithSubject:(NSString *)subject body:(NSString *)body;

@end

@implementation ADXActivityItemProviderWithSubject

- (instancetype)initWithSubject:(NSString *)subject body:(NSString *)body
{
    self = [super initWithPlaceholderItem:body];
    if (self) {
        _subject = [subject copy];
        _body = [body copy];
    }
    return self;
}

- (id)item
{
    return self.body;
}

- (NSString *)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    return self.subject;
}

@end
#endif

@interface ADXActivityViewControllerHelper()

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
- (UIActivityViewController *)initializeActivityViewControllerWithSubject:(NSString *)subject body:(NSString *)body;
#endif

@end

@implementation ADXActivityViewControllerHelper

- (instancetype)initWithDelegate:(id<ADXActivityViewControllerHelperDelegate>)delegate
{
    self = [super init];
    if (self) {
        _delegate = delegate;
    }
    return self;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
- (UIActivityViewController *)initializeActivityViewControllerWithSubject:(NSString *)subject body:(NSString *)body
{
    if (NSClassFromString(@"UIActivityViewController") && NSClassFromString(@"UIActivityItemProvider")) {
        ADXActivityItemProviderWithSubject *activityItemProvider =
            [[ADXActivityItemProviderWithSubject alloc] initWithSubject:subject body:body];
        UIActivityViewController *activityViewController =
            [[UIActivityViewController alloc] initWithActivityItems:@[activityItemProvider] applicationActivities:nil];
        activityViewController.completionHandler = ^
            (NSString* activityType, BOOL completed) {
                if ([self.delegate respondsToSelector:@selector(activityViewControllerDidDismiss)]) {
                    [self.delegate activityViewControllerDidDismiss];
                }
            };
        return activityViewController;
    } else {
        return nil;
    }
}
#endif

- (BOOL)presentActivityViewControllerWithSubject:(NSString *)subject body:(NSString *)body
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
    if (NSClassFromString(@"UIActivityViewController")) {
        UIActivityViewController *activityViewController = [self initializeActivityViewControllerWithSubject:subject body:body];
        if (activityViewController) {
            if ([self.delegate respondsToSelector:@selector(activityViewControllerWillPresent)]) {
                [self.delegate activityViewControllerWillPresent];
            }
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
            UIUserInterfaceIdiom userInterfaceIdiom = [[[ADXCoreInstanceProvider sharedProvider]
                                                        sharedCurrentDevice] userInterfaceIdiom];
            // iPad must present as popover on iOS >= 8
            if (userInterfaceIdiom == UIUserInterfaceIdiomPad) {
                if ([activityViewController respondsToSelector:@selector(popoverPresentationController)]) {
                    activityViewController.popoverPresentationController.sourceView =
                        [self.delegate viewControllerForPresentingActivityViewController].view;
                }
            }
#endif
            UIViewController *viewController = [self.delegate viewControllerForPresentingActivityViewController];
            [viewController presentViewController:activityViewController
                                         animated:YES
                                       completion:nil];
            return YES;
        }
    }
#endif
    return NO;
}

@end
