//
//  ADXLastResortDelegate.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface ADXLastResortDelegate : NSObject
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
<SKStoreProductViewControllerDelegate>
#endif

+ (id)sharedDelegate;

@end
