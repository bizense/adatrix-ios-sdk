//
//  ADXLastResortDelegate.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXLastResortDelegate.h"
#import "ADXGlobal.h"
#import "UIViewController+ADXAdditions.h"

@class MFMailComposeViewController;

@implementation ADXLastResortDelegate

+ (id)sharedDelegate
{
    static ADXLastResortDelegate *lastResortDelegate;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        lastResortDelegate = [[self alloc] init];
    });
    return lastResortDelegate;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(NSInteger)result error:(NSError*)error
{
    [(UIViewController *)controller adx_dismissModalViewControllerAnimated:ADX_ANIMATED];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController adx_dismissModalViewControllerAnimated:ADX_ANIMATED];
}
#endif

@end
