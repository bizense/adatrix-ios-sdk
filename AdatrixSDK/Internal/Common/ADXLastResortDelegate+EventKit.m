//
//  ADXLastResortDelegate+EventKit.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXLastResortDelegate+EventKit.h"
#import "ADXGlobal.h"
#import "UIViewController+ADXAdditions.h"


@implementation ADXLastResortDelegate (EventKit)

- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    [controller adx_dismissModalViewControllerAnimated:ADX_ANIMATED];
}

@end
