//
//  ADXFacebookAttributionIdProvider.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXKeywordProvider.h"

/*
 * This class enables the Adatrix SDK to deliver targeted ads from Facebook via Adatrix Marketplace
 * (Adatrix's real-time bidding ad exchange) as part of a test program. This class sends an identifier
 * to Facebook so Facebook can select the ad Adatrix will serve in your app through Adatrix Marketplace.
 * If this class is removed from the SDK, your application will not receive targeted ads from
 * Facebook.
 */

@interface ADXFacebookKeywordProvider : NSObject <ADXKeywordProvider>

@end
