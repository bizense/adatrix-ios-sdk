//
//  ADXAdBrowserController.h
//  Adatrix
//
//  Created by Nafis Jamal on 1/19/11.
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef CF_RETURNS_RETAINED
#if __has_feature(attribute_cf_returns_retained)
#define CF_RETURNS_RETAINED __attribute__((cf_returns_retained))
#else
#define CF_RETURNS_RETAINED
#endif
#endif

@protocol ADXAdBrowserControllerDelegate;

@interface ADXAdBrowserController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *backButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *forwardButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *refreshButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *safariButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *spinnerItem;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@property (nonatomic, weak) id<ADXAdBrowserControllerDelegate> delegate;
@property (nonatomic, copy) NSURL *URL;

- (id)initWithURL:(NSURL *)URL HTMLString:(NSString *)HTMLString delegate:(id<ADXAdBrowserControllerDelegate>)delegate;

// Navigation methods.
- (IBAction)back;
- (IBAction)forward;
- (IBAction)refresh;
- (IBAction)safari;
- (IBAction)done;

// Drawing methods.
- (CGContextRef)createContext CF_RETURNS_RETAINED;
- (UIImage *)backArrowImage;
- (UIImage *)forwardArrowImage;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ADXAdBrowserControllerDelegate <NSObject>

- (void)dismissBrowserController:(ADXAdBrowserController *)browserController animated:(BOOL)animated;

@end
