//
//  ADXClosableView.h
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    ADXClosableViewCloseButtonLocationTopRight,
    ADXClosableViewCloseButtonLocationTopLeft,
    ADXClosableViewCloseButtonLocationTopCenter,
    ADXClosableViewCloseButtonLocationBottomRight,
    ADXClosableViewCloseButtonLocationBottomLeft,
    ADXClosableViewCloseButtonLocationBottomCenter,
    ADXClosableViewCloseButtonLocationCenter
};
typedef NSInteger ADXClosableViewCloseButtonLocation;

enum {
    ADXClosableViewCloseButtonTypeNone,
    ADXClosableViewCloseButtonTypeTappableWithoutImage,
    ADXClosableViewCloseButtonTypeTappableWithImage,
};
typedef NSInteger ADXClosableViewCloseButtonType;

CGRect ADXClosableViewCustomCloseButtonFrame(CGSize size, ADXClosableViewCloseButtonLocation closeButtonLocation);

@protocol ADXClosableViewDelegate;

/**
 * `ADXClosableView` is composed of a content view and a close button. The close button can
 * be positioned at any corner, the center of top and bottom edges, and the center of the view.
 * The close button can either be a button that is tappable with image, tappable without an image,
 * and completely disabled altogether. Finally, `ADXClosableView` keeps track as to whether or not
 * it has been tapped.
 */
@interface ADXClosableView : UIView

@property (nonatomic, weak) id<ADXClosableViewDelegate> delegate;
@property (nonatomic, assign) ADXClosableViewCloseButtonType closeButtonType;
@property (nonatomic, assign) ADXClosableViewCloseButtonLocation closeButtonLocation;
@property (nonatomic, readonly) BOOL wasTapped;

- (instancetype)initWithFrame:(CGRect)frame closeButtonType:(ADXClosableViewCloseButtonType)closeButtonType;

@end

/**
 * `ADXClosableViewDelegate` allows an object that implements `ADXClosableViewDelegate` to
 * be notified when the close button of the `ADXClosableView` has been tapped.
 */
@protocol ADXClosableViewDelegate <NSObject>

- (void)closeButtonPressed:(ADXClosableView *)closableView;

@optional

- (void)closableView:(ADXClosableView *)closableView didMoveToWindow:(UIWindow *)window;

@end
