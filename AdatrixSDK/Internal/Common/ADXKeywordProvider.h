//
//  ADXKeywordProvider.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ADXKeywordProvider <NSObject>

+ (NSString *)keyword;

@end
