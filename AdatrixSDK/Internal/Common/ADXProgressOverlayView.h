//
//  ADXProgressOverlayView.h
//  Adatrix
//
//  Created by Andrew He on 7/18/12.
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADXProgressOverlayViewDelegate;

@interface ADXProgressOverlayView : UIView {
    id<ADXProgressOverlayViewDelegate> __weak _delegate;
    UIView *_outerContainer;
    UIView *_innerContainer;
    UIActivityIndicatorView *_activityIndicator;
    UIButton *_closeButton;
    CGPoint _closeButtonPortraitCenter;
}

@property (nonatomic, weak) id<ADXProgressOverlayViewDelegate> delegate;
@property (nonatomic, strong) UIButton *closeButton;

- (id)initWithDelegate:(id<ADXProgressOverlayViewDelegate>)delegate;
- (void)show;
- (void)hide;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ADXProgressOverlayViewDelegate <NSObject>

@optional
- (void)overlayCancelButtonPressed;
- (void)overlayDidAppear;

@end
