//
//  ADXAPIEndpoints.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ADATRIX_BASE_HOSTNAME                 @"adx1.exchange.adatrix.com"
#define ADATRIX_BASE_HOSTNAME_FOR_TESTING     @"adx1.exchange.adatrix.com"

#define ADATRIX_API_PATH_AD_REQUEST           @"/get.ad"
#define ADATRIX_API_PATH_CONVERSION           @"/m/open"
#define ADATRIX_API_PATH_NATIVE_POSITIONING   @"/m/pos"
#define ADATRIX_API_PATH_SESSION              @"/m/open"

@interface ADXAPIEndpoints : NSObject

+ (void)setUsesHTTPS:(BOOL)usesHTTPS;
+ (NSString *)baseURLStringWithPath:(NSString *)path testing:(BOOL)testing;

@end
