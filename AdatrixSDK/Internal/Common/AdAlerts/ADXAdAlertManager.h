//
//  ADXAdAlertManager.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ADXGlobal.h"

@class CLLocation;
@protocol ADXAdAlertManagerDelegate;

@class ADXAdConfiguration;

@interface ADXAdAlertManager : NSObject <ADXAdAlertManagerProtocol>

@end

@protocol ADXAdAlertManagerDelegate <NSObject>

@required
- (UIViewController *)viewControllerForPresentingMailVC;
- (void)adAlertManagerDidTriggerAlert:(ADXAdAlertManager *)manager;

@optional
- (void)adAlertManagerDidProcessAlert:(ADXAdAlertManager *)manager;

@end