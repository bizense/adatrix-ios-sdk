//
//  ADXAdAlertGestureRecognizer.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSInteger const kADXAdAlertGestureMaxAllowedYAxisMovement;

typedef enum
{
    ADXAdAlertGestureRecognizerState_ZigRight1,
    ADXAdAlertGestureRecognizerState_ZagLeft2,
    ADXAdAlertGestureRecognizerState_Recognized
} ADXAdAlertGestureRecognizerState;

@interface ADXAdAlertGestureRecognizer : UIGestureRecognizer

// default is 4
@property (nonatomic, assign) NSInteger numZigZagsForRecognition;

// default is 100
@property (nonatomic, assign) CGFloat minTrackedDistanceForZigZag;

@property (nonatomic, readonly) ADXAdAlertGestureRecognizerState currentAlertGestureState;
@property (nonatomic, readonly) CGPoint inflectionPoint;
@property (nonatomic, readonly) BOOL thresholdReached;
@property (nonatomic, readonly) NSInteger curNumZigZags;

@end
