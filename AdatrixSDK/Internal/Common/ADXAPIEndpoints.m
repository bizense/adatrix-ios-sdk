//
//  ADXAPIEndpoints.m
//  Adatrix
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import "ADXAPIEndpoints.h"
#import "ADXConstants.h"

@implementation ADXAPIEndpoints

static BOOL sUsesHTTPS = false;

+ (void)setUsesHTTPS:(BOOL)usesHTTPS
{
    sUsesHTTPS = usesHTTPS;
}

+ (NSString *)APIScheme
{
    return sUsesHTTPS ? @"https" : @"http";
}

+ (NSString *)baseURLStringWithPath:(NSString *)path testing:(BOOL)testing
{
    return [NSString stringWithFormat:@"%@://%@%@",
            [[self class] APIScheme],
            testing ? ADATRIX_BASE_HOSTNAME_FOR_TESTING : ADATRIX_BASE_HOSTNAME,
            path];
}

@end
