//
//  ADXClosableView.m
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXClosableView.h"
#import "ADXInstanceProvider.h"
#import "ADXUserInteractionGestureRecognizer.h"

static CGFloat kCloseRegionWidth = 50.0f;
static CGFloat kCloseRegionHeight = 50.0f;
static NSString *const kExpandableCloseButtonImageName = @"ADXCloseButtonX.png";

CGRect ADXClosableViewCustomCloseButtonFrame(CGSize size, ADXClosableViewCloseButtonLocation closeButtonLocation)
{
    CGFloat width = size.width;
    CGFloat height = size.height;
    CGRect closeButtonFrame = CGRectMake(0.0f, 0.0f, kCloseRegionWidth, kCloseRegionHeight);

    switch (closeButtonLocation) {
        case ADXClosableViewCloseButtonLocationTopRight:
            closeButtonFrame.origin = CGPointMake(width-kCloseRegionWidth, 0.0f);
            break;
        case ADXClosableViewCloseButtonLocationTopLeft:
            closeButtonFrame.origin = CGPointMake(0.0f, 0.0f);
            break;
        case ADXClosableViewCloseButtonLocationTopCenter:
            closeButtonFrame.origin = CGPointMake((width-kCloseRegionWidth) / 2.0f, 0.0f);
            break;
        case ADXClosableViewCloseButtonLocationBottomRight:
            closeButtonFrame.origin = CGPointMake(width-kCloseRegionWidth, height-kCloseRegionHeight);
            break;
        case ADXClosableViewCloseButtonLocationBottomLeft:
            closeButtonFrame.origin = CGPointMake(0.0f, height-kCloseRegionHeight);
            break;
        case ADXClosableViewCloseButtonLocationBottomCenter:
            closeButtonFrame.origin = CGPointMake((width-kCloseRegionWidth) / 2.0f, height-kCloseRegionHeight);
            break;
        case ADXClosableViewCloseButtonLocationCenter:
            closeButtonFrame.origin = CGPointMake((width-kCloseRegionWidth) / 2.0f, (height-kCloseRegionHeight) / 2.0f);
            break;
        default:
            closeButtonFrame.origin = CGPointMake(width-kCloseRegionWidth, 0.0f);
            break;
    }

    return closeButtonFrame;
}

@interface ADXClosableView () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIImage *closeButtonImage;
@property (nonatomic, strong) ADXUserInteractionGestureRecognizer *userInteractionRecognizer;
@property (nonatomic, assign) BOOL wasTapped;

@end

@implementation ADXClosableView

- (instancetype)initWithFrame:(CGRect)frame closeButtonType:(ADXClosableViewCloseButtonType)closeButtonType
{
    self = [super initWithFrame:frame];

    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;

        _closeButtonLocation = ADXClosableViewCloseButtonLocationTopRight;

        _userInteractionRecognizer = [[ADXUserInteractionGestureRecognizer alloc] initWithTarget:self action:@selector(handleInteraction:)];
        _userInteractionRecognizer.cancelsTouchesInView = NO;
        [self addGestureRecognizer:_userInteractionRecognizer];
        _userInteractionRecognizer.delegate = self;

        _closeButtonImage = [UIImage imageNamed:ADXResourcePathForResource(kExpandableCloseButtonImageName)];

        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeButton.backgroundColor = [UIColor clearColor];
        _closeButton.accessibilityLabel = @"Close Interstitial Ad";

        [_closeButton addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];

        [self setCloseButtonType:closeButtonType];

        [self addSubview:_closeButton];
    }

    return self;
}

- (void)dealloc
{
    _userInteractionRecognizer.delegate = nil;
    [self.userInteractionRecognizer removeTarget:self action:nil];
}

- (void)layoutSubviews
{
    self.closeButton.frame = ADXClosableViewCustomCloseButtonFrame(self.bounds.size, self.closeButtonLocation);
    [self bringSubviewToFront:self.closeButton];
}

- (void)didMoveToWindow
{
    if ([self.delegate respondsToSelector:@selector(closableView:didMoveToWindow:)]) {
        [self.delegate closableView:self didMoveToWindow:self.window];
    }
}

- (void)setCloseButtonType:(ADXClosableViewCloseButtonType)closeButtonType
{
    _closeButtonType = closeButtonType;

    switch (closeButtonType) {
        case ADXClosableViewCloseButtonTypeNone:
            self.closeButton.hidden = YES;
            break;
        case ADXClosableViewCloseButtonTypeTappableWithoutImage:
            self.closeButton.hidden = NO;
            [self.closeButton setImage:nil forState:UIControlStateNormal];
            break;
        case ADXClosableViewCloseButtonTypeTappableWithImage:
            self.closeButton.hidden = NO;
            [self.closeButton setImage:self.closeButtonImage forState:UIControlStateNormal];
            break;
        default:
            self.closeButton.hidden = NO;
            [self.closeButton setImage:self.closeButtonImage forState:UIControlStateNormal];
            break;
    }
}

- (void)setCloseButtonLocation:(ADXClosableViewCloseButtonLocation)closeButtonLocation
{
    _closeButtonLocation = closeButtonLocation;
    [self setNeedsLayout];
}

- (void)handleInteraction:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        self.wasTapped = YES;
    }
}

#pragma mark - <UIGestureRecognizerDelegate>

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer;
{
    return YES;
}

#pragma mark - <UIButton>

- (void)closeButtonPressed
{
    [self.delegate closeButtonPressed:self];
}

@end
