//
//  ADXAdDestinationDisplayAgent.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXActivityViewControllerHelper+TweetShare.h"
#import "ADXURLResolver.h"
#import "ADXProgressOverlayView.h"
#import "ADXAdBrowserController.h"
#import "ADXStoreKitProvider.h"

@protocol ADXAdDestinationDisplayAgentDelegate;

@interface ADXAdDestinationDisplayAgent : NSObject <ADXURLResolverDelegate,
                                                   ADXProgressOverlayViewDelegate,
                                                   ADXAdBrowserControllerDelegate,
                                                   ADXSKStoreProductViewControllerDelegate,
                                                   ADXActivityViewControllerHelperDelegate>

@property (nonatomic, weak) id<ADXAdDestinationDisplayAgentDelegate> delegate;

+ (ADXAdDestinationDisplayAgent *)agentWithDelegate:(id<ADXAdDestinationDisplayAgentDelegate>)delegate;
- (void)displayDestinationForURL:(NSURL *)URL;
- (void)cancel;

@end

@protocol ADXAdDestinationDisplayAgentDelegate <NSObject>

- (UIViewController *)viewControllerForPresentingModalView;
- (void)displayAgentWillPresentModal;
- (void)displayAgentWillLeaveApplication;
- (void)displayAgentDidDismissModal;

@end
