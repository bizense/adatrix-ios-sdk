//
//  ADXURLResolver.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXGlobal.h"

@protocol ADXURLResolverDelegate;

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_5_0
@interface ADXURLResolver : NSObject <NSURLConnectionDataDelegate>
#else
@interface ADXURLResolver : NSObject
#endif

@property (nonatomic, weak) id<ADXURLResolverDelegate> delegate;

+ (ADXURLResolver *)resolver;
- (void)startResolvingWithURL:(NSURL *)URL delegate:(id<ADXURLResolverDelegate>)delegate;
- (void)cancel;

@end

@protocol ADXURLResolverDelegate <NSObject>

- (void)showWebViewWithHTMLString:(NSString *)HTMLString baseURL:(NSURL *)URL;
- (void)showStoreKitProductWithParameter:(NSString *)parameter fallbackURL:(NSURL *)URL;
- (void)openURLInApplication:(NSURL *)URL;
- (void)failedToResolveURLWithError:(NSError *)error;
- (BOOL)openShareURL:(NSURL*)URL;

@end
