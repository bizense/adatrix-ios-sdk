//
//  ADXAdDestinationDisplayAgent.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXAdDestinationDisplayAgent.h"
#import "UIViewController+ADXAdditions.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXLastResortDelegate.h"
#import "ADXLogging.h"
#import "NSURL+ADXAdditions.h"

@interface ADXAdDestinationDisplayAgent ()

@property (nonatomic, strong) ADXURLResolver *resolver;
@property (nonatomic, strong) ADXProgressOverlayView *overlayView;
@property (nonatomic, assign) BOOL isLoadingDestination;

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
@property (nonatomic, strong) SKStoreProductViewController *storeKitController;
#endif

@property (nonatomic, strong) ADXAdBrowserController *browserController;
@property (nonatomic, strong) ADXTelephoneConfirmationController *telephoneConfirmationController;
@property (nonatomic, strong) ADXActivityViewControllerHelper *activityViewControllerHelper;

- (void)presentStoreKitControllerWithItemIdentifier:(NSString *)identifier fallbackURL:(NSURL *)URL;
- (void)hideOverlay;
- (void)hideModalAndNotifyDelegate;
- (void)dismissAllModalContent;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation ADXAdDestinationDisplayAgent

@synthesize delegate = _delegate;
@synthesize resolver = _resolver;
@synthesize isLoadingDestination = _isLoadingDestination;

+ (ADXAdDestinationDisplayAgent *)agentWithDelegate:(id<ADXAdDestinationDisplayAgentDelegate>)delegate
{
    ADXAdDestinationDisplayAgent *agent = [[ADXAdDestinationDisplayAgent alloc] init];
    agent.delegate = delegate;
    agent.resolver = [[ADXCoreInstanceProvider sharedProvider] buildADXURLResolver];
    agent.overlayView = [[ADXProgressOverlayView alloc] initWithDelegate:agent];
    agent.activityViewControllerHelper = [[ADXActivityViewControllerHelper alloc] initWithDelegate:agent];
    return agent;
}

- (void)dealloc
{
    [self dismissAllModalContent];

    self.overlayView.delegate = nil;
    self.resolver.delegate = nil;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
    // XXX: If this display agent is deallocated while a StoreKit controller is still on-screen,
    // nil-ing out the controller's delegate would leave us with no way to dismiss the controller
    // in the future. Therefore, we change the controller's delegate to a singleton object which
    // implements SKStoreProductViewControllerDelegate and is always around.
    self.storeKitController.delegate = [ADXLastResortDelegate sharedDelegate];
#endif
    self.browserController.delegate = nil;

}

- (void)dismissAllModalContent
{
    [self.overlayView hide];
}

- (void)displayDestinationForURL:(NSURL *)URL
{
    if (self.isLoadingDestination) return;
    self.isLoadingDestination = YES;

    [self.delegate displayAgentWillPresentModal];
    [self.overlayView show];

    [self.resolver startResolvingWithURL:URL delegate:self];
}

- (void)cancel
{
    if (self.isLoadingDestination) {
        self.isLoadingDestination = NO;
        [self.resolver cancel];
        [self hideOverlay];
        [self.delegate displayAgentDidDismissModal];
    }
}

#pragma mark - <ADXURLResolverDelegate>

- (void)showWebViewWithHTMLString:(NSString *)HTMLString baseURL:(NSURL *)URL
{
    [self hideOverlay];

    self.browserController = [[ADXAdBrowserController alloc] initWithURL:URL
                                                              HTMLString:HTMLString
                                                                delegate:self];
    self.browserController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [[self.delegate viewControllerForPresentingModalView] adx_presentModalViewController:self.browserController
                                                                               animated:ADX_ANIMATED];
}

- (void)showStoreKitProductWithParameter:(NSString *)parameter fallbackURL:(NSURL *)URL
{
    if ([ADXStoreKitProvider deviceHasStoreKit]) {
        [self presentStoreKitControllerWithItemIdentifier:parameter fallbackURL:URL];
    } else {
        [self openURLInApplication:URL];
    }
}

- (void)openURLInApplication:(NSURL *)URL
{
    [self hideOverlay];

    if ([URL adx_hasTelephoneScheme] || [URL adx_hasTelephonePromptScheme]) {
        [self interceptTelephoneURL:URL];
    } else {
        [self.delegate displayAgentWillLeaveApplication];
        [[UIApplication sharedApplication] openURL:URL];
        self.isLoadingDestination = NO;
    }
}

- (BOOL)openShareURL:(NSURL *)URL
{
    ADXLogDebug(@"ADXAdDestinationDisplayAgent - loading Share URL: %@", URL);
    ADXAdatrixShareHostCommand command = [URL adx_AdatrixShareHostCommand];
    switch (command) {
        case ADXAdatrixShareHostCommandTweet:
            return [self.activityViewControllerHelper presentActivityViewControllerWithTweetShareURL:URL];
        default:
            ADXLogWarn(@"ADXAdDestinationDisplayAgent - unsupported Share URL: %@", [URL absoluteString]);
            return NO;
    }
}

- (void)interceptTelephoneURL:(NSURL *)URL
{
    __weak ADXAdDestinationDisplayAgent *weakSelf = self;
    self.telephoneConfirmationController = [[ADXTelephoneConfirmationController alloc] initWithURL:URL clickHandler:^(NSURL *targetTelephoneURL, BOOL confirmed) {
        ADXAdDestinationDisplayAgent *strongSelf = weakSelf;
        if (strongSelf) {
            if (confirmed) {
                [strongSelf.delegate displayAgentWillLeaveApplication];
                [[UIApplication sharedApplication] openURL:targetTelephoneURL];
            }
            strongSelf.isLoadingDestination = NO;
            [strongSelf.delegate displayAgentDidDismissModal];
        }
    }];

    [self.telephoneConfirmationController show];
}

- (void)failedToResolveURLWithError:(NSError *)error
{
    self.isLoadingDestination = NO;
    [self hideOverlay];
    [self.delegate displayAgentDidDismissModal];
}

- (void)presentStoreKitControllerWithItemIdentifier:(NSString *)identifier fallbackURL:(NSURL *)URL
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
    self.storeKitController = [ADXStoreKitProvider buildController];
    self.storeKitController.delegate = self;

    NSDictionary *parameters = [NSDictionary dictionaryWithObject:identifier
                                                           forKey:SKStoreProductParameterITunesItemIdentifier];
    [self.storeKitController loadProductWithParameters:parameters completionBlock:nil];

    [self hideOverlay];
    [[self.delegate viewControllerForPresentingModalView] adx_presentModalViewController:self.storeKitController
                                                                               animated:ADX_ANIMATED];
#endif
}

#pragma mark - <ADXSKStoreProductViewControllerDelegate>
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    self.isLoadingDestination = NO;
    [self hideModalAndNotifyDelegate];
}

#pragma mark - <ADXAdBrowserControllerDelegate>
- (void)dismissBrowserController:(ADXAdBrowserController *)browserController animated:(BOOL)animated
{
    self.isLoadingDestination = NO;
    [self hideModalAndNotifyDelegate];
}

#pragma mark - <ADXProgressOverlayViewDelegate>
- (void)overlayCancelButtonPressed
{
    [self cancel];
}

#pragma mark - Convenience Methods
- (void)hideModalAndNotifyDelegate
{
    [[self.delegate viewControllerForPresentingModalView] adx_dismissModalViewControllerAnimated:ADX_ANIMATED];
    [self.delegate displayAgentDidDismissModal];
}

- (void)hideOverlay
{
    [self.overlayView hide];
}

#pragma mark <ADXActivityViewControllerHelperDelegate>

- (UIViewController *)viewControllerForPresentingActivityViewController
{
    return self.delegate.viewControllerForPresentingModalView;
}

- (void)activityViewControllerWillPresent
{
    [self hideOverlay];
    self.isLoadingDestination = NO;
    [self.delegate displayAgentWillPresentModal];
}

- (void)activityViewControllerDidDismiss
{
    [self.delegate displayAgentDidDismissModal];
}

@end
