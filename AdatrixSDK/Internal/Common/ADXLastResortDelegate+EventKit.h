//
//  ADXLastResortDelegate+EventKit.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXLastResortDelegate.h"
#import <EventKitUI/EventKitUI.h>

@interface ADXLastResortDelegate (EventKit) <EKEventEditViewDelegate>

@end
