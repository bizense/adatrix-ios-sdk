//
//  ADXAdServerCommunicator.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ADXAdConfiguration.h"
#import "ADXGlobal.h"

@protocol ADXAdServerCommunicatorDelegate;

////////////////////////////////////////////////////////////////////////////////////////////////////

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_5_0
@interface ADXAdServerCommunicator : NSObject <NSURLConnectionDataDelegate>
#else
@interface ADXAdServerCommunicator : NSObject
#endif

@property (nonatomic, weak) id<ADXAdServerCommunicatorDelegate> delegate;
@property (nonatomic, assign, readonly) BOOL loading;

- (id)initWithDelegate:(id<ADXAdServerCommunicatorDelegate>)delegate;

- (void)loadURL:(NSURL *)URL;
- (void)cancel;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ADXAdServerCommunicatorDelegate <NSObject>

@required
- (void)communicatorDidReceiveAdConfiguration:(ADXAdConfiguration *)configuration;
- (void)communicatorDidFailWithError:(NSError *)error;

@end
