//
//  ADXUserInteractionGestureRecognizer.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXUserInteractionGestureRecognizer : UIGestureRecognizer

@end
