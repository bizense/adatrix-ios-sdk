//
//  ADXAdRequestError.h
//  Adatrix
//
//  Copyright (c) 2012 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kADXErrorDomain;

typedef enum {
    ADXErrorUnknown = -1,
    ADXErrorNoInventory = 0,
    ADXErrorAdUnitWarmingUp = 1,
    ADXErrorNetworkTimedOut = 4,
    ADXErrorServerError = 8,
    ADXErrorAdapterNotFound = 16,
    ADXErrorAdapterInvalid = 17,
    ADXErrorAdapterHasNoInventory = 18
} ADXErrorCode;

@interface ADXError : NSError

+ (ADXError *)errorWithCode:(ADXErrorCode)code;

@end
