//
//  ADXAdRequestError.m
//  Adatrix
//
//  Copyright (c) 2012 Adatrix. All rights reserved.
//

#import "ADXError.h"

NSString * const kADXErrorDomain = @"com.adatrix.iossdk";

@implementation ADXError

+ (ADXError *)errorWithCode:(ADXErrorCode)code
{
    return [self errorWithDomain:kADXErrorDomain code:code userInfo:nil];
}

@end
