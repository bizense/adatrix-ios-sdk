//
//  NSJSONSerialization+ADXAdditions.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "NSJSONSerialization+ADXAdditions.h"

@interface NSMutableDictionary (RemoveNullObjects)

- (void)adx_removeNullsRecursively;

@end

@interface NSMutableArray (RemoveNullObjects)

- (void)adx_removeNullsRecursively;

@end

@implementation NSJSONSerialization (ADXAdditions)

+ (id)adx_JSONObjectWithData:(NSData *)data options:(NSJSONReadingOptions)opt clearNullObjects:(BOOL)clearNulls error:(NSError **)error
{
    if (clearNulls) {
        opt |= NSJSONReadingMutableContainers;
    }

    id JSONObject = [NSJSONSerialization JSONObjectWithData:data options:opt error:error];

    if (error || !clearNulls) {
        return JSONObject;
    }

    [JSONObject adx_removeNullsRecursively];

    return JSONObject;
}

@end

@implementation NSMutableDictionary (RemovingNulls)

-(void)adx_removeNullsRecursively
{
    // First, filter out directly stored nulls
    NSMutableArray *nullKeys = [NSMutableArray array];
    NSMutableArray *arrayKeys = [NSMutableArray array];
    NSMutableArray *dictionaryKeys = [NSMutableArray array];

    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
         if ([obj isEqual:[NSNull null]]) {
             [nullKeys addObject:key];
         } else if ([obj isKindOfClass:[NSDictionary  class]]) {
             [dictionaryKeys addObject:key];
         } else if ([obj isKindOfClass:[NSArray class]]) {
             [arrayKeys addObject:key];
         }
     }];

    // Remove all the nulls
    [self removeObjectsForKeys:nullKeys];

    // Cascade down the dictionaries
    for (id dictionaryKey in dictionaryKeys) {
        NSMutableDictionary *dictionary = [self objectForKey:dictionaryKey];
        [dictionary adx_removeNullsRecursively];
    }

    // Recursively remove nulls from arrays
    for (id arrayKey in arrayKeys) {
        NSMutableArray *array = [self objectForKey:arrayKey];
        [array adx_removeNullsRecursively];
    }
}

@end

@implementation NSMutableArray (RemovingNulls)

-(void)adx_removeNullsRecursively
{
    [self removeObjectIdenticalTo:[NSNull null]];

    for (id object in self) {
        if ([object respondsToSelector:@selector(adx_removeNullsRecursively)]) {
            [(NSMutableDictionary *)object adx_removeNullsRecursively];
        }
    }
}

@end
