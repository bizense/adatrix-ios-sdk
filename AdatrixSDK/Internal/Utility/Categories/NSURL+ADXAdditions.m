//
//  NSURL+ADXAdditions.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "NSURL+ADXAdditions.h"

static NSString * const kTelephoneScheme = @"tel";
static NSString * const kTelephonePromptScheme = @"telprompt";

// Share Constants
static NSString * const kAdatrixShareScheme = @"adatrixshare";
static NSString * const kAdatrixShareTweetHost = @"tweet";

// Commands Constants
static NSString * const kAdatrixURLScheme = @"adatrix";
static NSString * const kAdatrixCloseHost = @"close";
static NSString * const kAdatrixFinishLoadHost = @"finishLoad";
static NSString * const kAdatrixFailLoadHost = @"failLoad";
static NSString * const kAdatrixPrecacheCompleteHost = @"precacheComplete";

@implementation NSURL (ADXAdditions)

- (NSDictionary *)adx_queryAsDictionary
{
    NSMutableDictionary *queryDict = [NSMutableDictionary dictionary];
    NSArray *queryElements = [self.query componentsSeparatedByString:@"&"];
    for (NSString *element in queryElements) {
        NSArray *keyVal = [element componentsSeparatedByString:@"="];
        if (keyVal.count >= 2) {
            NSString *key = [keyVal objectAtIndex:0];
            NSString *value = [keyVal objectAtIndex:1];
            [queryDict setObject:[value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                          forKey:key];
        }
    }
    return queryDict;
}

- (BOOL)adx_hasTelephoneScheme
{
    return [[[self scheme] lowercaseString] isEqualToString:kTelephoneScheme];
}

- (BOOL)adx_hasTelephonePromptScheme
{
    return [[[self scheme] lowercaseString] isEqualToString:kTelephonePromptScheme];
}

- (BOOL)adx_isSafeForLoadingWithoutUserAction
{
    return [[self scheme].lowercaseString isEqualToString:@"http"] ||
        [[self scheme].lowercaseString isEqualToString:@"https"] ||
        [[self scheme].lowercaseString isEqualToString:@"about"];
}

- (BOOL)adx_isAdatrixScheme
{
    return [[self scheme] isEqualToString:kAdatrixURLScheme];
}

- (ADXAdatrixShareHostCommand)adx_AdatrixShareHostCommand
{
    NSString *host = [self host];
    if (![self adx_isAdatrixShareScheme]) {
        return ADXAdatrixShareHostCommandUnrecognized;
    } else if ([host isEqualToString:kAdatrixShareTweetHost]) {
        return ADXAdatrixShareHostCommandTweet;
    } else {
        return ADXAdatrixShareHostCommandUnrecognized;
    }
}

- (ADXAdatrixHostCommand)adx_adatrixHostCommand
{
    NSString *host = [self host];
    if (![self adx_isAdatrixScheme]) {
        return ADXAdatrixHostCommandUnrecognized;
    } else if ([host isEqualToString:kAdatrixCloseHost]) {
        return ADXAdatrixHostCommandClose;
    } else if ([host isEqualToString:kAdatrixFinishLoadHost]) {
        return ADXAdatrixHostCommandFinishLoad;
    } else if ([host isEqualToString:kAdatrixFailLoadHost]) {
        return ADXAdatrixHostCommandFailLoad;
    } else if ([host isEqualToString:kAdatrixPrecacheCompleteHost]) {
        return ADXAdatrixHostCommandPrecacheComplete;
    } else {
        return ADXAdatrixHostCommandUnrecognized;
    }
}

- (BOOL)adx_isAdatrixShareScheme
{
    return [[self scheme] isEqualToString:kAdatrixShareScheme];
}

@end
