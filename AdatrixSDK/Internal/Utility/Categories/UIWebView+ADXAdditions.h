//
//  UIWebView+ADXAdditions.h
//  Adatrix
//
//  Created by Andrew He on 11/6/11.
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *const kJavaScriptDisableDialogSnippet;

@interface UIWebView (ADXAdditions)

- (void)adx_setScrollable:(BOOL)scrollable;
- (void)disableJavaScriptDialogs;

@end
