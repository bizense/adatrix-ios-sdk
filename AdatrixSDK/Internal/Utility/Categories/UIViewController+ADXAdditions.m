//
//  UIViewController+ADXAdditions.m
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import "UIViewController+ADXAdditions.h"

#import "ADXGlobal.h"

@implementation UIViewController (ADXAdditions)

- (UIViewController *)adx_presentedViewController
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_5_0
    if ([self respondsToSelector:@selector(presentedViewController)]) {
        // For iOS 5 and above.
        return self.presentedViewController;
    }
#endif
    // Prior to iOS 5, the parentViewController property holds the presenting view controller.
    return self.parentViewController;
}

- (UIViewController *)adx_presentingViewController
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_5_0
    if ([self respondsToSelector:@selector(presentingViewController)]) {
        // For iOS 5 and above.
        return self.presentingViewController;
    } else {
        // Prior to iOS 5, the parentViewController property holds the presenting view controller.
        return self.parentViewController;
    }
#endif
    return self.parentViewController;
}

- (void)adx_presentModalViewController:(UIViewController *)modalViewController
                             animated:(BOOL)animated
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_5_0
    if ([self respondsToSelector:@selector(presentViewController:animated:completion:)]) {
        [self presentViewController:modalViewController animated:animated completion:nil];
        return;
    }
#endif

    [self presentModalViewController:modalViewController animated:animated];
}

- (void)adx_dismissModalViewControllerAnimated:(BOOL)animated
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_5_0
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]) {
        [self dismissViewControllerAnimated:animated completion:nil];
        return;
    }
#endif

    [self dismissModalViewControllerAnimated:animated];
}

@end
