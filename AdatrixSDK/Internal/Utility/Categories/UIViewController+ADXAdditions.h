//
//  UIViewController+ADXAdditions.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ADXAdditions)

/*
 * Returns the view controller that is being presented by this view controller.
 */
- (UIViewController *)adx_presentedViewController;

/*
 * Returns the view controller that presented this view controller.
 */
- (UIViewController *)adx_presentingViewController;

/*
 * Presents a view controller.
 */
- (void)adx_presentModalViewController:(UIViewController *)modalViewController
                             animated:(BOOL)animated;

/*
 * Dismisses a view controller.
 */
- (void)adx_dismissModalViewControllerAnimated:(BOOL)animated;

@end
