//
//  NSURL+ADXAdditions.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ADXAdatrixHostCommandUnrecognized,
    ADXAdatrixHostCommandClose,
    ADXAdatrixHostCommandFinishLoad,
    ADXAdatrixHostCommandFailLoad,
    ADXAdatrixHostCommandPrecacheComplete
} ADXAdatrixHostCommand;

typedef enum {
    ADXAdatrixShareHostCommandTweet,
    ADXAdatrixShareHostCommandUnrecognized
} ADXAdatrixShareHostCommand;

@interface NSURL (ADXAdditions)

- (NSDictionary *)adx_queryAsDictionary;
- (BOOL)adx_hasTelephoneScheme;
- (BOOL)adx_hasTelephonePromptScheme;
- (BOOL)adx_isSafeForLoadingWithoutUserAction;
- (BOOL)adx_isAdatrixScheme;
- (ADXAdatrixHostCommand)adx_adatrixHostCommand;
- (BOOL)adx_isAdatrixShareScheme;
- (ADXAdatrixShareHostCommand)adx_AdatrixShareHostCommand;

@end
