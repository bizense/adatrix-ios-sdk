//
//  NSHTTPURLResponse+ADXAdditions.h
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kAdatrixHTTPHeaderContentType;

@interface NSHTTPURLResponse (ADXAdditions)

- (NSStringEncoding)stringEncodingFromContentType:(NSString *)contentType;

@end
