//
//  ADXLogging.m
//  Adatrix
//
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXLogging.h"
#import "ADXIdentityProvider.h"
#import "ADXLogProvider.h"

NSString * const kADXClearErrorLogFormatWithAdUnitID = @"No ads found for ad unit: %@";
NSString * const kADXWarmingUpErrorLogFormatWithAdUnitID = @"Ad unit %@ is currently warming up. Please try again in a few minutes.";
NSString * const kADXSystemLogPrefix = @"ADATRIX: %@";

static ADXLogLevel systemLogLevel = ADXLogLevelInfo;

ADXLogLevel ADXLogGetLevel()
{
    return systemLogLevel;
}

void ADXLogSetLevel(ADXLogLevel level)
{
    systemLogLevel = level;
}

void _ADXLog(ADXLogLevel level, NSString *format, va_list args)
{
    static NSString *sIdentifier;
    static NSString *sObfuscatedIdentifier;

    if (!sIdentifier) {
        sIdentifier = [[ADXIdentityProvider identifier] copy];
    }

    if (!sObfuscatedIdentifier) {
        sObfuscatedIdentifier = [[ADXIdentityProvider obfuscatedIdentifier] copy];
    }

    NSString *logString = [[NSString alloc] initWithFormat:format arguments:args];

    // Replace identifier with a obfuscated version when logging.
    logString = [logString stringByReplacingOccurrencesOfString:sIdentifier withString:sObfuscatedIdentifier];

    [[ADXLogProvider sharedLogProvider] logMessage:logString atLogLevel:level];
}

void _ADXLogTrace(NSString *format, ...)
{
    format = [NSString stringWithFormat:kADXSystemLogPrefix, format];
    va_list args;
    va_start(args, format);
    _ADXLog(ADXLogLevelTrace, format, args);
    va_end(args);
}

void _ADXLogDebug(NSString *format, ...)
{
    format = [NSString stringWithFormat:kADXSystemLogPrefix, format];
    va_list args;
    va_start(args, format);
    _ADXLog(ADXLogLevelDebug, format, args);
    va_end(args);
}

void _ADXLogWarn(NSString *format, ...)
{
    format = [NSString stringWithFormat:kADXSystemLogPrefix, format];
    va_list args;
    va_start(args, format);
    _ADXLog(ADXLogLevelWarn, format, args);
    va_end(args);
}

void _ADXLogInfo(NSString *format, ...)
{
    format = [NSString stringWithFormat:kADXSystemLogPrefix, format];
    va_list args;
    va_start(args, format);
    _ADXLog(ADXLogLevelInfo, format, args);
    va_end(args);
}

void _ADXLogError(NSString *format, ...)
{
    format = [NSString stringWithFormat:kADXSystemLogPrefix, format];
    va_list args;
    va_start(args, format);
    _ADXLog(ADXLogLevelError, format, args);
    va_end(args);
}

void _ADXLogFatal(NSString *format, ...)
{
    format = [NSString stringWithFormat:kADXSystemLogPrefix, format];
    va_list args;
    va_start(args, format);
    _ADXLog(ADXLogLevelFatal, format, args);
    va_end(args);
}
