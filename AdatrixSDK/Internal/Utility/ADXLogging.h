//
//  ADXLogging.h
//  Adatrix
//
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXConstants.h"

extern NSString * const kADXClearErrorLogFormatWithAdUnitID;
extern NSString * const kADXWarmingUpErrorLogFormatWithAdUnitID;

// Lower = finer-grained logs.
typedef enum
{
    ADXLogLevelAll        = 0,
    ADXLogLevelTrace        = 10,
    ADXLogLevelDebug        = 20,
    ADXLogLevelInfo        = 30,
    ADXLogLevelWarn        = 40,
    ADXLogLevelError        = 50,
    ADXLogLevelFatal        = 60,
    ADXLogLevelOff        = 70
} ADXLogLevel;

ADXLogLevel ADXLogGetLevel(void);
void ADXLogSetLevel(ADXLogLevel level);
void _ADXLogTrace(NSString *format, ...);
void _ADXLogDebug(NSString *format, ...);
void _ADXLogInfo(NSString *format, ...);
void _ADXLogWarn(NSString *format, ...);
void _ADXLogError(NSString *format, ...);
void _ADXLogFatal(NSString *format, ...);

#if ADX_DEBUG_MODE && !SPECS

#define ADXLogTrace(...) _ADXLogTrace(__VA_ARGS__)
#define ADXLogDebug(...) _ADXLogDebug(__VA_ARGS__)
#define ADXLogInfo(...) _ADXLogInfo(__VA_ARGS__)
#define ADXLogWarn(...) _ADXLogWarn(__VA_ARGS__)
#define ADXLogError(...) _ADXLogError(__VA_ARGS__)
#define ADXLogFatal(...) _ADXLogFatal(__VA_ARGS__)

#else

#define ADXLogTrace(...) {}
#define ADXLogDebug(...) {}
#define ADXLogInfo(...) {}
#define ADXLogWarn(...) {}
#define ADXLogError(...) {}
#define ADXLogFatal(...) {}

#endif
