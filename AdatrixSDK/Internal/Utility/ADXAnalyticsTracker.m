//
//  ADXAnalyticsTracker.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXAnalyticsTracker.h"
#import "ADXAdConfiguration.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXLogging.h"

@interface ADXAnalyticsTracker ()

- (NSURLRequest *)requestForURL:(NSURL *)URL;

@end

@implementation ADXAnalyticsTracker

+ (ADXAnalyticsTracker *)tracker
{
    return [[ADXAnalyticsTracker alloc] init];
}

- (void)trackImpressionForConfiguration:(ADXAdConfiguration *)configuration
{
    ADXLogDebug(@"Tracking impression: %@", configuration.impressionTrackingURL);
    [NSURLConnection connectionWithRequest:[self requestForURL:configuration.impressionTrackingURL]
                                  delegate:nil];
}

- (void)trackClickForConfiguration:(ADXAdConfiguration *)configuration
{
    ADXLogDebug(@"Tracking click: %@", configuration.clickTrackingURL);
    [NSURLConnection connectionWithRequest:[self requestForURL:configuration.clickTrackingURL]
                                  delegate:nil];
}

- (NSURLRequest *)requestForURL:(NSURL *)URL
{
    NSMutableURLRequest *request = [[ADXCoreInstanceProvider sharedProvider] buildConfiguredURLRequestWithURL:URL];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    return request;
}

@end
