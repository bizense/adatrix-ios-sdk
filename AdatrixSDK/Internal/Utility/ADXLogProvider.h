//
//  ADXLogProvider.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXLogging.h"

@protocol ADXLogger;

@interface ADXLogProvider : NSObject

+ (ADXLogProvider *)sharedLogProvider;
- (void)addLogger:(id<ADXLogger>)logger;
- (void)removeLogger:(id<ADXLogger>)logger;
- (void)logMessage:(NSString *)message atLogLevel:(ADXLogLevel)logLevel;

@end

@protocol ADXLogger <NSObject>

- (ADXLogLevel)logLevel;
- (void)logMessage:(NSString *)message;

@end
