//
//  ADXAnalyticsTracker.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXAdConfiguration;

@interface ADXAnalyticsTracker : NSObject

+ (ADXAnalyticsTracker *)tracker;

- (void)trackImpressionForConfiguration:(ADXAdConfiguration *)configuration;
- (void)trackClickForConfiguration:(ADXAdConfiguration *)configuration;

@end
