//
//  ADXLogProvider.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXLogProvider.h"

@interface ADXLogProvider ()

@property (nonatomic, strong) NSMutableArray *loggers;

@end

@interface ADXSystemLogger : NSObject <ADXLogger>
@end

@implementation ADXLogProvider

#pragma mark - Singleton instance

+ (ADXLogProvider *)sharedLogProvider
{
    static dispatch_once_t once;
    static ADXLogProvider *sharedLogProvider;
    dispatch_once(&once, ^{
        sharedLogProvider = [[self alloc] init];
    });

    return sharedLogProvider;
}

#pragma mark - Object Lifecycle

- (id)init
{
    self = [super init];
    if (self) {
        _loggers = [NSMutableArray array];
        [self addLogger:[[ADXSystemLogger alloc] init]];
    }
    return self;
}

#pragma mark - Loggers

- (void)addLogger:(id<ADXLogger>)logger
{
    [self.loggers addObject:logger];
}

- (void)removeLogger:(id<ADXLogger>)logger
{
    [self.loggers removeObject:logger];
}

#pragma mark - Logging

- (void)logMessage:(NSString *)message atLogLevel:(ADXLogLevel)logLevel
{
    [self.loggers enumerateObjectsUsingBlock:^(id<ADXLogger> logger, NSUInteger idx, BOOL *stop) {
        if ([logger logLevel] <= logLevel) {
            [logger logMessage:message];
        }
    }];
}

@end

@implementation ADXSystemLogger

- (void)logMessage:(NSString *)message
{
    NSLog(@"%@", message);
}

- (ADXLogLevel)logLevel
{
    return ADXLogGetLevel();
}

@end
