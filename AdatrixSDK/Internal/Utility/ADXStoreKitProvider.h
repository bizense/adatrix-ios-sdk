//
//  ADXFeatureDetector.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXGlobal.h"
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
#import <StoreKit/StoreKit.h>
#endif

@class SKStoreProductViewController;

@interface ADXStoreKitProvider : NSObject

+ (BOOL)deviceHasStoreKit;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
+ (SKStoreProductViewController *)buildController;
#endif

@end

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
@protocol ADXSKStoreProductViewControllerDelegate <SKStoreProductViewControllerDelegate>
#else
@protocol ADXSKStoreProductViewControllerDelegate
#endif
@end
