//
//  ADXInternalUtils.m
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXInternalUtils.h"

@implementation ADXInternalUtils

@end

@implementation NSMutableDictionary (ADXInternalUtils)

- (void)adx_safeSetObject:(id)obj forKey:(id<NSCopying>)key
{
    if (obj != nil) {
        [self setObject:obj forKey:key];
    }
}

- (void)adx_safeSetObject:(id)obj forKey:(id<NSCopying>)key withDefault:(id)defaultObj
{
    if (obj != nil) {
        [self setObject:obj forKey:key];
    } else {
        [self setObject:defaultObj forKey:key];
    }
}

@end
