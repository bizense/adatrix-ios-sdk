//
//  ADXTimer.h
//  Adatrix
//
//  Created by Andrew He on 3/8/11.
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * ADXTimer wraps an NSTimer and adds pause/resume functionality.
 */
@interface ADXTimer : NSObject

@property (nonatomic, copy) NSString *runLoopMode;

+ (ADXTimer *)timerWithTimeInterval:(NSTimeInterval)seconds
                            target:(id)target
                          selector:(SEL)aSelector
                           repeats:(BOOL)repeats;

- (BOOL)isValid;
- (void)invalidate;
- (BOOL)isScheduled;
- (BOOL)scheduleNow;
- (BOOL)pause;
- (BOOL)resume;
- (NSTimeInterval)initialTimeInterval;

@end
