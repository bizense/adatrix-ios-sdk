//
//  ADXIdentityProvider.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXIdentityProvider.h"
#import "ADXGlobal.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
#import <AdSupport/AdSupport.h>
#endif

#define ADATRIX_IDENTIFIER_DEFAULTS_KEY @"com.adatrix.identifier"

@interface ADXIdentityProvider ()

+ (BOOL)deviceHasASIdentifierManager;

+ (NSString *)identifierFromASIdentifierManager:(BOOL)obfuscate;
+ (NSString *)adatrixIdentifier:(BOOL)obfuscate;

@end

@implementation ADXIdentityProvider

+ (BOOL)deviceHasASIdentifierManager
{
    return !!NSClassFromString(@"ASIdentifierManager");
}

+ (NSString *)identifier
{
    return [self _identifier:NO];
}

+ (NSString *)obfuscatedIdentifier
{
    return [self _identifier:YES];
}

+ (NSString *)_identifier:(BOOL)obfuscate
{
    if ([self deviceHasASIdentifierManager]) {
        return [self identifierFromASIdentifierManager:obfuscate];
    } else {
        return [self adatrixIdentifier:obfuscate];
    }
}

+ (BOOL)advertisingTrackingEnabled
{
    BOOL enabled = YES;

    if ([self deviceHasASIdentifierManager]) {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
        enabled = [[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled];
#endif
    }

    return enabled;
}

+ (NSString *)identifierFromASIdentifierManager:(BOOL)obfuscate
{
    if (obfuscate) {
        return @"ifa:XXXX";
    }

    NSString *identifier = nil;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_6_0
    identifier = [[ASIdentifierManager sharedManager].advertisingIdentifier UUIDString];
#endif

    return [NSString stringWithFormat:@"ifa:%@", [identifier uppercaseString]];
}

+ (NSString *)adatrixIdentifier:(BOOL)obfuscate
{
    if (obfuscate) {
        return @"adatrix:XXXX";
    }

    NSString *identifier = [[NSUserDefaults standardUserDefaults] objectForKey:ADATRIX_IDENTIFIER_DEFAULTS_KEY];
    if (!identifier) {
        CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
        NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
        CFRelease(uuidObject);

        identifier = [NSString stringWithFormat:@"adatrix:%@", [uuidStr uppercaseString]];
        [[NSUserDefaults standardUserDefaults] setObject:identifier forKey:ADATRIX_IDENTIFIER_DEFAULTS_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    return identifier;
}

@end
