//
//  ADXGlobal.h
//  Adatrix
//
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef ADX_ANIMATED
#define ADX_ANIMATED YES
#endif

UIInterfaceOrientation ADXInterfaceOrientation(void);
UIWindow *ADXKeyWindow(void);
CGFloat ADXStatusBarHeight(void);
CGRect ADXApplicationFrame(void);
CGRect ADXScreenBounds(void);
CGSize ADXScreenResolution(void);
CGFloat ADXDeviceScaleFactor(void);
NSDictionary *ADXDictionaryFromQueryString(NSString *query);
NSString *ADXSHA1Digest(NSString *string);
BOOL ADXVIEWIsVisible(UIView *view);
BOOL ADXVIEWIntersectsParentWindowWithPercent(UIView *view, CGFloat percentVisible);
NSString *ADXResourcePathForResource(NSString *resourceName);

////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Availability constants.
 */

#define ADX_IOS_2_0  20000
#define ADX_IOS_2_1  20100
#define ADX_IOS_2_2  20200
#define ADX_IOS_3_0  30000
#define ADX_IOS_3_1  30100
#define ADX_IOS_3_2  30200
#define ADX_IOS_4_0  40000
#define ADX_IOS_4_1  40100
#define ADX_IOS_4_2  40200
#define ADX_IOS_4_3  40300
#define ADX_IOS_5_0  50000
#define ADX_IOS_5_1  50100
#define ADX_IOS_6_0  60000
#define ADX_IOS_7_0  70000

////////////////////////////////////////////////////////////////////////////////////////////////////

enum {
    ADXInterstitialCloseButtonStyleAlwaysVisible,
    ADXInterstitialCloseButtonStyleAlwaysHidden,
    ADXInterstitialCloseButtonStyleAdControlled
};
typedef NSUInteger ADXInterstitialCloseButtonStyle;

enum {
    ADXInterstitialOrientationTypePortrait,
    ADXInterstitialOrientationTypeLandscape,
    ADXInterstitialOrientationTypeAll
};
typedef NSUInteger ADXInterstitialOrientationType;


////////////////////////////////////////////////////////////////////////////////////////////////////

@interface NSString (ADXAdditions)

/*
 * Returns string with reserved/unsafe characters encoded.
 */
- (NSString *)URLEncodedString;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@interface UIDevice (ADXAdditions)

- (NSString *)hardwareDeviceName;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@interface UIApplication (ADXAdditions)

// Correct way to hide/show the status bar on pre-ios 7.
- (void)adx_preIOS7setApplicationStatusBarHidden:(BOOL)hidden;
- (BOOL)adx_supportsOrientationMask:(UIInterfaceOrientationMask)orientationMask;
- (BOOL)adx_doesOrientation:(UIInterfaceOrientation)orientation matchOrientationMask:(UIInterfaceOrientationMask)orientationMask;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////
// Optional Class Forward Def Protocols
////////////////////////////////////////////////////////////////////////////////////////////////////

@class ADXAdConfiguration, CLLocation;

@protocol ADXAdAlertManagerProtocol <NSObject>

@property (nonatomic, strong) ADXAdConfiguration *adConfiguration;
@property (nonatomic, copy) NSString *adUnitId;
@property (nonatomic, copy) NSString *viewId;
@property (nonatomic, copy) CLLocation *location;
@property (nonatomic, weak) UIView *targetAdView;
@property (nonatomic, weak) id delegate;

- (void)beginMonitoringAlerts;
- (void)endMonitoringAlerts;
- (void)processAdAlertOnce;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////
// Small alert wrapper class to handle telephone protocol prompting
////////////////////////////////////////////////////////////////////////////////////////////////////

@class ADXTelephoneConfirmationController;

typedef void (^ADXTelephoneConfirmationControllerClickHandler)(NSURL *targetTelephoneURL, BOOL confirmed);

@interface ADXTelephoneConfirmationController : NSObject <UIAlertViewDelegate>

- (id)initWithURL:(NSURL *)url clickHandler:(ADXTelephoneConfirmationControllerClickHandler)clickHandler;
- (void)show;

@end
