//
//  ADXPrivateInterstitialCustomEventDelegate.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXInterstitialCustomEventDelegate.h"

@class ADXAdConfiguration;
@class CLLocation;

@protocol ADXPrivateInterstitialCustomEventDelegate <ADXInterstitialCustomEventDelegate>

- (NSString *)adUnitId;
- (NSString *)viewId;
- (ADXAdConfiguration *)configuration;
- (id)interstitialDelegate;

@end
