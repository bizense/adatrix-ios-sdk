//
//  ADXLegacyInterstitialCustomEventAdapter.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXBaseInterstitialAdapter.h"

@interface ADXLegacyInterstitialCustomEventAdapter : ADXBaseInterstitialAdapter

- (void)customEventDidLoadAd;
- (void)customEventDidFailToLoadAd;
- (void)customEventActionWillBegin;

@end
