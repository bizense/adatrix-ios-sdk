//
//  ADXBaseInterstitialAdapter.h
//  Adatrix
//
//  Created by Nafis Jamal on 4/27/11.
//  Copyright 2011 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ADXAdConfiguration, CLLocation;

@protocol ADXInterstitialAdapterDelegate;

@interface ADXBaseInterstitialAdapter : NSObject

@property (nonatomic, weak) id<ADXInterstitialAdapterDelegate> delegate;

/*
 * Creates an adapter with a reference to an ADXInterstitialAdManager.
 */
- (id)initWithDelegate:(id<ADXInterstitialAdapterDelegate>)delegate;

/*
 * Sets the adapter's delegate to nil.
 */
- (void)unregisterDelegate;

- (void)getAdWithConfiguration:(ADXAdConfiguration *)configuration;
- (void)_getAdWithConfiguration:(ADXAdConfiguration *)configuration;

- (void)didStopLoading;

/*
 * Presents the interstitial from the specified view controller.
 */
- (void)showInterstitialFromViewController:(UIViewController *)controller;

@end

@interface ADXBaseInterstitialAdapter (ProtectedMethods)

- (void)trackImpression;
- (void)trackClick;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@class ADXInterstitialAdController;

@protocol ADXInterstitialAdapterDelegate

- (ADXInterstitialAdController *)interstitialAdController;
- (id)interstitialDelegate;
- (CLLocation *)location;

- (void)adapterDidFinishLoadingAd:(ADXBaseInterstitialAdapter *)adapter;
- (void)adapter:(ADXBaseInterstitialAdapter *)adapter didFailToLoadAdWithError:(NSError *)error;
- (void)interstitialWillAppearForAdapter:(ADXBaseInterstitialAdapter *)adapter;
- (void)interstitialDidAppearForAdapter:(ADXBaseInterstitialAdapter *)adapter;
- (void)interstitialWillDisappearForAdapter:(ADXBaseInterstitialAdapter *)adapter;
- (void)interstitialDidDisappearForAdapter:(ADXBaseInterstitialAdapter *)adapter;
- (void)interstitialDidExpireForAdapter:(ADXBaseInterstitialAdapter *)adapter;
- (void)interstitialDidReceiveTapEventForAdapter:(ADXBaseInterstitialAdapter *)adapter;
- (void)interstitialWillLeaveApplicationForAdapter:(ADXBaseInterstitialAdapter *)adapter;

@end
