//
//  ADXInterstitialAdManagerDelegate.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXInterstitialAdManager;
@class ADXInterstitialAdController;
@class CLLocation;

@protocol ADXInterstitialAdManagerDelegate <NSObject>

- (ADXInterstitialAdController *)interstitialAdController;
- (CLLocation *)location;
- (id)interstitialDelegate;
- (void)managerDidLoadInterstitial:(ADXInterstitialAdManager *)manager;
- (void)manager:(ADXInterstitialAdManager *)manager
didFailToLoadInterstitialWithError:(NSError *)error;
- (void)managerWillPresentInterstitial:(ADXInterstitialAdManager *)manager;
- (void)managerDidPresentInterstitial:(ADXInterstitialAdManager *)manager;
- (void)managerWillDismissInterstitial:(ADXInterstitialAdManager *)manager;
- (void)managerDidDismissInterstitial:(ADXInterstitialAdManager *)manager;
- (void)managerDidExpireInterstitial:(ADXInterstitialAdManager *)manager;
- (void)managerDidReceiveTapEventFromInterstitial:(ADXInterstitialAdManager *)manager;

@end
