//
//  ADXInterstitialAdManager.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXAdServerCommunicator.h"
#import "ADXBaseInterstitialAdapter.h"

@class CLLocation;
@protocol ADXInterstitialAdManagerDelegate;

@interface ADXInterstitialAdManager : NSObject <ADXAdServerCommunicatorDelegate,
    ADXInterstitialAdapterDelegate>

@property (nonatomic, weak) id<ADXInterstitialAdManagerDelegate> delegate;
@property (nonatomic, assign, readonly) BOOL ready;

- (id)initWithDelegate:(id<ADXInterstitialAdManagerDelegate>)delegate;

- (void)loadInterstitialWithAdUnitID:(NSString *)ID
                              viewID:(NSString *)viewID
                            keywords:(NSString *)keywords
                            location:(CLLocation *)location
                             testing:(BOOL)testing;
- (void)presentInterstitialFromViewController:(UIViewController *)controller;

// Deprecated
- (void)customEventDidLoadAd;
- (void)customEventDidFailToLoadAd;
- (void)customEventActionWillBegin;

@end
