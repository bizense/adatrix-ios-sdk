//
//  ADXInterstitialCustomEventAdapter.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXBaseInterstitialAdapter.h"

#import "ADXPrivateInterstitialCustomEventDelegate.h"

@interface ADXInterstitialCustomEventAdapter : ADXBaseInterstitialAdapter <ADXPrivateInterstitialCustomEventDelegate>

@end
