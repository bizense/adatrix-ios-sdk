//
//  ADXInterstitialViewController.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXGlobal.h"

@class CLLocation;

@protocol ADXInterstitialViewControllerDelegate;

////////////////////////////////////////////////////////////////////////////////////////////////////

@interface ADXInterstitialViewController : UIViewController

@property (nonatomic, assign) ADXInterstitialCloseButtonStyle closeButtonStyle;
@property (nonatomic, assign) ADXInterstitialOrientationType orientationType;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, weak) id<ADXInterstitialViewControllerDelegate> delegate;

- (void)presentInterstitialFromViewController:(UIViewController *)controller;
- (void)dismissInterstitialAnimated:(BOOL)animated;
- (BOOL)shouldDisplayCloseButton;
- (void)willPresentInterstitial;
- (void)didPresentInterstitial;
- (void)willDismissInterstitial;
- (void)didDismissInterstitial;
- (void)layoutCloseButton;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ADXInterstitialViewControllerDelegate <NSObject>

- (NSString *)adUnitId;
- (NSString *)viewId;
- (CLLocation *)location;
- (void)interstitialDidLoadAd:(ADXInterstitialViewController *)interstitial;
- (void)interstitialDidFailToLoadAd:(ADXInterstitialViewController *)interstitial;
- (void)interstitialWillAppear:(ADXInterstitialViewController *)interstitial;
- (void)interstitialDidAppear:(ADXInterstitialViewController *)interstitial;
- (void)interstitialWillDisappear:(ADXInterstitialViewController *)interstitial;
- (void)interstitialDidDisappear:(ADXInterstitialViewController *)interstitial;
- (void)interstitialDidReceiveTapEvent:(ADXInterstitialViewController *)interstitial;
- (void)interstitialWillLeaveApplication:(ADXInterstitialViewController *)interstitial;

@end
