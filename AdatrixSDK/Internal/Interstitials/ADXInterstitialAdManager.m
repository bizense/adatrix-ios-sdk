//
//  ADXInterstitialAdManager.m
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <objc/runtime.h>

#import "ADXInterstitialAdManager.h"

#import "ADXAdServerURLBuilder.h"
#import "ADXInterstitialAdController.h"
#import "ADXInterstitialCustomEventAdapter.h"
#import "ADXInstanceProvider.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXInterstitialAdManagerDelegate.h"
#import "ADXLogging.h"
#import "ADXError.h"
#import "ADXHTMLInterstitialCustomEvent.h"

@interface ADXInterstitialAdManager ()

@property (nonatomic, assign) BOOL loading;
@property (nonatomic, assign, readwrite) BOOL ready;
@property (nonatomic, strong) ADXBaseInterstitialAdapter *adapter;
@property (nonatomic, strong) ADXAdServerCommunicator *communicator;
@property (nonatomic, strong) ADXAdConfiguration *configuration;

- (void)setUpAdapterWithConfiguration:(ADXAdConfiguration *)configuration;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation ADXInterstitialAdManager

@synthesize loading = _loading;
@synthesize ready = _ready;
@synthesize delegate = _delegate;
@synthesize communicator = _communicator;
@synthesize adapter = _adapter;
@synthesize configuration = _configuration;

- (id)initWithDelegate:(id<ADXInterstitialAdManagerDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.communicator = [[ADXCoreInstanceProvider sharedProvider] buildADXAdServerCommunicatorWithDelegate:self];
        self.delegate = delegate;
    }
    return self;
}

- (void)dealloc
{
    [self.communicator cancel];
    [self.communicator setDelegate:nil];

    self.adapter = nil;
}

- (void)setAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    if (self.adapter != adapter) {
        [self.adapter unregisterDelegate];
        _adapter = adapter;
    }
}

#pragma mark - Public

- (void)loadAdWithURL:(NSURL *)URL
{
    if (self.loading) {
        ADXLogWarn(@"Interstitial controller is already loading an ad. "
                  @"Wait for previous load to finish.");
        return;
    }

    ADXLogInfo(@"Interstitial controller is loading ad with Adatrix server URL: %@", URL);

    self.loading = YES;
    [self.communicator loadURL:URL];
}


- (void)loadInterstitialWithAdUnitID:(NSString *)ID viewID:(NSString *)viewID keywords:(NSString *)keywords location:(CLLocation *)location testing:(BOOL)testing
{
    if (self.ready) {
        [self.delegate managerDidLoadInterstitial:self];
    } else {
        [self loadAdWithURL:[ADXAdServerURLBuilder URLWithAdUnitID:ID
                                                            viewID:viewID
                                                         keywords:keywords
                                                         location:location
                                                          testing:testing]];
    }
}

- (void)presentInterstitialFromViewController:(UIViewController *)controller
{
    if (self.ready) {
        [self.adapter showInterstitialFromViewController:controller];
    }
}

- (CLLocation *)location
{
    return [self.delegate location];
}

- (ADXInterstitialAdController *)interstitialAdController
{
    return [self.delegate interstitialAdController];
}

- (id)interstitialDelegate
{
    return [self.delegate interstitialDelegate];
}

#pragma mark - ADXAdServerCommunicatorDelegate

- (void)communicatorDidReceiveAdConfiguration:(ADXAdConfiguration *)configuration
{
    configuration.adType = ADXAdTypeInterstitial;
    configuration.customEventClass = NSClassFromString(@"ADXHTMLInterstitialCustomEvent");
    
    self.configuration = configuration;

    ADXLogInfo(@"Interstitial ad view is fetching ad network type: %@", self.configuration.networkType);

    if (self.configuration.adUnitWarmingUp) {
        ADXLogInfo(kADXWarmingUpErrorLogFormatWithAdUnitID, self.delegate.interstitialAdController.adUnitId);
        self.loading = NO;
        [self.delegate manager:self didFailToLoadInterstitialWithError:[ADXError errorWithCode:ADXErrorAdUnitWarmingUp]];
        return;
    }

    if ([self.configuration.networkType isEqualToString:kAdTypeClear]) {
        ADXLogInfo(kADXClearErrorLogFormatWithAdUnitID, self.delegate.interstitialAdController.adUnitId);
        self.loading = NO;
        [self.delegate manager:self didFailToLoadInterstitialWithError:[ADXError errorWithCode:ADXErrorNoInventory]];
        return;
    }

    if (self.configuration.adType != ADXAdTypeInterstitial) {
        ADXLogWarn(@"Could not load ad: interstitial object received a non-interstitial ad unit ID.");
        self.loading = NO;
        [self.delegate manager:self didFailToLoadInterstitialWithError:[ADXError errorWithCode:ADXErrorAdapterInvalid]];
        return;
    }
    [self setUpAdapterWithConfiguration:self.configuration];
}

- (void)communicatorDidFailWithError:(NSError *)error
{
    self.ready = NO;
    self.loading = NO;

    [self.delegate manager:self didFailToLoadInterstitialWithError:error];
}

- (void)setUpAdapterWithConfiguration:(ADXAdConfiguration *)configuration;
{
    ADXBaseInterstitialAdapter *adapter = [[ADXInstanceProvider sharedProvider] buildInterstitialAdapterForConfiguration:configuration
                                                                                                              delegate:self];
    if (!adapter) {
        [self adapter:nil didFailToLoadAdWithError:nil];
        return;
    }
    self.adapter = adapter;
    [self.adapter _getAdWithConfiguration:configuration];
}

#pragma mark - ADXInterstitialAdapterDelegate

- (void)adapterDidFinishLoadingAd:(ADXBaseInterstitialAdapter *)adapter
{
    self.ready = YES;
    self.loading = NO;
    [self.delegate managerDidLoadInterstitial:self];
}

- (void)adapter:(ADXBaseInterstitialAdapter *)adapter didFailToLoadAdWithError:(NSError *)error
{
    self.ready = NO;
    self.loading = NO;
    [self loadAdWithURL:self.configuration.failoverURL];
}

- (void)interstitialWillAppearForAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    [self.delegate managerWillPresentInterstitial:self];
}

- (void)interstitialDidAppearForAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    [self.delegate managerDidPresentInterstitial:self];
}

- (void)interstitialWillDisappearForAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    [self.delegate managerWillDismissInterstitial:self];
}

- (void)interstitialDidDisappearForAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    self.ready = NO;
    [self.delegate managerDidDismissInterstitial:self];
}

- (void)interstitialDidExpireForAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    self.ready = NO;
    [self.delegate managerDidExpireInterstitial:self];
}

- (void)interstitialDidReceiveTapEventForAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    [self.delegate managerDidReceiveTapEventFromInterstitial:self];
}

- (void)interstitialWillLeaveApplicationForAdapter:(ADXBaseInterstitialAdapter *)adapter
{
    //noop
}

#pragma mark - Legacy Custom Events

- (void)customEventDidLoadAd
{
    // XXX: The deprecated custom event behavior is to report an impression as soon as an ad loads,
    // rather than when the ad is actually displayed. Because of this, you may see impression-
    // reporting discrepancies between Adatrix and your custom ad networks.
    if ([self.adapter respondsToSelector:@selector(customEventDidLoadAd)]) {
        self.loading = NO;
        [self.adapter performSelector:@selector(customEventDidLoadAd)];
    }
}

- (void)customEventDidFailToLoadAd
{
    if ([self.adapter respondsToSelector:@selector(customEventDidFailToLoadAd)]) {
        self.loading = NO;
        [self.adapter performSelector:@selector(customEventDidFailToLoadAd)];
    }
}

- (void)customEventActionWillBegin
{
    if ([self.adapter respondsToSelector:@selector(customEventActionWillBegin)]) {
        [self.adapter performSelector:@selector(customEventActionWillBegin)];
    }
}

@end
