//
//  ADXBaseInterstitialAdapter.m
//  Adatrix
//
//  Created by Nafis Jamal on 4/27/11.
//  Copyright 2011 Adatrix. All rights reserved.
//

#import "ADXBaseInterstitialAdapter.h"
#import "ADXAdConfiguration.h"
#import "ADXGlobal.h"
#import "ADXAnalyticsTracker.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXTimer.h"
#import "ADXConstants.h"

@interface ADXBaseInterstitialAdapter ()

@property (nonatomic, strong) ADXAdConfiguration *configuration;
@property (nonatomic, strong) ADXTimer *timeoutTimer;

- (void)startTimeoutTimer;

@end

@implementation ADXBaseInterstitialAdapter

@synthesize delegate = _delegate;
@synthesize configuration = _configuration;
@synthesize timeoutTimer = _timeoutTimer;

- (id)initWithDelegate:(id<ADXInterstitialAdapterDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

- (void)dealloc
{
    [self unregisterDelegate];

    [self.timeoutTimer invalidate];

}

- (void)unregisterDelegate
{
    self.delegate = nil;
}

- (void)getAdWithConfiguration:(ADXAdConfiguration *)configuration
{
    // To be implemented by subclasses.
    [self doesNotRecognizeSelector:_cmd];
}

- (void)_getAdWithConfiguration:(ADXAdConfiguration *)configuration
{
    self.configuration = configuration;

    [self startTimeoutTimer];

    ADXBaseInterstitialAdapter *strongSelf = self;
    [strongSelf getAdWithConfiguration:configuration];
}

- (void)startTimeoutTimer
{
    NSTimeInterval timeInterval = (self.configuration && self.configuration.adTimeoutInterval >= 0) ?
            self.configuration.adTimeoutInterval : INTERSTITIAL_TIMEOUT_INTERVAL;

    if (timeInterval > 0) {
        self.timeoutTimer = [[ADXCoreInstanceProvider sharedProvider] buildADXTimerWithTimeInterval:timeInterval
                                                                                       target:self
                                                                                     selector:@selector(timeout)
                                                                                      repeats:NO];

        [self.timeoutTimer scheduleNow];
    }
}

- (void)didStopLoading
{
    [self.timeoutTimer invalidate];
}

- (void)timeout
{
    [self.delegate adapter:self didFailToLoadAdWithError:nil];
}

#pragma mark - Presentation

- (void)showInterstitialFromViewController:(UIViewController *)controller
{
    [self doesNotRecognizeSelector:_cmd];
}

#pragma mark - Metrics

- (void)trackImpression
{
    [[[ADXCoreInstanceProvider sharedProvider] sharedADXAnalyticsTracker] trackImpressionForConfiguration:self.configuration];
}

- (void)trackClick
{
    [[[ADXCoreInstanceProvider sharedProvider] sharedADXAnalyticsTracker] trackClickForConfiguration:self.configuration];
}

@end

