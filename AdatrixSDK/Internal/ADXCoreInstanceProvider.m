//
//  ADXCoreInstanceProvider.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXCoreInstanceProvider.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#import "ADXAdServerCommunicator.h"
#import "ADXURLResolver.h"
#import "ADXAdDestinationDisplayAgent.h"
#import "ADXReachability.h"
#import "ADXTimer.h"
#import "ADXAnalyticsTracker.h"
#import "ADXGeolocationProvider.h"
#import "ADXLogEventRecorder.h"
#import "ADXNetworkManager.h"

#define ADATRIX_CARRIER_INFO_DEFAULTS_KEY @"com.adatrix.carrierinfo"


typedef enum
{
    ADXTwitterDeepLinkNotChecked,
    ADXTwitterDeepLinkEnabled,
    ADXTwitterDeepLinkDisabled
} ADXTwitterDeepLink;

@interface ADXCoreInstanceProvider ()

@property (nonatomic, copy) NSString *userAgent;
@property (nonatomic, strong) NSMutableDictionary *singletons;
@property (nonatomic, strong) NSMutableDictionary *carrierInfo;
@property (nonatomic, assign) ADXTwitterDeepLink twitterDeepLinkStatus;

@end

@implementation ADXCoreInstanceProvider

@synthesize userAgent = _userAgent;
@synthesize singletons = _singletons;
@synthesize carrierInfo = _carrierInfo;
@synthesize twitterDeepLinkStatus = _twitterDeepLinkStatus;

static ADXCoreInstanceProvider *sharedProvider = nil;

+ (instancetype)sharedProvider
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedProvider = [[self alloc] init];
    });

    return sharedProvider;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.singletons = [NSMutableDictionary dictionary];

        [self initializeCarrierInfo];
    }
    return self;
}


- (id)singletonForClass:(Class)klass provider:(ADXSingletonProviderBlock)provider
{
    id singleton = [self.singletons objectForKey:klass];
    if (!singleton) {
        singleton = provider();
        [self.singletons setObject:singleton forKey:(id<NSCopying>)klass];
    }
    return singleton;
}

// This method ensures that "anObject" is retained until the next runloop iteration when
// performNoOp: is executed.
//
// This is useful in situations where, potentially due to a callback chain reaction, an object
// is synchronously deallocated as it's trying to do more work, especially invoking self, after
// the callback.
- (void)keepObjectAliveForCurrentRunLoopIteration:(id)anObject
{
    [self performSelector:@selector(performNoOp:) withObject:anObject afterDelay:0];
}

- (void)performNoOp:(id)anObject
{
    ; // noop
}

#pragma mark - Initializing Carrier Info

- (void)initializeCarrierInfo
{
    self.carrierInfo = [NSMutableDictionary dictionary];

    // check if we have a saved copy
    NSDictionary *saved = [[NSUserDefaults standardUserDefaults] dictionaryForKey:ADATRIX_CARRIER_INFO_DEFAULTS_KEY];
    if (saved != nil) {
        [self.carrierInfo addEntriesFromDictionary:saved];
    }

    // now asynchronously load a fresh copy
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
        [self performSelectorOnMainThread:@selector(updateCarrierInfoForCTCarrier:) withObject:networkInfo.subscriberCellularProvider waitUntilDone:NO];
    });
}

- (void)updateCarrierInfoForCTCarrier:(CTCarrier *)ctCarrier
{
    // use setValue instead of setObject here because ctCarrier could be nil, and any of its properties could be nil
    [self.carrierInfo setValue:ctCarrier.carrierName forKey:@"carrierName"];
    [self.carrierInfo setValue:ctCarrier.isoCountryCode forKey:@"isoCountryCode"];
    [self.carrierInfo setValue:ctCarrier.mobileCountryCode forKey:@"mobileCountryCode"];
    [self.carrierInfo setValue:ctCarrier.mobileNetworkCode forKey:@"mobileNetworkCode"];

    [[NSUserDefaults standardUserDefaults] setObject:self.carrierInfo forKey:ADATRIX_CARRIER_INFO_DEFAULTS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Fetching Ads
- (NSMutableURLRequest *)buildConfiguredURLRequestWithURL:(NSURL *)URL
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPShouldHandleCookies:YES];
    [request setValue:self.userAgent forHTTPHeaderField:@"User-Agent"];
    return request;
}

- (NSString *)userAgent
{
    if (!_userAgent) {
        self.userAgent = [[[UIWebView alloc] init] stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    }

    return _userAgent;
}

- (ADXAdServerCommunicator *)buildADXAdServerCommunicatorWithDelegate:(id<ADXAdServerCommunicatorDelegate>)delegate
{
    return [(ADXAdServerCommunicator *)[ADXAdServerCommunicator alloc] initWithDelegate:delegate];
}


#pragma mark - URL Handling

- (ADXURLResolver *)buildADXURLResolver
{
    return [ADXURLResolver resolver];
}

- (ADXAdDestinationDisplayAgent *)buildADXAdDestinationDisplayAgentWithDelegate:(id<ADXAdDestinationDisplayAgentDelegate>)delegate
{
    return [ADXAdDestinationDisplayAgent agentWithDelegate:delegate];
}

#pragma mark - Utilities

- (UIDevice *)sharedCurrentDevice
{
    return [UIDevice currentDevice];
}

- (ADXGeolocationProvider *)sharedADXGeolocationProvider
{
    return [self singletonForClass:[ADXGeolocationProvider class] provider:^id{
        return [ADXGeolocationProvider sharedProvider];
    }];
}

- (CLLocationManager *)buildCLLocationManager
{
    return [[CLLocationManager alloc] init];
}

- (id<ADXAdAlertManagerProtocol>)buildADXAdAlertManagerWithDelegate:(id)delegate
{
    id<ADXAdAlertManagerProtocol> adAlertManager = nil;

    Class adAlertManagerClass = NSClassFromString(@"ADXAdAlertManager");
    if (adAlertManagerClass != nil) {
        adAlertManager = [[adAlertManagerClass alloc] init];
        [adAlertManager performSelector:@selector(setDelegate:) withObject:delegate];
    }

    return adAlertManager;
}

- (ADXAdAlertGestureRecognizer *)buildADXAdAlertGestureRecognizerWithTarget:(id)target action:(SEL)action
{
    ADXAdAlertGestureRecognizer *gestureRecognizer = nil;

    Class gestureRecognizerClass = NSClassFromString(@"ADXAdAlertGestureRecognizer");
    if (gestureRecognizerClass != nil) {
        gestureRecognizer = [[gestureRecognizerClass alloc] initWithTarget:target action:action];
    }

    return gestureRecognizer;
}

- (NSOperationQueue *)sharedOperationQueue
{
    static NSOperationQueue *sharedOperationQueue = nil;
    static dispatch_once_t pred;

    dispatch_once(&pred, ^{
        sharedOperationQueue = [[NSOperationQueue alloc] init];
    });

    return sharedOperationQueue;
}

- (ADXAnalyticsTracker *)sharedADXAnalyticsTracker
{
    return [self singletonForClass:[ADXAnalyticsTracker class] provider:^id{
        return [ADXAnalyticsTracker tracker];
    }];
}

- (ADXReachability *)sharedADXReachability
{
    return [self singletonForClass:[ADXReachability class] provider:^id{
        return [ADXReachability reachabilityForLocalWiFi];
    }];
}

- (ADXLogEventRecorder *)sharedLogEventRecorder
{
    return [self singletonForClass:[ADXLogEventRecorder class] provider:^id{
        ADXLogEventRecorder *recorder = [[ADXLogEventRecorder alloc] init];
        return recorder;
    }];
}

- (ADXNetworkManager *)sharedNetworkManager
{
    return [self singletonForClass:[ADXNetworkManager class] provider:^id{
        return [ADXNetworkManager sharedNetworkManager];
    }];
}

- (NSDictionary *)sharedCarrierInfo
{
    return self.carrierInfo;
}

- (ADXTimer *)buildADXTimerWithTimeInterval:(NSTimeInterval)seconds target:(id)target selector:(SEL)selector repeats:(BOOL)repeats
{
    return [ADXTimer timerWithTimeInterval:seconds target:target selector:selector repeats:repeats];
}

@end
