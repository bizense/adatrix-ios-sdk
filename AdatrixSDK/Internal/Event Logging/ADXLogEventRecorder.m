//
//  ADXLogEventRecorder.m
//  AdatrixSDK

//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#include <stdlib.h>
#import "ADXLogEventRecorder.h"
#import "ADXLogEvent.h"
#import "ADXLogging.h"
#import "ADXLogEventCommunicator.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXTimer.h"

void ADXAddLogEvent(ADXLogEvent *event)
{
    [[[ADXCoreInstanceProvider sharedProvider] sharedLogEventRecorder] addEvent:event];
}

/**
 *  The max number of events allowed in the event queue.
 */
static const NSInteger QUEUE_LIMIT = 1000;

/**
 *  The max number of events sent per request.
 */
static const NSInteger EVENT_SEND_THRESHOLD = 25;

/**
 * A number between 0 and 1 that represents the ratio at which events will be added
 * to the event queue. For example, if SAMPLE_RATE is set to 0.2, then 20% of all 
 * events reported to the ADXLogEventRecorder will be communicated to scribe.
 */
static const double SAMPLE_RATE = 0.1;

/**
 *  The delay in seconds to wait until sending the next batch of events.
 */
static const NSTimeInterval POLL_DELAY_INTERVAL = 2 * 60;

///////////////////////////////////////////////////////////////////////////

@interface ADXLogEventRecorder ()<NSURLConnectionDataDelegate>

/**
 *  IMPORTANT: All access to self.events should be performed inside a block on self.dispatchQueue.
 *  This is to prevent concurrent access issues to the event array.
 */
@property (nonatomic) dispatch_queue_t dispatchQueue;
@property (nonatomic) NSMutableArray *events;

@property (nonatomic) ADXLogEventCommunicator *communicator;
@property (nonatomic) ADXTimer *sendTimer;

@end

///////////////////////////////////////////////////////////////////////////

@implementation ADXLogEventRecorder

#pragma mark - Public methods

- (instancetype)init
{
    if (self = [super init]) {
        _events = [NSMutableArray array];
        _dispatchQueue = dispatch_queue_create("com.adatrix.ADXLogEventRecorder", NULL);
        _communicator = [[ADXLogEventCommunicator alloc] init];
        _sendTimer = [ADXTimer timerWithTimeInterval:POLL_DELAY_INTERVAL
                                                 target:self
                                               selector:@selector(sendEvents)
                                                repeats:YES];
        [_sendTimer scheduleNow];
    }
    
    return self;
}

- (void)dealloc
{
    [self.sendTimer invalidate];
}

- (void)addEvent:(ADXLogEvent *)event
{
    if (event) {
        dispatch_async(self.dispatchQueue, ^{
            
            // We only add the event to the queue if it's been selected for sampling.
            if (![self sample]) {
                ADXLogDebug(@"RECORDER: Skipped adding log event to the queue because it failed the sample test.");
                return;
            }
            
            if ([self overQueueLimit]) {
                ADXLogDebug(@"RECORDER: Skipped adding log event to the queue because the event queue is over its size limit.");
                return;
            }
            
            [self.events addObject:event];
            ADXLogDebug([NSString stringWithFormat:@"RECORDER: Event added. There are now %lu events in the queue.", (unsigned long)[self.events count]]);
        });
    }
}

#pragma mark - Private methods

- (void)sendEvents
{
    dispatch_async(self.dispatchQueue, ^{
        ADXLogDebug([NSString stringWithFormat:@"RECORDER: -sendEvents dispatched with %lu events in the queue.", (unsigned long)[self.events count]]);
        
        if ([self.communicator isOverLimit]) {
            ADXLogDebug(@"RECORDER: Skipped sending events because the communicator has too many active connections.");
            return;
        }
        
        if ([self.events count] == 0) {
            return;
        }
        
        if ([self.events count] > EVENT_SEND_THRESHOLD) {
            ADXLogDebug(@"RECORDER: Enqueueing a portion of events to be scribed.");
            
            // If we have more events than we can send at once, then send only the first slice.
            NSRange sendRange;
            sendRange.location = 0;
            sendRange.length = EVENT_SEND_THRESHOLD;
            NSArray *eventsToSend = [self.events subarrayWithRange:sendRange];
            
            // Don't flush the event array in this case, because we'll have more to send in the
            // future.
            NSRange unSentRange;
            unSentRange.location = sendRange.length;
            unSentRange.length = [self.events count] - sendRange.length;
            NSArray *unSentEvents = [self.events subarrayWithRange:unSentRange];
            
            [self.communicator sendEvents:eventsToSend];
            self.events = [unSentEvents mutableCopy];
        } else {
            ADXLogDebug(@"RECORDER: Enqueueing all events to be scribed.");
            [self.communicator sendEvents:[NSArray arrayWithArray:self.events]];
            [self.events removeAllObjects];
        }
        
        ADXLogDebug([NSString stringWithFormat:@"RECORDER: There are now %lu events in the queue.", (unsigned long)[self.events count]]);
    });
}

/**
 *  IMPORTANT: This method should only be called inside a block on the object's dispatch queue.
 */
- (BOOL)overQueueLimit
{
    return [self.events count] >= QUEUE_LIMIT;
}

- (BOOL)sample
{
    NSUInteger diceRoll = arc4random_uniform(100);
    return [self shouldSampleForRate:SAMPLE_RATE diceRoll:diceRoll];
}

/**
 *  IMPORTANT: This method takes a sample rate between 0 and 1, and the diceRoll is intended to be between 0 and 100. It has been separated from the -(BOOL)sample method for easier testing. 
 */
- (BOOL)shouldSampleForRate:(CGFloat)sampleRate diceRoll:(NSUInteger)diceRoll
{
    NSUInteger sample = (NSUInteger)(sampleRate*100);
    return diceRoll < sample;
}

@end
