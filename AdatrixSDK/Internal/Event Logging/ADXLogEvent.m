//
//  ADXLogEvent.m
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import "ADXLogEvent.h"
#import "ADXIdentityProvider.h"
#import "ADXInternalUtils.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXConstants.h"
#import "ADXReachability.h"
#import "ADXGeolocationProvider.h"

NSUInteger const IOS_APP_PLATFORM_ID = 1;
NSUInteger const IOS_SDK_PRODUCT_ID = 0;
NSString * const IOS_MANUFACTURER_NAME = @"Apple";

/*
 * Event names.
 */
NSString * const ADXLogEventNameAdRequest = @"ad_request";

/*
 * Event categories.
 */
NSString * const ADXLogEventCategoryRequests = @"requests";

#pragma mark - Private properties

@interface ADXLogEvent ()

@property (nonatomic, readwrite) NSDate *timestamp;
@property (nonatomic, readwrite) ADXLogEventScribeCategory scribeCategory;
@property (nonatomic, copy, readwrite) NSString *sdkVersion;
@property (nonatomic, copy, readwrite) NSString *deviceModel;
@property (nonatomic, copy, readwrite) NSString *deviceOSVersion;
@property (nonatomic, readwrite) CGSize deviceSize;
@property (nonatomic, readwrite) double geoLat;
@property (nonatomic, readwrite) double geoLon;
@property (nonatomic, readwrite) double geoAccuracy;
@property (nonatomic, readwrite) ADXLogEventNetworkType networkType;
@property (nonatomic, copy, readwrite) NSString *networkOperatorCode;
@property (nonatomic, copy, readwrite) NSString *networkOperatorName;
@property (nonatomic, copy, readwrite) NSString *networkISOCountryCode;
@property (nonatomic, copy, readwrite) NSString *networkSIMCode;
@property (nonatomic, copy, readwrite) NSString *networkSIMOperatorName;
@property (nonatomic, copy, readwrite) NSString *networkSimISOCountryCode;
@property (nonatomic, readwrite) NSUInteger performanceDurationMs;
@property (nonatomic, copy, readwrite) NSString *clientAdvertisingId;
@property (nonatomic, readwrite) BOOL clientDoNotTrack;

@end


@implementation ADXLogEvent

- (instancetype)init
{
    if (self = [super init]) {
        
        ADXCoreInstanceProvider *provider = [ADXCoreInstanceProvider sharedProvider];
        
        [self setTimestamp:[NSDate date]];
        [self setScribeCategory:ADXExchangeClientEventCategory];
        
        // SDK Info
        [self setSdkVersion:ADX_SDK_VERSION];
        
        // Device info
        UIDevice *device = [UIDevice currentDevice];
        [self setDeviceModel:[device model]];
        [self setDeviceOSVersion:[device systemVersion]];
        
        [self setDeviceSize:ADXScreenBounds().size];
        
        // Geolocation info
        CLLocation *location = [[[ADXCoreInstanceProvider sharedProvider] sharedADXGeolocationProvider] lastKnownLocation];
        [self setGeoLat:location.coordinate.latitude];
        [self setGeoLon:location.coordinate.longitude];
        [self setGeoAccuracy:location.horizontalAccuracy];
        
        // Client info
        [self setClientDoNotTrack:![ADXIdentityProvider advertisingTrackingEnabled]];
        // Note: we've chosen at this time to never send the real IDFA.
        [self setClientAdvertisingId:[ADXIdentityProvider identifier]];


        // Network/Carrier info
        if ([provider sharedADXReachability].hasWifi) {
            [self setNetworkType:ADXLogEventNetworkTypeWifi];
        } else if ([provider sharedADXReachability].hasCellular) {
            [self setNetworkType:ADXLogEventNetworkTypeMobile];
            
            NSDictionary *carrierInfo = [[ADXCoreInstanceProvider sharedProvider] sharedCarrierInfo];
            NSString *networkOperatorName = carrierInfo[@"carrierName"];
            NSString *mcc = carrierInfo[@"mobileCountryCode"];
            NSString *mnc = carrierInfo[@"mobileNetworkCode"];
            NSString *networkOperatorCode = [NSString stringWithFormat:@"%@%@", mcc, mnc];
            NSString *isoCountryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
            
            [self setNetworkOperatorName:networkOperatorName];
            [self setNetworkSIMOperatorName:networkOperatorName];
            
            [self setNetworkSIMCode:networkOperatorCode];
            [self setNetworkOperatorCode:networkOperatorCode];
            
            [self setNetworkISOCountryCode:isoCountryCode];
            [self setNetworkSimISOCountryCode:isoCountryCode];
            
        } else {
            [self setNetworkType:ADXLogEventNetworkTypeUnknown];
        }
        
    }
    
    return self;
}

- (void)setRequestURI:(NSString *)requestURI
{
    // We don't pass up the advertising identifier so we obfuscate it.
    _requestURI = [requestURI stringByReplacingOccurrencesOfString:[ADXIdentityProvider identifier]
                                                        withString:[ADXIdentityProvider identifier]];
}

- (NSUInteger)appPlatform
{
    return IOS_APP_PLATFORM_ID;
}

- (NSUInteger)sdkProduct
{
    return IOS_SDK_PRODUCT_ID;
}

- (NSString *)deviceManufacturer
{
    return IOS_MANUFACTURER_NAME;
}

/**
 * Current timestamp in ms since January 1, 1970 00:00:00.0 UTC (aka epoch time)
 */
- (NSUInteger)timestampAsEpoch
{
    return [[self timestamp] timeIntervalSince1970];
}

- (NSString *)eventCategoryAsString
{
    return @"exchange_client_event";
}

- (NSUInteger)networkTypeAsInteger
{
    switch ([self networkType]) {
        case ADXLogEventNetworkTypeUnknown:
            return 0;
            break;
        case ADXLogEventNetworkTypeEthernet:
            return 1;
            break;
        case ADXLogEventNetworkTypeWifi:
            return 2;
            break;
        case ADXLogEventNetworkTypeMobile:
            return 3;
            break;
        default:
            return 0;
            break;
    }
}

- (NSDictionary *)asDictionary
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
    
    [d adx_safeSetObject:[self eventCategoryAsString] forKey:@"_category_"];
    [d adx_safeSetObject:[self eventName] forKey:@"name"];
    [d adx_safeSetObject:[self eventCategory] forKey:@"name_category"];
    
    [d adx_safeSetObject:@([self sdkProduct]) forKey:@"sdk_product"];
    [d adx_safeSetObject:[self sdkVersion] forKey:@"sdk_version"];
    
    [d adx_safeSetObject:[self adUnitId] forKey:@"ad_unit_id"];
    [d adx_safeSetObject:[self viewId] forKey:@"ad_viewid"];
    [d adx_safeSetObject:[self adCreativeId] forKey:@"ad_creative_id"];
    [d adx_safeSetObject:[self adType] forKey:@"ad_type"];
    [d adx_safeSetObject:[self adNetworkType] forKey:@"ad_network_type"];
    [d adx_safeSetObject:@(self.adSize.width) forKey:@"ad_width_px"];
    [d adx_safeSetObject:@(self.adSize.height) forKey:@"ad_height_px"];
    
    [d adx_safeSetObject:@([self appPlatform]) forKey:@"app_platform"];
    [d adx_safeSetObject:[self appName] forKey:@"app_name"];
    [d adx_safeSetObject:[self appStoreId] forKey:@"app_appstore_id"];
    [d adx_safeSetObject:[self appBundleId] forKey:@"app_bundle_id"];
    [d adx_safeSetObject:[self appVersion] forKey:@"app_version"];
    
    [d adx_safeSetObject:[self clientAdvertisingId] forKey:@"client_advertising_id"];
    [d adx_safeSetObject:@([self clientDoNotTrack]) forKey:@"client_do_not_track"];
    
    [d adx_safeSetObject:[self deviceManufacturer] forKey:@"device_manufacturer"];
    [d adx_safeSetObject:[self deviceModel] forKey:@"device_model"];
    [d adx_safeSetObject:[self deviceModel] forKey:@"device_product"];
    [d adx_safeSetObject:[self deviceOSVersion] forKey:@"device_os_version"];
    [d adx_safeSetObject:@(self.deviceSize.width) forKey:@"device_screen_width_px"];
    [d adx_safeSetObject:@(self.deviceSize.height) forKey:@"device_screen_height_px"];
    
    [d adx_safeSetObject:@([self geoLat]) forKey:@"geo_lat"];
    [d adx_safeSetObject:@([self geoLon]) forKey:@"geo_lon"];
    [d adx_safeSetObject:@([self geoAccuracy]) forKey:@"geo_accuracy_radius_meters"];
    
    [d adx_safeSetObject:@([self performanceDurationMs]) forKey:@"perf_duration_ms"];
    
    [d adx_safeSetObject:@([self networkType]) forKey:@"network_type"];
    [d adx_safeSetObject:[self networkOperatorCode] forKey:@"network_operator_code"];
    [d adx_safeSetObject:[self networkOperatorName] forKey:@"network_operator_name"];
    [d adx_safeSetObject:[self networkISOCountryCode] forKey:@"network_iso_country_code"];
    [d adx_safeSetObject:[self networkSimISOCountryCode] forKey:@"network_sim_iso_country_code"];
    
    [d adx_safeSetObject:[self requestId] forKey:@"req_id"];
    [d adx_safeSetObject:@([self requestStatusCode]) forKey:@"req_status_code"];
    [d adx_safeSetObject:[self requestURI] forKey:@"req_uri"];
    [d adx_safeSetObject:@([self requestRetries]) forKey:@"req_retries"];
    
    [d adx_safeSetObject:@([self timestampAsEpoch]) forKey:@"timestaadx_client"];
    
    return d;
}

- (NSString *)serialize
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:[self asDictionary] options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
 
    return jsonString;
}

- (void)recordEndTime
{
    NSDate *currentTime = [NSDate date];
    NSTimeInterval durationMs = [currentTime timeIntervalSinceDate:self.timestamp] * 1000.0;
    
    self.performanceDurationMs = durationMs;
}

- (void)setRequestPropertiesWithConfiguration:(ADXAdConfiguration *)configuration
{
    [self setEventName:ADXLogEventNameAdRequest];
    [self setEventCategory:ADXLogEventCategoryRequests];

    [self setAdType:configuration.headerAdType];
    [self setAdCreativeId:configuration.creativeId];
    [self setAdNetworkType:configuration.networkType];
    [self setAdSize:configuration.preferredSize];

    NSDictionary *failURLQueryParameters = ADXDictionaryFromQueryString([configuration.failoverURL query]);
    [self setRequestId:failURLQueryParameters[@"request_id"]];
    [self setAdUnitId:failURLQueryParameters[@"id"]];
    [self setViewId:failURLQueryParameters[@"viewid"]];
}

@end
