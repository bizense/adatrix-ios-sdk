//
//  ADXLogEventRecorder.h
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXLogEvent;

void ADXAddLogEvent(ADXLogEvent *event);

@interface ADXLogEventRecorder : NSObject

- (void)addEvent:(ADXLogEvent *)event;

@end