//
//  ADXRetryingHTTPOperation.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ADXQRunLoopOperation.h"

extern NSString * const ADXRetryingHTTPOperationErrorDomain;

typedef enum {
    ADXRetryingHTTPOperationReceivedNonRetryResponse = -1000,
    ADXRetryingHTTPOperationExceededRetryLimit = -1001
} ADXRetryingHTTPOperationErrorCode;

////////////////////////////////////////////////////////////////////////////////////////////////////

// Adapted from QHTTPOperation / RetryingHTTPOperation in Apple's MVCNetworking sample code.

@interface ADXRetryingHTTPOperation : ADXQRunLoopOperation

// Things that are configured via the init method and can't be changed.
@property (copy, readonly) NSURLRequest *request;

// Things that are only meaningful after the operation is finished.
@property (copy, readonly) NSHTTPURLResponse *lastResponse;
@property (strong, readonly) NSMutableData *lastReceivedData;

- (instancetype)initWithRequest:(NSURLRequest *)request;

@end
