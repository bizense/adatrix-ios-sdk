//
//  ADXLogEventCommunicator.h
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADXLogEventCommunicator : NSObject

- (void)sendEvents:(NSArray *)events;
- (BOOL)isOverLimit;

@end
