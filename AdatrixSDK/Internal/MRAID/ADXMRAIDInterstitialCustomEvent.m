//
//  ADXMRAIDInterstitialCustomEvent.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXMRAIDInterstitialCustomEvent.h"
#import "ADXInstanceProvider.h"
#import "ADXLogging.h"

@interface ADXMRAIDInterstitialCustomEvent ()

@property (nonatomic, strong) ADXMRAIDInterstitialViewController *interstitial;

@end

@implementation ADXMRAIDInterstitialCustomEvent

@synthesize interstitial = _interstitial;
@dynamic delegate;

- (void)requestInterstitialWithCustomEventInfo:(NSDictionary *)info
{
    ADXLogInfo(@"Loading Adatrix MRAID interstitial");
    self.interstitial = [[ADXInstanceProvider sharedProvider] buildADXMRAIDInterstitialViewControllerWithDelegate:self
                                                                                                  configuration:[self.delegate configuration]];

    // The MRAID ad view will handle the close button so we don't need the ADXInterstitialViewController's close button.
    [self.interstitial setCloseButtonStyle:ADXInterstitialCloseButtonStyleAlwaysHidden];
    [self.interstitial startLoading];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)controller
{
    [self.interstitial presentInterstitialFromViewController:controller];
}

#pragma mark - ADXMRAIDInterstitialViewControllerDelegate

- (CLLocation *)location
{
    return [self.delegate location];
}

- (NSString *)adUnitId
{
    return [self.delegate adUnitId];
}

- (NSString *)viewId
{
    return [self.delegate viewId];
}

- (void)interstitialDidLoadAd:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial did load");
    [self.delegate interstitialCustomEvent:self didLoadAd:self.interstitial];
}

- (void)interstitialDidFailToLoadAd:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial did fail");
    [self.delegate interstitialCustomEvent:self didFailToLoadAdWithError:nil];
}

- (void)interstitialWillAppear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial will appear");
    [self.delegate interstitialCustomEventWillAppear:self];
}

- (void)interstitialDidAppear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial did appear");
    [self.delegate interstitialCustomEventDidAppear:self];
}

- (void)interstitialWillDisappear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial will disappear");
    [self.delegate interstitialCustomEventWillDisappear:self];
}

- (void)interstitialDidDisappear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial did disappear");
    [self.delegate interstitialCustomEventDidDisappear:self];

    // Deallocate the interstitial as we don't need it anymore. If we don't deallocate the interstitial after dismissal,
    // then the html in the webview will continue to run which could lead to bugs such as continuing to play the sound of an inline
    // video since the app may hold onto the interstitial ad controller. Moreover, we keep an array of controllers around as well.
    self.interstitial = nil;
}

- (void)interstitialDidReceiveTapEvent:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial did receive tap event");
    [self.delegate interstitialCustomEventDidReceiveTapEvent:self];
}

- (void)interstitialWillLeaveApplication:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix MRAID interstitial will leave application");
    [self.delegate interstitialCustomEventWillLeaveApplication:self];
}

@end
