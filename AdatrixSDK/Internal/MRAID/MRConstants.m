//
//  MRConstants.m
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *const kOrientationPropertyForceOrientationPortraitKey = @"portrait";
NSString *const kOrientationPropertyForceOrientationLandscapeKey = @"landscape";
NSString *const kOrientationPropertyForceOrientationNoneKey = @"none";
