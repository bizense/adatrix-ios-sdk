//
//  ADXMRAIDBannerCustomEvent.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXBannerCustomEvent.h"
#import "ADXPrivateBannerCustomEventDelegate.h"

@interface ADXMRAIDBannerCustomEvent : ADXBannerCustomEvent

@property (nonatomic, weak) id<ADXPrivateBannerCustomEventDelegate> delegate;

@end
