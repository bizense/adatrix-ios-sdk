//
//  MRError.h
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const AdatrixMRAIDAdsSDKDomain;

enum {
    MRErrorMRAIDJSNotFound
};
typedef NSInteger MRErrorCode;
