//
//  ADXMRAIDBannerCustomEvent.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXMRAIDBannerCustomEvent.h"
#import "ADXLogging.h"
#import "ADXAdConfiguration.h"
#import "ADXInstanceProvider.h"
#import "MRController.h"

@interface ADXMRAIDBannerCustomEvent () <MRControllerDelegate>

@property (nonatomic, strong) MRController *mraidController;

@end

@implementation ADXMRAIDBannerCustomEvent
@synthesize delegate;

- (void)requestAdWithSize:(CGSize)size customEventInfo:(NSDictionary *)info
{
    ADXLogInfo(@"Loading Adatrix MRAID banner");
    ADXAdConfiguration *configuration = [self.delegate configuration];

    CGRect adViewFrame = CGRectZero;
    if ([configuration hasPreferredSize]) {
        adViewFrame = CGRectMake(0, 0, configuration.preferredSize.width,
                                 configuration.preferredSize.height);
    }

    self.mraidController = [[ADXInstanceProvider sharedProvider] buildBannerMRControllerWithFrame:adViewFrame delegate:self];
    [self.mraidController loadAdWithConfiguration:configuration];
}

#pragma mark - MRControllerDelegate

- (CLLocation *)location
{
    return [self.delegate location];
}

- (NSString *)adUnitId
{
    return [self.delegate adUnitId];
}
- (NSString *)viewId
{
    return [self.delegate viewId];
}

- (ADXAdConfiguration *)adConfiguration
{
    return [self.delegate configuration];
}

- (UIViewController *)viewControllerForPresentingModalView
{
    return [self.delegate viewControllerForPresentingModalView];
}

- (void)adDidLoad:(UIView *)adView
{
    ADXLogInfo(@"Adatrix MRAID banner did load");
    [self.delegate bannerCustomEvent:self didLoadAd:adView];
}

- (void)adDidFailToLoad:(UIView *)adView
{
    ADXLogInfo(@"Adatrix MRAID banner did fail");
    [self.delegate bannerCustomEvent:self didFailToLoadAdWithError:nil];
}

- (void)closeButtonPressed
{
    //don't care
}

- (void)appShouldSuspendForAd:(UIView *)adView
{
    ADXLogInfo(@"Adatrix MRAID banner will begin action");
    [self.delegate bannerCustomEventWillBeginAction:self];
}

- (void)appShouldResumeFromAd:(UIView *)adView
{
    ADXLogInfo(@"Adatrix MRAID banner did end action");
    [self.delegate bannerCustomEventDidFinishAction:self];
}

@end
