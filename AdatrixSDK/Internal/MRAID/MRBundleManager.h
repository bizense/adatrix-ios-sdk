//
//  MRBundleManager.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRBundleManager : NSObject

+ (MRBundleManager *)sharedManager;
- (NSString *)mraidPath;

@end
