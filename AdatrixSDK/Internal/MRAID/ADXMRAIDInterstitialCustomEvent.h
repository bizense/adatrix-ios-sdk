//
//  ADXMRAIDInterstitialCustomEvent.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXInterstitialCustomEvent.h"
#import "ADXMRAIDInterstitialViewController.h"
#import "ADXPrivateInterstitialCustomEventDelegate.h"

@interface ADXMRAIDInterstitialCustomEvent : ADXInterstitialCustomEvent <ADXInterstitialViewControllerDelegate>

@property (nonatomic, weak) id<ADXPrivateInterstitialCustomEventDelegate> delegate;

@end
