//
//  MRBundleManager.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "MRBundleManager.h"

@implementation MRBundleManager

static MRBundleManager *sharedManager = nil;

+ (MRBundleManager *)sharedManager
{
    if (!sharedManager) {
        sharedManager = [[MRBundleManager alloc] init];
    }
    return sharedManager;
}

- (NSString *)mraidPath
{
    NSBundle *parentBundle = [NSBundle mainBundle];

#ifdef ADX_FABRIC
    // If we're in Fabric, all resources live inside Adatrix.bundle.
    NSString *parentBundlePath = [[NSBundle mainBundle] pathForResource:@"Adatrix" ofType:@"bundle"];
    parentBundle = [NSBundle bundleWithPath:parentBundlePath];
#endif

    NSString *mraidBundlePath = [parentBundle pathForResource:@"MRAID" ofType:@"bundle"];
    NSBundle *mraidBundle = [NSBundle bundleWithPath:mraidBundlePath];
    return [mraidBundle pathForResource:@"mraid" ofType:@"js"];
}

@end
