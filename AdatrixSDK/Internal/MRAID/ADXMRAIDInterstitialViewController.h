//
//  ADXMRAIDInterstitialViewController.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXInterstitialViewController.h"
#import "ADXForceableOrientationProtocol.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ADXMRAIDInterstitialViewControllerDelegate;
@class ADXAdConfiguration;

////////////////////////////////////////////////////////////////////////////////////////////////////

@interface ADXMRAIDInterstitialViewController : ADXInterstitialViewController <ADXForceableOrientationProtocol>

- (id)initWithAdConfiguration:(ADXAdConfiguration *)configuration;
- (void)startLoading;

@end

