//
//  ADXForceableOrientationProtocol.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADXForceableOrientationProtocol <NSObject>

/**
 * An orientation mask that defines the orientations the view controller supports.
 * This cannot force a change in orientation though.
 */
- (void)setSupportedOrientationMask:(UIInterfaceOrientationMask)supportedOrientationMask;

@end
