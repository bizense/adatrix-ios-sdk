//
//  ADXBannerAdManagerDelegate.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXAdView;
@protocol ADXAdViewDelegate;

@protocol ADXBannerAdManagerDelegate <NSObject>

- (NSString *)adUnitId;
- (NSString *)viewId;
- (ADXNativeAdOrientation)allowedNativeAdsOrientation;
- (ADXAdView *)banner;
- (id<ADXAdViewDelegate>)bannerDelegate;
- (CGSize)containerSize;
- (BOOL)ignoresAutorefresh;
- (NSString *)keywords;
- (CLLocation *)location;
- (BOOL)isTesting;
- (UIViewController *)viewControllerForPresentingModalView;

- (void)invalidateContentView;

- (void)managerDidLoadAd:(UIView *)ad;
- (void)managerDidFailToLoadAd;
- (void)userActionWillBegin;
- (void)userActionDidFinish;
- (void)userWillLeaveApplication;

@end
