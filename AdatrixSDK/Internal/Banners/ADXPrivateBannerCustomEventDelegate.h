//
//  ADXPrivateBannerCustomEventDelegate.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXBannerCustomEventDelegate.h"

@class ADXAdConfiguration;

@protocol ADXPrivateBannerCustomEventDelegate <ADXBannerCustomEventDelegate>

- (NSString *)adUnitId;
- (NSString *)viewId;
- (ADXAdConfiguration *)configuration;
- (id)bannerDelegate;

@end
