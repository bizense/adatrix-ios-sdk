//
//  ADXBaseBannerAdapter.h
//  Adatrix
//
//  Created by Nafis Jamal on 1/19/11.
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ADXAdView.h"

@protocol ADXBannerAdapterDelegate;
@class ADXAdConfiguration;

@interface ADXBaseBannerAdapter : NSObject
{
    id<ADXBannerAdapterDelegate> __weak _delegate;
}

@property (nonatomic, weak) id<ADXBannerAdapterDelegate> delegate;
@property (nonatomic, copy) NSURL *impressionTrackingURL;
@property (nonatomic, copy) NSURL *clickTrackingURL;

- (id)initWithDelegate:(id<ADXBannerAdapterDelegate>)delegate;

/*
 * Sets the adapter's delegate to nil.
 */
- (void)unregisterDelegate;

/*
 * -_getAdWithConfiguration creates a strong reference to self before calling
 * -getAdWithConfiguration to prevent the adapter from being prematurely deallocated.
 */
- (void)getAdWithConfiguration:(ADXAdConfiguration *)configuration containerSize:(CGSize)size;
- (void)_getAdWithConfiguration:(ADXAdConfiguration *)configuration containerSize:(CGSize)size;

- (void)didStopLoading;
- (void)didDisplayAd;

/*
 * Your subclass should implement this method if your native ads vary depending on orientation.
 */
- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation;

- (void)trackImpression;

- (void)trackClick;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ADXBannerAdapterDelegate

@required

- (ADXAdView *)banner;
- (id<ADXAdViewDelegate>)bannerDelegate;
- (UIViewController *)viewControllerForPresentingModalView;
- (ADXNativeAdOrientation)allowedNativeAdsOrientation;
- (CLLocation *)location;

/*
 * These callbacks notify you that the adapter (un)successfully loaded an ad.
 */
- (void)adapter:(ADXBaseBannerAdapter *)adapter didFailToLoadAdWithError:(NSError *)error;
- (void)adapter:(ADXBaseBannerAdapter *)adapter didFinishLoadingAd:(UIView *)ad;

/*
 * These callbacks notify you that the user interacted (or stopped interacting) with the native ad.
 */
- (void)userActionWillBeginForAdapter:(ADXBaseBannerAdapter *)adapter;
- (void)userActionDidFinishForAdapter:(ADXBaseBannerAdapter *)adapter;

/*
 * This callback notifies you that user has tapped on an ad which will cause them to leave the
 * current application (e.g. the ad action opens the iTunes store, Mobile Safari, etc).
 */
- (void)userWillLeaveApplicationFromAdapter:(ADXBaseBannerAdapter *)adapter;

@end
