//
//  ADXBannerAdManager.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXAdServerCommunicator.h"
#import "ADXBaseBannerAdapter.h"

@protocol ADXBannerAdManagerDelegate;

@interface ADXBannerAdManager : NSObject <ADXAdServerCommunicatorDelegate, ADXBannerAdapterDelegate>

@property (nonatomic, weak) id<ADXBannerAdManagerDelegate> delegate;

- (id)initWithDelegate:(id<ADXBannerAdManagerDelegate>)delegate;

- (void)loadAd;
- (void)forceRefreshAd;
- (void)stopAutomaticallyRefreshingContents;
- (void)startAutomaticallyRefreshingContents;
- (void)rotateToOrientation:(UIInterfaceOrientation)orientation;

// Deprecated.
- (void)customEventDidLoadAd;
- (void)customEventDidFailToLoadAd;
- (void)customEventActionWillBegin;
- (void)customEventActionDidEnd;

@end
