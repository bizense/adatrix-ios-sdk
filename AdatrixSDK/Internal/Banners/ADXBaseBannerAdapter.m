//
//  ADXBaseBannerAdapter.m
//  Adatrix
//
//  Created by Nafis Jamal on 1/19/11.
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXBaseBannerAdapter.h"
#import "ADXConstants.h"

#import "ADXAdConfiguration.h"
#import "ADXLogging.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXAnalyticsTracker.h"
#import "ADXTimer.h"

@interface ADXBaseBannerAdapter ()

@property (nonatomic, strong) ADXAdConfiguration *configuration;
@property (nonatomic, strong) ADXTimer *timeoutTimer;

- (void)startTimeoutTimer;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation ADXBaseBannerAdapter

@synthesize delegate = _delegate;
@synthesize configuration = _configuration;
@synthesize timeoutTimer = _timeoutTimer;

- (id)initWithDelegate:(id<ADXBannerAdapterDelegate>)delegate
{
    if (self = [super init]) {
        self.delegate = delegate;
    }
    return self;
}

- (void)dealloc
{
    [self unregisterDelegate];
    [self.timeoutTimer invalidate];
}

- (void)unregisterDelegate
{
    self.delegate = nil;
}

#pragma mark - Requesting Ads

- (void)getAdWithConfiguration:(ADXAdConfiguration *)configuration containerSize:(CGSize)size
{
    // To be implemented by subclasses.
    [self doesNotRecognizeSelector:_cmd];
}

- (void)_getAdWithConfiguration:(ADXAdConfiguration *)configuration containerSize:(CGSize)size
{
    self.configuration = configuration;

    [self startTimeoutTimer];

    ADXBaseBannerAdapter *strongSelf = self;
    [strongSelf getAdWithConfiguration:configuration containerSize:size];
}

- (void)didStopLoading
{
    [self.timeoutTimer invalidate];
}

- (void)didDisplayAd
{
    [self trackImpression];
}

- (void)startTimeoutTimer
{
    NSTimeInterval timeInterval = (self.configuration && self.configuration.adTimeoutInterval >= 0) ?
    self.configuration.adTimeoutInterval : BANNER_TIMEOUT_INTERVAL;

    if (timeInterval > 0) {
        self.timeoutTimer = [[ADXCoreInstanceProvider sharedProvider] buildADXTimerWithTimeInterval:timeInterval
                                                                                       target:self
                                                                                     selector:@selector(timeout)
                                                                                      repeats:NO];

        [self.timeoutTimer scheduleNow];
    }
}

- (void)timeout
{
    [self.delegate adapter:self didFailToLoadAdWithError:nil];
}

#pragma mark - Rotation

- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation
{
    // Do nothing by default. Subclasses can override.
    ADXLogDebug(@"rotateToOrientation %d called for adapter %@ (%p)",
          newOrientation, NSStringFromClass([self class]), self);
}

#pragma mark - Metrics

- (void)trackImpression
{
    [[[ADXCoreInstanceProvider sharedProvider] sharedADXAnalyticsTracker] trackImpressionForConfiguration:self.configuration];
}

- (void)trackClick
{
    [[[ADXCoreInstanceProvider sharedProvider] sharedADXAnalyticsTracker] trackClickForConfiguration:self.configuration];
}

@end
