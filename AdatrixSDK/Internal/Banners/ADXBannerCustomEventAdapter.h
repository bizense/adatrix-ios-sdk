//
//  ADXBannerCustomEventAdapter.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXBaseBannerAdapter.h"

#import "ADXPrivateBannerCustomEventDelegate.h"

@class ADXBannerCustomEvent;

@interface ADXBannerCustomEventAdapter : ADXBaseBannerAdapter <ADXPrivateBannerCustomEventDelegate>

@end
