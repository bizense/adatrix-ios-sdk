//
//  ADXInstanceProvider.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXGlobal.h"
#import "ADXCoreInstanceProvider.h"

// Banners
@class ADXBannerAdManager;
@protocol ADXBannerAdManagerDelegate;
@class ADXBaseBannerAdapter;
@protocol ADXBannerAdapterDelegate;
@class ADXBannerCustomEvent;
@protocol ADXBannerCustomEventDelegate;

// Interstitials
@class ADXInterstitialAdManager;
@protocol ADXInterstitialAdManagerDelegate;
@class ADXBaseInterstitialAdapter;
@protocol ADXInterstitialAdapterDelegate;
@class ADXInterstitialCustomEvent;
@protocol ADXInterstitialCustomEventDelegate;
@class ADXHTMLInterstitialViewController;
@class ADXMRAIDInterstitialViewController;
@protocol ADXInterstitialViewControllerDelegate;

// Rewarded Video
@class ADXRewardedVideoAdManager;
@class ADXRewardedVideoAdapter;
@class ADXRewardedVideoCustomEvent;
@protocol ADXRewardedVideoAdapterDelegate;
@protocol ADXRewardedVideoCustomEventDelegate;
@protocol ADXRewardedVideoAdManagerDelegate;

// HTML Ads
@class ADXAdWebView;
@class ADXAdWebViewAgent;
@protocol ADXAdWebViewAgentDelegate;

// MRAID
@class MRController;
@protocol MRControllerDelegate;
@class ADXClosableView;
@class MRBridge;
@protocol MRBridgeDelegate;
@protocol ADXClosableViewDelegate;
@class MRBundleManager;
@class MRCalendarManager;
@protocol MRCalendarManagerDelegate;
@class EKEventStore;
@class EKEventEditViewController;
@protocol EKEventEditViewDelegate;
@class MRPictureManager;
@protocol MRPictureManagerDelegate;
@class MRImageDownloader;
@protocol MRImageDownloaderDelegate;
@class MRVideoPlayerManager;
@protocol MRVideoPlayerManagerDelegate;
@class MPMoviePlayerViewController;
@class MRNativeCommandHandler;
@protocol MRNativeCommandHandlerDelegate;

//Native
@protocol ADXNativeCustomEventDelegate;
@class ADXNativeCustomEvent;
@class ADXNativeAdSource;
@protocol ADXNativeAdSourceDelegate;
@class ADXNativePositionSource;
@class ADXStreamAdPlacementData;
@class ADXStreamAdPlacer;
@class ADXAdPositioning;

@interface ADXInstanceProvider : NSObject

+(instancetype)sharedProvider;
- (id)singletonForClass:(Class)klass provider:(ADXSingletonProviderBlock)provider;
- (id)singletonForClass:(Class)klass provider:(ADXSingletonProviderBlock)provider context:(id)context;

#pragma mark - Banners
- (ADXBannerAdManager *)buildADXBannerAdManagerWithDelegate:(id<ADXBannerAdManagerDelegate>)delegate;
- (ADXBaseBannerAdapter *)buildBannerAdapterForConfiguration:(ADXAdConfiguration *)configuration
                                                   delegate:(id<ADXBannerAdapterDelegate>)delegate;
- (ADXBannerCustomEvent *)buildBannerCustomEventFromCustomClass:(Class)customClass
                                                      delegate:(id<ADXBannerCustomEventDelegate>)delegate;

#pragma mark - Interstitials
- (ADXInterstitialAdManager *)buildADXInterstitialAdManagerWithDelegate:(id<ADXInterstitialAdManagerDelegate>)delegate;
- (ADXBaseInterstitialAdapter *)buildInterstitialAdapterForConfiguration:(ADXAdConfiguration *)configuration
                                                               delegate:(id<ADXInterstitialAdapterDelegate>)delegate;
- (ADXInterstitialCustomEvent *)buildInterstitialCustomEventFromCustomClass:(Class)customClass
                                                                  delegate:(id<ADXInterstitialCustomEventDelegate>)delegate;
- (ADXHTMLInterstitialViewController *)buildADXHTMLInterstitialViewControllerWithDelegate:(id<ADXInterstitialViewControllerDelegate>)delegate
                                                                        orientationType:(ADXInterstitialOrientationType)type;
- (ADXMRAIDInterstitialViewController *)buildADXMRAIDInterstitialViewControllerWithDelegate:(id<ADXInterstitialViewControllerDelegate>)delegate
                                                                            configuration:(ADXAdConfiguration *)configuration;

#pragma mark - Rewarded Video
- (ADXRewardedVideoAdManager *)buildRewardedVideoAdManagerWithAdUnitID:(NSString *)adUnitID viewID:(NSString *)viewID delegate:(id<ADXRewardedVideoAdManagerDelegate>)delegate;
- (ADXRewardedVideoAdapter *)buildRewardedVideoAdapterWithDelegate:(id<ADXRewardedVideoAdapterDelegate>)delegate;
- (ADXRewardedVideoCustomEvent *)buildRewardedVideoCustomEventFromCustomClass:(Class)customClass delegate:(id<ADXRewardedVideoCustomEventDelegate>)delegate;


#pragma mark - HTML Ads
- (ADXAdWebView *)buildADXAdWebViewWithFrame:(CGRect)frame
                                  delegate:(id<UIWebViewDelegate>)delegate;
- (ADXAdWebViewAgent *)buildADXAdWebViewAgentWithAdWebViewFrame:(CGRect)frame
                                                     delegate:(id<ADXAdWebViewAgentDelegate>)delegate;

#pragma mark - MRAID
- (ADXClosableView *)buildMRAIDADXClosableViewWithFrame:(CGRect)frame webView:(UIWebView *)webView delegate:(id<ADXClosableViewDelegate>)delegate;
- (MRBundleManager *)buildMRBundleManager;
- (MRController *)buildBannerMRControllerWithFrame:(CGRect)frame delegate:(id<MRControllerDelegate>)delegate;
- (MRController *)buildInterstitialMRControllerWithFrame:(CGRect)frame delegate:(id<MRControllerDelegate>)delegate;
- (MRBridge *)buildMRBridgeWithWebView:(UIWebView *)webView delegate:(id<MRBridgeDelegate>)delegate;
- (UIWebView *)buildUIWebViewWithFrame:(CGRect)frame;
- (MRCalendarManager *)buildMRCalendarManagerWithDelegate:(id<MRCalendarManagerDelegate>)delegate;
- (EKEventEditViewController *)buildEKEventEditViewControllerWithEditViewDelegate:(id<EKEventEditViewDelegate>)editViewDelegate;
- (EKEventStore *)buildEKEventStore;
- (MRPictureManager *)buildMRPictureManagerWithDelegate:(id<MRPictureManagerDelegate>)delegate;
- (MRImageDownloader *)buildMRImageDownloaderWithDelegate:(id<MRImageDownloaderDelegate>)delegate;
- (MRVideoPlayerManager *)buildMRVideoPlayerManagerWithDelegate:(id<MRVideoPlayerManagerDelegate>)delegate;
- (MPMoviePlayerViewController *)buildMPMoviePlayerViewControllerWithURL:(NSURL *)URL;
- (MRNativeCommandHandler *)buildMRNativeCommandHandlerWithDelegate:(id<MRNativeCommandHandlerDelegate>)delegate;

#pragma mark - Native

- (ADXNativeCustomEvent *)buildNativeCustomEventFromCustomClass:(Class)customClass
                                                      delegate:(id<ADXNativeCustomEventDelegate>)delegate;
- (ADXNativeAdSource *)buildNativeAdSourceWithDelegate:(id<ADXNativeAdSourceDelegate>)delegate;
- (ADXNativePositionSource *)buildNativePositioningSource;
- (ADXStreamAdPlacementData *)buildStreamAdPlacementDataWithPositioning:(ADXAdPositioning *)positioning;
- (ADXStreamAdPlacer *)buildStreamAdPlacerWithViewController:(UIViewController *)controller adPositioning:(ADXAdPositioning *)positioning defaultAdRenderingClass:defaultAdRenderingClass;

@end
