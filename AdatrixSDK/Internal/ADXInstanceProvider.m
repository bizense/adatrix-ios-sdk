//
//  ADXInstanceProvider.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXInstanceProvider.h"
#import "ADXAdWebView.h"
#import "ADXAdWebViewAgent.h"
#import "ADXInterstitialAdManager.h"
#import "ADXInterstitialCustomEventAdapter.h"
#import "ADXLegacyInterstitialCustomEventAdapter.h"
#import "ADXHTMLInterstitialViewController.h"
#import "ADXMRAIDInterstitialViewController.h"
#import "ADXInterstitialCustomEvent.h"
#import "ADXBaseBannerAdapter.h"
#import "ADXBannerCustomEventAdapter.h"
#import "ADXLegacyBannerCustomEventAdapter.h"
#import "ADXBannerCustomEvent.h"
#import "ADXBannerAdManager.h"
#import "ADXLogging.h"
#import "MRImageDownloader.h"
#import "MRBundleManager.h"
#import "MRCalendarManager.h"
#import "MRPictureManager.h"
#import "MRVideoPlayerManager.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ADXNativeCustomEvent.h"
#import "ADXNativeAdSource.h"
#import "ADXNativePositionSource.h"
#import "ADXStreamAdPlacementData.h"
#import "ADXStreamAdPlacer.h"
#import "MRNativeCommandHandler.h"
#import "MRBridge.h"
#import "MRController.h"
#import "ADXClosableView.h"
#import "ADXRewardedVideoAdManager.h"
#import "ADXRewardedVideoAdapter.h"
#import "ADXRewardedVideoCustomEvent.h"

@interface ADXInstanceProvider ()

// A nested dictionary. The top-level dictionary maps Class objects to second-level dictionaries.
// The second level dictionaries map identifiers to singleton objects.
//
// An example:
//  {
//      SomeClass:
//      {
//          @"default": <singleton_a>
//          @"other_context": <singleton_b>
//      }
//  }
//
@property (nonatomic, strong) NSMutableDictionary *singletons;

@end


@implementation ADXInstanceProvider

static ADXInstanceProvider *sharedAdProvider = nil;

+ (instancetype)sharedProvider
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedAdProvider = [[self alloc] init];
    });

    return sharedAdProvider;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.singletons = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)singletonForClass:(Class)klass provider:(ADXSingletonProviderBlock)provider
{
    return [self singletonForClass:klass provider:provider context:@"default"];
}

- (id)singletonForClass:(Class)klass provider:(ADXSingletonProviderBlock)provider context:(id)context
{
    id singleton = [[self.singletons objectForKey:klass] objectForKey:context];
    if (!singleton) {
        singleton = provider();
        NSMutableDictionary *singletonsForClass = [self.singletons objectForKey:klass];
        if (!singletonsForClass) {
            NSMutableDictionary *singletonsForContext = [NSMutableDictionary dictionaryWithObjectsAndKeys:singleton, context, nil];
            [self.singletons setObject:singletonsForContext forKey:(id<NSCopying>)klass];
        } else {
            [singletonsForClass setObject:singleton forKey:context];
        }
    }
    return singleton;
}

#pragma mark - Banners

- (ADXBannerAdManager *)buildADXBannerAdManagerWithDelegate:(id<ADXBannerAdManagerDelegate>)delegate
{
    return [(ADXBannerAdManager *)[ADXBannerAdManager alloc] initWithDelegate:delegate];
}

- (ADXBaseBannerAdapter *)buildBannerAdapterForConfiguration:(ADXAdConfiguration *)configuration
                                                   delegate:(id<ADXBannerAdapterDelegate>)delegate
{
    if (configuration.customEventClass) {
        return [(ADXBannerCustomEventAdapter *)[ADXBannerCustomEventAdapter alloc] initWithDelegate:delegate];
    } else if (configuration.customSelectorName) {
        return [(ADXLegacyBannerCustomEventAdapter *)[ADXLegacyBannerCustomEventAdapter alloc] initWithDelegate:delegate];
    }

    return nil;
}

- (ADXBannerCustomEvent *)buildBannerCustomEventFromCustomClass:(Class)customClass
                                                      delegate:(id<ADXBannerCustomEventDelegate>)delegate
{
    ADXBannerCustomEvent *customEvent = [[customClass alloc] init];
    if (![customEvent isKindOfClass:[ADXBannerCustomEvent class]]) {
        ADXLogError(@"**** Custom Event Class: %@ does not extend ADXBannerCustomEvent ****", NSStringFromClass(customClass));
        return nil;
    }
    customEvent.delegate = delegate;
    return customEvent;
}

#pragma mark - Interstitials

- (ADXInterstitialAdManager *)buildADXInterstitialAdManagerWithDelegate:(id<ADXInterstitialAdManagerDelegate>)delegate
{
    return [(ADXInterstitialAdManager *)[ADXInterstitialAdManager alloc] initWithDelegate:delegate];
}


- (ADXBaseInterstitialAdapter *)buildInterstitialAdapterForConfiguration:(ADXAdConfiguration *)configuration
                                                               delegate:(id<ADXInterstitialAdapterDelegate>)delegate
{
    if (configuration.customEventClass) {
        return [(ADXInterstitialCustomEventAdapter *)[ADXInterstitialCustomEventAdapter alloc] initWithDelegate:delegate];
    } else if (configuration.customSelectorName) {
        return [(ADXLegacyInterstitialCustomEventAdapter *)[ADXLegacyInterstitialCustomEventAdapter alloc] initWithDelegate:delegate];
    }
    NSLog(@"here returning nil");
    return nil;
}

- (ADXInterstitialCustomEvent *)buildInterstitialCustomEventFromCustomClass:(Class)customClass
                                                                  delegate:(id<ADXInterstitialCustomEventDelegate>)delegate
{
    ADXInterstitialCustomEvent *customEvent = [[customClass alloc] init];
    if (![customEvent isKindOfClass:[ADXInterstitialCustomEvent class]]) {
        ADXLogError(@"**** Custom Event Class: %@ does not extend ADXInterstitialCustomEvent ****", NSStringFromClass(customClass));
        return nil;
    }
    if ([customEvent respondsToSelector:@selector(customEventDidUnload)]) {
        ADXLogWarn(@"**** Custom Event Class: %@ implements the deprecated -customEventDidUnload method.  This is no longer called.  Use -dealloc for cleanup instead ****", NSStringFromClass(customClass));
    }
    customEvent.delegate = delegate;
    return customEvent;
}

- (ADXHTMLInterstitialViewController *)buildADXHTMLInterstitialViewControllerWithDelegate:(id<ADXInterstitialViewControllerDelegate>)delegate
                                                                        orientationType:(ADXInterstitialOrientationType)type
{
    ADXHTMLInterstitialViewController *controller = [[ADXHTMLInterstitialViewController alloc] init];
    controller.delegate = delegate;
    controller.orientationType = type;
    return controller;
}

- (ADXMRAIDInterstitialViewController *)buildADXMRAIDInterstitialViewControllerWithDelegate:(id<ADXInterstitialViewControllerDelegate>)delegate
                                                                            configuration:(ADXAdConfiguration *)configuration
{
    ADXMRAIDInterstitialViewController *controller = [[ADXMRAIDInterstitialViewController alloc] initWithAdConfiguration:configuration];
    controller.delegate = delegate;
    return controller;
}

#pragma mark - Rewarded Video

- (ADXRewardedVideoAdManager *)buildRewardedVideoAdManagerWithAdUnitID:(NSString *)adUnitID viewID:(NSString *)viewID delegate:(id<ADXRewardedVideoAdManagerDelegate>)delegate
{
    return [[ADXRewardedVideoAdManager alloc] initWithAdUnitID:adUnitID viewID:viewID delegate:delegate];
}

- (ADXRewardedVideoAdapter *)buildRewardedVideoAdapterWithDelegate:(id<ADXRewardedVideoAdapterDelegate>)delegate
{
    return [[ADXRewardedVideoAdapter alloc] initWithDelegate:delegate];
}

- (ADXRewardedVideoCustomEvent *)buildRewardedVideoCustomEventFromCustomClass:(Class)customClass delegate:(id<ADXRewardedVideoCustomEventDelegate>)delegate
{
    ADXRewardedVideoCustomEvent *customEvent = [[customClass alloc] init];

    if (![customEvent isKindOfClass:[ADXRewardedVideoCustomEvent class]]) {
        ADXLogError(@"**** Custom Event Class: %@ does not extend ADXRewardedVideoCustomEvent ****", NSStringFromClass(customClass));
        return nil;
    }

    customEvent.delegate = delegate;
    return customEvent;
}

#pragma mark - HTML Ads

- (ADXAdWebView *)buildADXAdWebViewWithFrame:(CGRect)frame delegate:(id<UIWebViewDelegate>)delegate
{
    ADXAdWebView *webView = [[ADXAdWebView alloc] initWithFrame:frame];
    webView.delegate = delegate;
    return webView;
}

- (ADXAdWebViewAgent *)buildADXAdWebViewAgentWithAdWebViewFrame:(CGRect)frame delegate:(id<ADXAdWebViewAgentDelegate>)delegate
{
    return [[ADXAdWebViewAgent alloc] initWithAdWebViewFrame:frame delegate:delegate];
}

#pragma mark - MRAID

- (ADXClosableView *)buildMRAIDADXClosableViewWithFrame:(CGRect)frame webView:(UIWebView *)webView delegate:(id<ADXClosableViewDelegate>)delegate
{
    ADXClosableView *adView = [[ADXClosableView alloc] initWithFrame:frame closeButtonType:ADXClosableViewCloseButtonTypeTappableWithImage];
    adView.delegate = delegate;
    adView.clipsToBounds = YES;
    webView.frame = adView.bounds;
    [adView addSubview:webView];
    return adView;
}

- (MRBundleManager *)buildMRBundleManager
{
    return [MRBundleManager sharedManager];
}

- (MRController *)buildBannerMRControllerWithFrame:(CGRect)frame delegate:(id<MRControllerDelegate>)delegate
{
    return [self buildMRControllerWithFrame:frame placementType:MRAdViewPlacementTypeInline delegate:delegate];
}

- (MRController *)buildInterstitialMRControllerWithFrame:(CGRect)frame delegate:(id<MRControllerDelegate>)delegate
{
    return [self buildMRControllerWithFrame:frame placementType:MRAdViewPlacementTypeInterstitial delegate:delegate];
}

- (MRController *)buildMRControllerWithFrame:(CGRect)frame placementType:(MRAdViewPlacementType)placementType delegate:(id<MRControllerDelegate>)delegate
{
    MRController *controller = [[MRController alloc] initWithAdViewFrame:frame adPlacementType:placementType];
    controller.delegate = delegate;
    return controller;
}

- (MRBridge *)buildMRBridgeWithWebView:(UIWebView *)webView delegate:(id<MRBridgeDelegate>)delegate
{
    MRBridge *bridge = [[MRBridge alloc] initWithWebView:webView];
    bridge.delegate = delegate;
    bridge.shouldHandleRequests = YES;
    return bridge;
}

- (UIWebView *)buildUIWebViewWithFrame:(CGRect)frame
{
    return [[UIWebView alloc] initWithFrame:frame];
}

- (MRCalendarManager *)buildMRCalendarManagerWithDelegate:(id<MRCalendarManagerDelegate>)delegate
{
    return [[MRCalendarManager alloc] initWithDelegate:delegate];
}

- (EKEventEditViewController *)buildEKEventEditViewControllerWithEditViewDelegate:(id<EKEventEditViewDelegate>)editViewDelegate
{
    EKEventEditViewController *controller = [[EKEventEditViewController alloc] init];
    controller.editViewDelegate = editViewDelegate;
    controller.eventStore = [self buildEKEventStore];
    return controller;
}

- (EKEventStore *)buildEKEventStore
{
    return [[EKEventStore alloc] init];
}

- (MRPictureManager *)buildMRPictureManagerWithDelegate:(id<MRPictureManagerDelegate>)delegate
{
    return [[MRPictureManager alloc] initWithDelegate:delegate];
}

- (MRImageDownloader *)buildMRImageDownloaderWithDelegate:(id<MRImageDownloaderDelegate>)delegate
{
    return [[MRImageDownloader alloc] initWithDelegate:delegate];
}

- (MRVideoPlayerManager *)buildMRVideoPlayerManagerWithDelegate:(id<MRVideoPlayerManagerDelegate>)delegate
{
    return [[MRVideoPlayerManager alloc] initWithDelegate:delegate];
}

- (MPMoviePlayerViewController *)buildMPMoviePlayerViewControllerWithURL:(NSURL *)URL
{
    // ImageContext used to avoid CGErrors
    // http://stackoverflow.com/questions/13203336/iphone-MPMovieplayerviewcontroller-cgcontext-errors/14669166#14669166
    UIGraphicsBeginImageContext(CGSizeMake(1,1));
    MPMoviePlayerViewController *playerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:URL];
    UIGraphicsEndImageContext();

    return playerViewController;
}

- (MRNativeCommandHandler *)buildMRNativeCommandHandlerWithDelegate:(id<MRNativeCommandHandlerDelegate>)delegate
{
    return [[MRNativeCommandHandler alloc] initWithDelegate:delegate];
}

#pragma mark - Native

- (ADXNativeCustomEvent *)buildNativeCustomEventFromCustomClass:(Class)customClass
                                                      delegate:(id<ADXNativeCustomEventDelegate>)delegate
{
    ADXNativeCustomEvent *customEvent = [[customClass alloc] init];
    if (![customEvent isKindOfClass:[ADXNativeCustomEvent class]]) {
        ADXLogError(@"**** Custom Event Class: %@ does not extend ADXNativeCustomEvent ****", NSStringFromClass(customClass));
        return nil;
    }
    customEvent.delegate = delegate;
    return customEvent;
}

- (ADXNativeAdSource *)buildNativeAdSourceWithDelegate:(id<ADXNativeAdSourceDelegate>)delegate
{
    ADXNativeAdSource *source = [ADXNativeAdSource source];
    source.delegate = delegate;
    return source;
}

- (ADXNativePositionSource *)buildNativePositioningSource
{
    return [[ADXNativePositionSource alloc] init];
}

- (ADXStreamAdPlacementData *)buildStreamAdPlacementDataWithPositioning:(ADXAdPositioning *)positioning
{
    ADXStreamAdPlacementData *placementData = [[ADXStreamAdPlacementData alloc] initWithPositioning:positioning];
    return placementData;
}

- (ADXStreamAdPlacer *)buildStreamAdPlacerWithViewController:(UIViewController *)controller adPositioning:(ADXAdPositioning *)positioning defaultAdRenderingClass:defaultAdRenderingClass
{
    return [ADXStreamAdPlacer placerWithViewController:controller adPositioning:positioning defaultAdRenderingClass:defaultAdRenderingClass];
}

@end

