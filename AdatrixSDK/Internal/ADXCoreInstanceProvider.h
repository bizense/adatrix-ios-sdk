//
//  ADXCoreInstanceProvider.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ADXGlobal.h"


@class ADXAdConfiguration;

// Fetching Ads
@class ADXAdServerCommunicator;
@protocol ADXAdServerCommunicatorDelegate;

// URL Handling
@class ADXURLResolver;
@class ADXAdDestinationDisplayAgent;
@protocol ADXAdDestinationDisplayAgentDelegate;

// Utilities
@class ADXAdAlertManager, ADXAdAlertGestureRecognizer;
@class ADXAnalyticsTracker;
@class ADXReachability;
@class ADXTimer;
@class ADXGeolocationProvider;
@class CLLocationManager;
@class ADXLogEventRecorder;
@class ADXNetworkManager;

typedef id(^ADXSingletonProviderBlock)();

@interface ADXCoreInstanceProvider : NSObject

+ (instancetype)sharedProvider;
- (id)singletonForClass:(Class)klass provider:(ADXSingletonProviderBlock)provider;

- (void)keepObjectAliveForCurrentRunLoopIteration:(id)anObject;

#pragma mark - Fetching Ads
- (NSMutableURLRequest *)buildConfiguredURLRequestWithURL:(NSURL *)URL;
- (ADXAdServerCommunicator *)buildADXAdServerCommunicatorWithDelegate:(id<ADXAdServerCommunicatorDelegate>)delegate;

#pragma mark - URL Handling
- (ADXURLResolver *)buildADXURLResolver;
- (ADXAdDestinationDisplayAgent *)buildADXAdDestinationDisplayAgentWithDelegate:(id<ADXAdDestinationDisplayAgentDelegate>)delegate;

#pragma mark - Utilities
- (UIDevice *)sharedCurrentDevice;
- (ADXGeolocationProvider *)sharedADXGeolocationProvider;
- (CLLocationManager *)buildCLLocationManager;
- (id<ADXAdAlertManagerProtocol>)buildADXAdAlertManagerWithDelegate:(id)delegate;
- (ADXAdAlertGestureRecognizer *)buildADXAdAlertGestureRecognizerWithTarget:(id)target action:(SEL)action;
- (NSOperationQueue *)sharedOperationQueue;
- (ADXAnalyticsTracker *)sharedADXAnalyticsTracker;
- (ADXReachability *)sharedADXReachability;
- (ADXLogEventRecorder *)sharedLogEventRecorder;
- (ADXNetworkManager *)sharedNetworkManager;

// This call may return nil and may not update if the user hot-swaps the device's sim card.
- (NSDictionary *)sharedCarrierInfo;

- (ADXTimer *)buildADXTimerWithTimeInterval:(NSTimeInterval)seconds target:(id)target selector:(SEL)selector repeats:(BOOL)repeats;

@end
