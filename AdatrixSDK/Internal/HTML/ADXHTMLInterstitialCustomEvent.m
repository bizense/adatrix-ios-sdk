//
//  ADXHTMLInterstitialCustomEvent.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXHTMLInterstitialCustomEvent.h"
#import "ADXLogging.h"
#import "ADXAdConfiguration.h"
#import "ADXInstanceProvider.h"

@interface ADXHTMLInterstitialCustomEvent ()

@property (nonatomic, strong) ADXHTMLInterstitialViewController *interstitial;
@property (nonatomic, assign) BOOL trackedImpression;

@end

@implementation ADXHTMLInterstitialCustomEvent

@synthesize interstitial = _interstitial;
@synthesize delegate;

- (BOOL)enableAutomaticImpressionAndClickTracking
{
    // An HTML interstitial tracks its own clicks. Turn off automatic tracking to prevent the tap event callback
    // from generating an additional click.
    // However, an HTML interstitial does not track its own impressions so we must manually do it in this class.
    // See interstitialDidAppear:
    return NO;
}

- (void)requestInterstitialWithCustomEventInfo:(NSDictionary *)info
{
    ADXLogInfo(@"Loading Adatrix HTML interstitial");
    ADXAdConfiguration *configuration = [self.delegate configuration];
    ADXLogTrace(@"Loading HTML interstitial with source: %@", [configuration adResponseHTMLString]);

    self.interstitial = [[ADXInstanceProvider sharedProvider] buildADXHTMLInterstitialViewControllerWithDelegate:self
                                                                                               orientationType:configuration.orientationType];
    [self.interstitial loadConfiguration:configuration];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController
{
    [self.interstitial presentInterstitialFromViewController:rootViewController];
}

#pragma mark - ADXInterstitialViewControllerDelegate

- (CLLocation *)location
{
    return [self.delegate location];
}

- (NSString *)adUnitId
{
    return [self.delegate adUnitId];
}

- (NSString *)viewId
{
    return [self.delegate viewId];
}

- (void)interstitialDidLoadAd:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial did load");
    [self.delegate interstitialCustomEvent:self didLoadAd:self.interstitial];
}

- (void)interstitialDidFailToLoadAd:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial did fail");
    [self.delegate interstitialCustomEvent:self didFailToLoadAdWithError:nil];
}

- (void)interstitialWillAppear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial will appear");
    [self.delegate interstitialCustomEventWillAppear:self];
}

- (void)interstitialDidAppear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial did appear");
    [self.delegate interstitialCustomEventDidAppear:self];

    if (!self.trackedImpression) {
        self.trackedImpression = YES;
        [self.delegate trackImpression];
    }
}

- (void)interstitialWillDisappear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial will disappear");
    [self.delegate interstitialCustomEventWillDisappear:self];
}

- (void)interstitialDidDisappear:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial did disappear");
    [self.delegate interstitialCustomEventDidDisappear:self];

    // Deallocate the interstitial as we don't need it anymore. If we don't deallocate the interstitial after dismissal,
    // then the html in the webview will continue to run which could lead to bugs such as continuing to play the sound of an inline
    // video since the app may hold onto the interstitial ad controller. Moreover, we keep an array of controllers around as well.
    self.interstitial = nil;
}

- (void)interstitialDidReceiveTapEvent:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial did receive tap event");
    [self.delegate interstitialCustomEventDidReceiveTapEvent:self];
}

- (void)interstitialWillLeaveApplication:(ADXInterstitialViewController *)interstitial
{
    ADXLogInfo(@"Adatrix HTML interstitial will leave application");
    [self.delegate interstitialCustomEventWillLeaveApplication:self];
}

@end
