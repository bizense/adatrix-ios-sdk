//
//  ADXAdWebView.m
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXAdWebView.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation ADXAdWebView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= ADX_IOS_4_0
        if ([self respondsToSelector:@selector(allowsInlineMediaPlayback)]) {
            [self setAllowsInlineMediaPlayback:YES];
            [self setMediaPlaybackRequiresUserAction:NO];
        }
#endif
    }
    return self;
}

@end
