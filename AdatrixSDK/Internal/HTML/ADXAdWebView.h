//
//  ADXAdWebView.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXAdWebView : UIWebView

@end
