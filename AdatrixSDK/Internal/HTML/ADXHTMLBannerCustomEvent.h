//
//  ADXHTMLBannerCustomEvent.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXBannerCustomEvent.h"
#import "ADXAdWebViewAgent.h"
#import "ADXPrivateBannerCustomEventDelegate.h"

@interface ADXHTMLBannerCustomEvent : ADXBannerCustomEvent <ADXAdWebViewAgentDelegate>

@property (nonatomic, weak) id<ADXPrivateBannerCustomEventDelegate> delegate;

@end
