//
//  ADXHTMLInterstitialCustomEvent.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXInterstitialCustomEvent.h"
#import "ADXHTMLInterstitialViewController.h"
#import "ADXPrivateInterstitialCustomEventDelegate.h"

@interface ADXHTMLInterstitialCustomEvent : ADXInterstitialCustomEvent <ADXInterstitialViewControllerDelegate>

@property (nonatomic, weak) id<ADXPrivateInterstitialCustomEventDelegate> delegate;

@end
