//
//  ADXAdWebViewAgent.h
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXAdDestinationDisplayAgent.h"

enum {
    ADXAdWebViewEventAdDidAppear     = 0,
    ADXAdWebViewEventAdDidDisappear  = 1
};
typedef NSUInteger ADXAdWebViewEvent;

@protocol ADXAdWebViewAgentDelegate;

@class ADXAdConfiguration;
@class ADXAdWebView;
@class CLLocation;

@interface ADXAdWebViewAgent : NSObject <UIWebViewDelegate, ADXAdDestinationDisplayAgentDelegate>

@property (nonatomic, strong) ADXAdWebView *view;
@property (nonatomic, weak) id<ADXAdWebViewAgentDelegate> delegate;

- (id)initWithAdWebViewFrame:(CGRect)frame delegate:(id<ADXAdWebViewAgentDelegate>)delegate;
- (void)loadConfiguration:(ADXAdConfiguration *)configuration;
- (void)rotateToOrientation:(UIInterfaceOrientation)orientation;
- (void)invokeJavaScriptForEvent:(ADXAdWebViewEvent)event;
- (void)forceRedraw;

- (void)enableRequestHandling;
- (void)disableRequestHandling;

@end

@class ADXAdWebView;

@protocol ADXAdWebViewAgentDelegate <NSObject>

- (NSString *)adUnitId;
- (NSString *)viewId;
- (CLLocation *)location;
- (UIViewController *)viewControllerForPresentingModalView;
- (void)adDidClose:(ADXAdWebView *)ad;
- (void)adDidFinishLoadingAd:(ADXAdWebView *)ad;
- (void)adDidFailToLoadAd:(ADXAdWebView *)ad;
- (void)adActionWillBegin:(ADXAdWebView *)ad;
- (void)adActionWillLeaveApplication:(ADXAdWebView *)ad;
- (void)adActionDidFinish:(ADXAdWebView *)ad;

@end
