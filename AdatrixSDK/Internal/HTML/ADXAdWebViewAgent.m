//
//  ADXAdWebViewAgent.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXAdWebViewAgent.h"
#import "ADXAdConfiguration.h"
#import "ADXGlobal.h"
#import "ADXLogging.h"
#import "ADXAdDestinationDisplayAgent.h"
#import "NSURL+ADXAdditions.h"
#import "UIWebView+ADXAdditions.h"
#import "ADXAdWebView.h"
#import "ADXInstanceProvider.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXUserInteractionGestureRecognizer.h"
#import "NSJSONSerialization+ADXAdditions.h"
#import "NSURL+ADXAdditions.h"
#import "ADXInternalUtils.h"

#ifndef NSFoundationVersionNumber_iOS_6_1
#define NSFoundationVersionNumber_iOS_6_1 993.00
#endif

#define ADXOffscreenWebViewNeedsRenderingWorkaround() (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)

@interface ADXAdWebViewAgent () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) ADXAdConfiguration *configuration;
@property (nonatomic, strong) ADXAdDestinationDisplayAgent *destinationDisplayAgent;
@property (nonatomic, assign) BOOL shouldHandleRequests;
@property (nonatomic, strong) id<ADXAdAlertManagerProtocol> adAlertManager;
@property (nonatomic, assign) BOOL userInteractedWithWebView;
@property (nonatomic, strong) ADXUserInteractionGestureRecognizer *userInteractionRecognizer;

- (void)performActionForAdatrixSpecificURL:(NSURL *)URL;
- (BOOL)shouldIntercept:(NSURL *)URL navigationType:(UIWebViewNavigationType)navigationType;
- (void)interceptURL:(NSURL *)URL;

@end

@implementation ADXAdWebViewAgent

@synthesize configuration = _configuration;
@synthesize delegate = _delegate;
@synthesize destinationDisplayAgent = _destinationDisplayAgent;
@synthesize shouldHandleRequests = _shouldHandleRequests;
@synthesize view = _view;
@synthesize adAlertManager = _adAlertManager;
@synthesize userInteractedWithWebView = _userInteractedWithWebView;
@synthesize userInteractionRecognizer = _userInteractionRecognizer;

- (id)initWithAdWebViewFrame:(CGRect)frame delegate:(id<ADXAdWebViewAgentDelegate>)delegate;
{
    self = [super init];
    if (self) {
        self.view = [[ADXInstanceProvider sharedProvider] buildADXAdWebViewWithFrame:frame delegate:self];
        self.destinationDisplayAgent = [[ADXCoreInstanceProvider sharedProvider] buildADXAdDestinationDisplayAgentWithDelegate:self];
        self.delegate = delegate;
        self.shouldHandleRequests = YES;
        self.adAlertManager = [[ADXCoreInstanceProvider sharedProvider] buildADXAdAlertManagerWithDelegate:self];

        self.userInteractionRecognizer = [[ADXUserInteractionGestureRecognizer alloc] initWithTarget:self action:@selector(handleInteraction:)];
        self.userInteractionRecognizer.cancelsTouchesInView = NO;
        [self.view addGestureRecognizer:self.userInteractionRecognizer];
        self.userInteractionRecognizer.delegate = self;
    }
    return self;
}

- (void)dealloc
{
    self.userInteractionRecognizer.delegate = nil;
    [self.userInteractionRecognizer removeTarget:self action:nil];
    [self.destinationDisplayAgent cancel];
    [self.destinationDisplayAgent setDelegate:nil];
    self.view.delegate = nil;
}

- (void)handleInteraction:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
        self.userInteractedWithWebView = YES;
    }
}

#pragma mark - <UIGestureRecognizerDelegate>

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer;
{
    return YES;
}

#pragma mark - <ADXAdAlertManagerDelegate>

- (UIViewController *)viewControllerForPresentingMailVC
{
    return [self.delegate viewControllerForPresentingModalView];
}

- (void)adAlertManagerDidTriggerAlert:(ADXAdAlertManager *)manager
{
    [self.adAlertManager processAdAlertOnce];
}

#pragma mark - Public

- (void)loadConfiguration:(ADXAdConfiguration *)configuration
{
    self.configuration = configuration;

    // Ignore server configuration size for interstitials. At this point our web view
    // is sized correctly for the device's screen. Currently the server sends down values for a 3.5in
    // screen, and they do not size correctly on a 4in screen.
    if (configuration.adType != ADXAdTypeInterstitial) {
        if ([configuration hasPreferredSize]) {
            CGRect frame = self.view.frame;
            frame.size.width = configuration.preferredSize.width;
            frame.size.height = configuration.preferredSize.height;
            self.view.frame = frame;
        }
    }

    // excuse interstitials from user tapped check since it's already a takeover experience
    // and certain videos may delay tap gesture recognition
    if (configuration.adType == ADXAdTypeInterstitial) {
        self.userInteractedWithWebView = YES;
    }

    [self.view adx_setScrollable:configuration.scrollable];
    [self.view disableJavaScriptDialogs];
    [self.view loadHTMLString:[configuration adResponseHTMLString] baseURL:nil];

    [self initAdAlertManager];
}

- (void)invokeJavaScriptForEvent:(ADXAdWebViewEvent)event
{
    switch (event) {
        case ADXAdWebViewEventAdDidAppear:
            [self.view stringByEvaluatingJavaScriptFromString:@"webviewDidAppear();"];
            break;
        case ADXAdWebViewEventAdDidDisappear:
            [self.view stringByEvaluatingJavaScriptFromString:@"webviewDidClose();"];
            break;
        default:
            break;
    }
}

- (void)disableRequestHandling
{
    self.shouldHandleRequests = NO;
    [self.destinationDisplayAgent cancel];
}

- (void)enableRequestHandling
{
    self.shouldHandleRequests = YES;
}

#pragma mark - <ADXAdDestinationDisplayAgentDelegate>

- (UIViewController *)viewControllerForPresentingModalView
{
    return [self.delegate viewControllerForPresentingModalView];
}

- (void)displayAgentWillPresentModal
{
    [self.delegate adActionWillBegin:self.view];
}

- (void)displayAgentWillLeaveApplication
{
    [self.delegate adActionWillLeaveApplication:self.view];
}

- (void)displayAgentDidDismissModal
{
    [self.delegate adActionDidFinish:self.view];
}

#pragma mark - <UIWebViewDelegate>

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    if (!self.shouldHandleRequests) {
        return NO;
    }

    NSURL *URL = [request URL];
    if ([URL adx_isAdatrixScheme]) {
        [self performActionForAdatrixSpecificURL:URL];
        return NO;
    } else if ([self shouldIntercept:URL navigationType:navigationType]) {
        [self interceptURL:URL];
        return NO;
    } else {
        // don't handle any deep links without user interaction
        return self.userInteractedWithWebView || [URL adx_isSafeForLoadingWithoutUserAction];
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.view disableJavaScriptDialogs];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.delegate adDidFinishLoadingAd:self.view];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.delegate adDidFailToLoadAd:self.view];
}

#pragma mark - Adatrix-specific URL handlers
- (void)performActionForAdatrixSpecificURL:(NSURL *)URL
{
    ADXLogDebug(@"ADXAdWebView - loading Adatrix URL: %@", URL);
    ADXAdatrixHostCommand command = [URL adx_adatrixHostCommand];
    switch (command) {
        case ADXAdatrixHostCommandClose:
            [self.delegate adDidClose:self.view];
            break;
        case ADXAdatrixHostCommandFinishLoad:
            [self.delegate adDidFinishLoadingAd:self.view];
            break;
        case ADXAdatrixHostCommandFailLoad:
            [self.delegate adDidFailToLoadAd:self.view];
            break;
        default:
            ADXLogWarn(@"ADXAdWebView - unsupported Adatrix URL: %@", [URL absoluteString]);
            break;
    }
}

#pragma mark - URL Interception
- (BOOL)shouldIntercept:(NSURL *)URL navigationType:(UIWebViewNavigationType)navigationType
{
    if ([URL adx_hasTelephoneScheme] || [URL adx_hasTelephonePromptScheme]) {
        return YES;
    } else if (!(self.configuration.shouldInterceptLinks)) {
        return NO;
    } else if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        return YES;
    } else if (navigationType == UIWebViewNavigationTypeOther) {
        return [[URL absoluteString] hasPrefix:[self.configuration clickDetectionURLPrefix]];
    } else {
        return NO;
    }
}

- (void)interceptURL:(NSURL *)URL
{
    NSURL *redirectedURL = URL;
    if (self.configuration.clickTrackingURL) {
        NSString *path = [NSString stringWithFormat:@"%@&r=%@",
                          self.configuration.clickTrackingURL.absoluteString,
                          [[URL absoluteString] URLEncodedString]];
        redirectedURL = [NSURL URLWithString:path];
    }

    [self.destinationDisplayAgent displayDestinationForURL:redirectedURL];
}

#pragma mark - Utility

- (void)initAdAlertManager
{
    self.adAlertManager.adConfiguration = self.configuration;
    self.adAlertManager.adUnitId = [self.delegate adUnitId];
    self.adAlertManager.viewId = [self.delegate viewId];
    self.adAlertManager.targetAdView = self.view;
    self.adAlertManager.location = [self.delegate location];
    [self.adAlertManager beginMonitoringAlerts];
}

- (void)rotateToOrientation:(UIInterfaceOrientation)orientation
{
    [self forceRedraw];
}

- (void)forceRedraw
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    int angle = -1;
    switch (orientation) {
        case UIInterfaceOrientationPortrait: angle = 0; break;
        case UIInterfaceOrientationLandscapeLeft: angle = 90; break;
        case UIInterfaceOrientationLandscapeRight: angle = -90; break;
        case UIInterfaceOrientationPortraitUpsideDown: angle = 180; break;
        default: break;
    }

    if (angle == -1) return;

    // UIWebView doesn't seem to fire the 'orientationchange' event upon rotation, so we do it here.
    NSString *orientationEventScript = [NSString stringWithFormat:
                                        @"window.__defineGetter__('orientation',function(){return %d;});"
                                        @"(function(){ var evt = document.createEvent('Events');"
                                        @"evt.initEvent('orientationchange',true,true);window.dispatchEvent(evt);})();",
                                        angle];
    [self.view stringByEvaluatingJavaScriptFromString:orientationEventScript];

    // XXX: If the UIWebView is rotated off-screen (which may happen with interstitials), its
    // content may render off-center upon display. We compensate by setting the viewport meta tag's
    // 'width' attribute to be the size of the webview.
    NSString *viewportUpdateScript = [NSString stringWithFormat:
                                      @"document.querySelector('meta[name=viewport]')"
                                      @".setAttribute('content', 'width=%f;', false);",
                                      self.view.frame.size.width];
    [self.view stringByEvaluatingJavaScriptFromString:viewportUpdateScript];

    // XXX: In iOS 7, off-screen UIWebViews will fail to render certain image creatives.
    // Specifically, creatives that only contain an <img> tag whose src attribute uses a 302
    // redirect will not be rendered at all. One workaround is to temporarily change the web view's
    // internal contentInset property; this seems to force the web view to re-draw.
    if (ADXOffscreenWebViewNeedsRenderingWorkaround()) {
        if ([self.view respondsToSelector:@selector(scrollView)]) {
            UIScrollView *scrollView = self.view.scrollView;
            UIEdgeInsets originalInsets = scrollView.contentInset;
            UIEdgeInsets newInsets = UIEdgeInsetsMake(originalInsets.top + 1,
                                                      originalInsets.left,
                                                      originalInsets.bottom,
                                                      originalInsets.right);
            scrollView.contentInset = newInsets;
            scrollView.contentInset = originalInsets;
        }
    }
}

@end
