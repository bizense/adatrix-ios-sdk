//
//  ADXHTMLInterstitialViewController.h
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ADXAdWebViewAgent.h"
#import "ADXInterstitialViewController.h"

@class ADXAdConfiguration;

@interface ADXHTMLInterstitialViewController : ADXInterstitialViewController <ADXAdWebViewAgentDelegate>

@property (nonatomic, strong) ADXAdWebViewAgent *backingViewAgent;

- (void)loadConfiguration:(ADXAdConfiguration *)configuration;

@end
