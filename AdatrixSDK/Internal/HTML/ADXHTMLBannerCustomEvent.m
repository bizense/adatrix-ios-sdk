//
//  ADXHTMLBannerCustomEvent.m
//  Adatrix
//
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXHTMLBannerCustomEvent.h"
#import "ADXAdWebView.h"
#import "ADXLogging.h"
#import "ADXAdConfiguration.h"
#import "ADXInstanceProvider.h"

@interface ADXHTMLBannerCustomEvent ()

@property (nonatomic, strong) ADXAdWebViewAgent *bannerAgent;

@end

@implementation ADXHTMLBannerCustomEvent

@synthesize bannerAgent = _bannerAgent;
@synthesize delegate;

- (BOOL)enableAutomaticImpressionAndClickTracking
{
    return NO;
}

- (void)requestAdWithSize:(CGSize)size customEventInfo:(NSDictionary *)info
{
    ADXLogInfo(@"Loading Adatrix HTML banner");
//    ADXLogInfo(@"Loading banner with HTML source: %@", [[self.delegate configuration] adResponseHTMLString]);

    CGRect adWebViewFrame = CGRectMake(0, 0, size.width, size.height);
    self.bannerAgent = [[ADXInstanceProvider sharedProvider] buildADXAdWebViewAgentWithAdWebViewFrame:adWebViewFrame
                                                                                           delegate:self];
    [self.bannerAgent loadConfiguration:[self.delegate configuration]];
}

- (void)dealloc
{
    self.bannerAgent.delegate = nil;
}

- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation
{
    [self.bannerAgent rotateToOrientation:newOrientation];
}

#pragma mark - ADXAdWebViewAgentDelegate

- (CLLocation *)location
{
    return [self.delegate location];
}

- (NSString *)adUnitId
{
    return [self.delegate adUnitId];
}

- (NSString *)viewId
{
    return [self.delegate viewId];
}
- (UIViewController *)viewControllerForPresentingModalView
{
    return [self.delegate viewControllerForPresentingModalView];
}

- (void)adDidFinishLoadingAd:(ADXAdWebView *)ad
{
    ADXLogInfo(@"Adatrix HTML banner did load");
    [self.delegate bannerCustomEvent:self didLoadAd:ad];
}

- (void)adDidFailToLoadAd:(ADXAdWebView *)ad
{
    ADXLogInfo(@"Adatrix HTML banner did fail");
    [self.delegate bannerCustomEvent:self didFailToLoadAdWithError:nil];
}

- (void)adDidClose:(ADXAdWebView *)ad
{
    //don't care
}

- (void)adActionWillBegin:(ADXAdWebView *)ad
{
    ADXLogInfo(@"Adatrix HTML banner will begin action");
    [self.delegate bannerCustomEventWillBeginAction:self];
}

- (void)adActionDidFinish:(ADXAdWebView *)ad
{
    ADXLogInfo(@"Adatrix HTML banner did finish action");
    [self.delegate bannerCustomEventDidFinishAction:self];
}

- (void)adActionWillLeaveApplication:(ADXAdWebView *)ad
{
    ADXLogInfo(@"Adatrix HTML banner will leave application");
    [self.delegate bannerCustomEventWillLeaveApplication:self];
}


@end
