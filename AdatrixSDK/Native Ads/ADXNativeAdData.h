//
//  ADXNativeAdData.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXNativeAd;

@interface ADXNativeAdData : NSObject

@property (nonatomic, copy) NSString *adUnitID;
@property (nonatomic, copy) NSString *viewID;
@property (nonatomic, strong) ADXNativeAd *ad;
@property (nonatomic, assign) Class renderingClass;

@end
