//
//  ADXNativeAdError.h
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const AdatrixNativeAdsSDKDomain;

typedef enum ADXNativeAdErrorCode {
    ADXNativeAdErrorUnknown = -1,

    ADXNativeAdErrorHTTPError = -1000,
    ADXNativeAdErrorInvalidServerResponse = -1001,
    ADXNativeAdErrorNoInventory = -1002,
    ADXNativeAdErrorImageDownloadFailed = -1003,
    ADXNativeAdErrorAdUnitWarmingUp = -1004,

    ADXNativeAdErrorContentDisplayError = -1100,
} ADXNativeAdErrorCode;

extern NSString *const ADXNativeAdErrorContentDisplayErrorReasonKey;
