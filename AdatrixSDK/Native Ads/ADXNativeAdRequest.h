//
//  ADXNativeAdRequest.h
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXNativeAd;
@class ADXNativeAdRequest;
@class ADXNativeAdRequestTargeting;

typedef void(^ADXNativeAdRequestHandler)(ADXNativeAdRequest *request,
                                      ADXNativeAd *response,
                                      NSError *error);

////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * The `ADXNativeAdRequest` class is used to manage individual requests to the Adatrix ad server for
 * native ads.
 *
 * @warning **Note:** This class is meant for one-off requests for which you intend to manually
 * process the native ad response. If you are using `ADXTableViewAdPlacer` or
 * `ADXCollectionViewAdPlacer` to display ads, there should be no need for you to use this class.
 */

@interface ADXNativeAdRequest : NSObject

/** @name Targeting Information */

/**
 * An object representing targeting parameters that can be passed to the Adatrix ad server to
 * serve more relevant advertising.
 */
@property (nonatomic, strong) ADXNativeAdRequestTargeting *targeting;

/** @name Initializing and Starting an Ad Request */

/**
 * Initializes a request object.
 *
 * @param identifier The ad unit identifier for this request. An ad unit is a defined placement in
 * your application set aside for advertising. Ad unit IDs are created on the Adatrix website.
 *
 * @return An `ADXNativeAdRequest` object.
 */
+ (ADXNativeAdRequest *)requestWithAdUnitIdentifier:(NSString *)identifier;

/**
 * Executes a request to the Adatrix ad server.
 *
 * @param handler A block to execute when the request finishes. The block includes as parameters the
 * request itself and either a valid ADXNativeAd or an NSError object indicating failure.
 */
- (void)startWithCompletionHandler:(ADXNativeAdRequestHandler)handler;

@end
