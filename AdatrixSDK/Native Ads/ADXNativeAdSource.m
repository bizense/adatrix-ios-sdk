//
//  ADXNativeAdSource.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeAdSource.h"
#import "ADXNativeAd.h"
#import "ADXNativeAdRequestTargeting.h"
#import "ADXNativeAdSourceQueue.h"

static NSTimeInterval const kCacheTimeoutInterval = 900; //15 minutes

@interface ADXNativeAdSource () <ADXNativeAdSourceQueueDelegate>

@property (nonatomic, strong) NSMutableDictionary *adQueueDictionary;

@end

@implementation ADXNativeAdSource

#pragma mark - Object Lifecycle

+ (instancetype)source
{
    return [[ADXNativeAdSource alloc] init];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _adQueueDictionary = [[NSMutableDictionary alloc] init];
    }

    return self;
}

- (void)dealloc
{
    for (NSString *queueKey in [_adQueueDictionary allKeys]) {
        [self deleteCacheForAdUnitIdentifier:queueKey];
    }
}

#pragma mark - Ad Source Interface

- (void)loadAdsWithAdUnitIdentifier:(NSString *)identifier viewID:(NSString *)viewID andTargeting:(ADXNativeAdRequestTargeting *)targeting
{
    [self deleteCacheForAdUnitIdentifier:identifier];

    ADXNativeAdSourceQueue *adQueue = [[ADXNativeAdSourceQueue alloc] initWithAdUnitIdentifier:identifier andTargeting:targeting];
    adQueue.delegate = self;
    [self.adQueueDictionary setObject:adQueue forKey:identifier];

    [adQueue loadAds];
}

- (id)dequeueAdForAdUnitIdentifier:(NSString *)identifier
{
    ADXNativeAdSourceQueue *adQueue = [self.adQueueDictionary objectForKey:identifier];
    ADXNativeAd *nextAd = [adQueue dequeueAdWithMaxAge:kCacheTimeoutInterval];
    return nextAd;
}

- (void)deleteCacheForAdUnitIdentifier:(NSString *)identifier
{
    ADXNativeAdSourceQueue *sourceQueue = [self.adQueueDictionary objectForKey:identifier];
    sourceQueue.delegate = nil;
    [sourceQueue cancelRequests];

    [self.adQueueDictionary removeObjectForKey:identifier];
}

#pragma mark - ADXNativeAdSourceQueueDelegate

- (void)adSourceQueueAdIsAvailable:(ADXNativeAdSourceQueue *)source
{
    [self.delegate adSourceDidFinishRequest:self];
}

@end
