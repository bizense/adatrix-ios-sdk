//
//  ADXClientAdPositioning.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXClientAdPositioning.h"
#import "ADXLogging.h"

@implementation ADXClientAdPositioning

+ (instancetype)positioning
{
    return [[self alloc] init];
}

- (void)addFixedIndexPath:(NSIndexPath *)indexPath
{
    [self.fixedPositions addObject:indexPath];
}

- (void)enableRepeatingPositionsWithInterval:(NSUInteger)interval
{
    if (interval > 1) {
        self.repeatingInterval = interval;
    } else {
        ADXLogWarn(@"Repeating positions will not be enabled: the provided interval must be greater "
                  @"than 1.");
    }
}

@end
