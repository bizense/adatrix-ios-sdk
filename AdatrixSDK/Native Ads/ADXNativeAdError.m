//
//  ADXNativeAdError.m
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXNativeAdError.h"

NSString * const AdatrixNativeAdsSDKDomain = @"com.adatrix.nativeads";

NSString * const ADXNativeAdErrorContentDisplayErrorReasonKey = @"contentDisplayErrorReason";
