//
//  ADXAdPositioning.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADXAdPositioning : NSObject <NSCopying>

@property (nonatomic, assign) NSUInteger repeatingInterval;
@property (nonatomic, strong, readonly) NSMutableOrderedSet *fixedPositions;

@end
