//
//  ADXNativeAdSourceDelegate.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXNativeAdSource;

@protocol ADXNativeAdSourceDelegate <NSObject>

- (void)adSourceDidFinishRequest:(ADXNativeAdSource *)source;

@end
