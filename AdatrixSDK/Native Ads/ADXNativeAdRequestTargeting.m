//
//  ADXNativeAdRequestTargeting.m
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeAdRequestTargeting.h"
#import "ADXNativeAdConstants.h"

#import <CoreLocation/CoreLocation.h>

@implementation ADXNativeAdRequestTargeting

+ (ADXNativeAdRequestTargeting *)targeting
{
    return [[ADXNativeAdRequestTargeting alloc] init];
}

- (void)setDesiredAssets:(NSSet *)desiredAssets
{
    if (_desiredAssets != desiredAssets) {

        NSMutableSet *allowedAdAssets = [NSMutableSet setWithObjects:kAdTitleKey,
                                         kAdTextKey,
                                         kAdIconImageKey,
                                         kAdMainImageKey,
                                         kAdCTATextKey,
                                         kAdStarRatingKey,
                                         nil];
        [allowedAdAssets intersectSet:desiredAssets];
        _desiredAssets = allowedAdAssets;
    }
}


@end
