//
//  ADXNativeCustomEvent.m
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeCustomEvent.h"
#import "ADXNativeAdError.h"
#import "ADXImageDownloadQueue.h"
#import "ADXLogging.h"

@interface ADXNativeCustomEvent ()

@property (nonatomic, strong) ADXImageDownloadQueue *imageDownloadQueue;

@end

@implementation ADXNativeCustomEvent

- (id)init
{
    self = [super init];
    if (self) {
        _imageDownloadQueue = [[ADXImageDownloadQueue alloc] init];
    }

    return self;
}

- (void)precacheImagesWithURLs:(NSArray *)imageURLs completionBlock:(void (^)(NSArray *errors))completionBlock
{
    if (imageURLs.count > 0) {
        [_imageDownloadQueue addDownloadImageURLs:imageURLs completionBlock:^(NSArray *errors) {
            if (completionBlock) {
                completionBlock(errors);
            }
        }];
    } else {
        if (completionBlock) {
            completionBlock(nil);
        }
    }
}

- (void)requestAdWithCustomEventInfo:(NSDictionary *)info
{
    /*override with custom network behavior*/
}

@end
