//
//  ADXAdPositioning.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXAdPositioning.h"

@interface ADXAdPositioning ()

@property (nonatomic, strong) NSMutableOrderedSet *fixedPositions;

@end

@implementation ADXAdPositioning

- (id)init
{
    self = [super init];
    if (self) {
        _fixedPositions = [[NSMutableOrderedSet alloc] init];
    }

    return self;
}


#pragma mark - <NSCopying>

- (id)copyWithZone:(NSZone *)zone
{
    ADXAdPositioning *newPositioning = [[[self class] allocWithZone:zone] init];
    newPositioning.fixedPositions = [self.fixedPositions copyWithZone:zone];
    newPositioning.repeatingInterval = self.repeatingInterval;
    return newPositioning;
}

@end
