//
//  ADXNativeCache.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeCache.h"
#import "ADXDiskLRUCache.h"
#import "ADXLogging.h"

typedef enum {
    ADXNativeCacheMethodDisk = 0,
    ADXNativeCacheMethodDiskAndMemory = 1 << 0
} ADXNativeCacheMethod;

@interface ADXNativeCache () <NSCacheDelegate>

@property (nonatomic, strong) NSCache *memoryCache;
@property (nonatomic, strong) ADXDiskLRUCache *diskCache;
@property (nonatomic, assign) ADXNativeCacheMethod cacheMethod;

- (BOOL)cachedDataExistsForKey:(NSString *)key withCacheMethod:(ADXNativeCacheMethod)cacheMethod;
- (NSData *)retrieveDataForKey:(NSString *)key withCacheMethod:(ADXNativeCacheMethod)cacheMethod;
- (void)storeData:(id)data forKey:(NSString *)key withCacheMethod:(ADXNativeCacheMethod)cacheMethod;
- (void)removeAllDataFromMemory;
- (void)removeAllDataFromDisk;

@end

@implementation ADXNativeCache

+ (instancetype)sharedCache;
{
    static dispatch_once_t once;
    static ADXNativeCache *sharedCache;
    dispatch_once(&once, ^{
        sharedCache = [[self alloc] init];
    });
    return sharedCache;
}

- (id)init
{
    self = [super init];
    if (self != nil) {
        _memoryCache = [[NSCache alloc] init];
        _memoryCache.delegate = self;

        _diskCache = [[ADXDiskLRUCache alloc] init];

        _cacheMethod = ADXNativeCacheMethodDiskAndMemory;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning:) name:UIApplicationDidReceiveMemoryWarningNotification object:[UIApplication sharedApplication]];
    }

    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
}

#pragma mark - Public Cache Interactions

- (void)setInMemoryCacheEnabled:(BOOL)enabled
{
    if (enabled) {
        self.cacheMethod = ADXNativeCacheMethodDiskAndMemory;
    } else {
        self.cacheMethod = ADXNativeCacheMethodDisk;
        [self.memoryCache removeAllObjects];
    }
}

- (BOOL)cachedDataExistsForKey:(NSString *)key
{
    return [self cachedDataExistsForKey:key withCacheMethod:self.cacheMethod];
}

- (NSData *)retrieveDataForKey:(NSString *)key
{
    return [self retrieveDataForKey:key withCacheMethod:self.cacheMethod];
}

- (void)storeData:(NSData *)data forKey:(NSString *)key
{
    [self storeData:data forKey:key withCacheMethod:self.cacheMethod];
}

- (void)removeAllDataFromCache
{
    [self removeAllDataFromMemory];
    [self removeAllDataFromDisk];
}

#pragma mark - Private Cache Implementation

- (BOOL)cachedDataExistsForKey:(NSString *)key withCacheMethod:(ADXNativeCacheMethod)cacheMethod
{
    BOOL dataExists = NO;
    if (cacheMethod & ADXNativeCacheMethodDiskAndMemory) {
        dataExists = [self.memoryCache objectForKey:key] != nil;
    }

    if (!dataExists) {
        dataExists = [self.diskCache cachedDataExistsForKey:key];
    }

    return dataExists;
}

- (id)retrieveDataForKey:(NSString *)key withCacheMethod:(ADXNativeCacheMethod)cacheMethod
{
    id data = nil;

    if (cacheMethod & ADXNativeCacheMethodDiskAndMemory) {
        data = [self.memoryCache objectForKey:key];
    }

    if (data) {
        ADXLogDebug(@"RETRIEVE FROM MEMORY: %@", key);
    }


    if (data == nil) {
        data = [self.diskCache retrieveDataForKey:key];

        if (data && cacheMethod & ADXNativeCacheMethodDiskAndMemory) {
            ADXLogDebug(@"RETRIEVE FROM DISK: %@", key);

            [self.memoryCache setObject:data forKey:key];
            ADXLogDebug(@"STORED IN MEMORY: %@", key);
        }
    }

    if (data == nil) {
        ADXLogDebug(@"RETRIEVE FAILED: %@", key);
    }

    return data;
}

- (void)storeData:(id)data forKey:(NSString *)key withCacheMethod:(ADXNativeCacheMethod)cacheMethod
{
    if (data == nil) {
        return;
    }

    if (cacheMethod & ADXNativeCacheMethodDiskAndMemory) {
        [self.memoryCache setObject:data forKey:key];
        ADXLogDebug(@"STORED IN MEMORY: %@", key);
    }

    [self.diskCache storeData:data forKey:key];
    ADXLogDebug(@"STORED ON DISK: %@", key);
}

- (void)removeAllDataFromMemory
{
    [self.memoryCache removeAllObjects];
}

- (void)removeAllDataFromDisk
{
    [self.diskCache removeAllCachedFiles];
}

#pragma mark - Notifications

- (void)didReceiveMemoryWarning:(NSNotification *)notification
{
    [self.memoryCache removeAllObjects];
}

#pragma mark - NSCacheDelegate

- (void)cache:(NSCache *)cache willEvictObject:(id)obj
{
    ADXLogDebug(@"Evicting Object");
}


@end
