//
//  ADXNativePositionSource.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXAdPositioning;

typedef enum : NSUInteger {
    ADXNativePositionSourceInvalidAdUnitIdentifier,
    ADXNativePositionSourceEmptyResponse,
    ADXNativePositionSourceDeserializationFailed,
    ADXNativePositionSourceConnectionFailed,
} ADXNativePositionSourceErrorCode;

////////////////////////////////////////////////////////////////////////////////////////////////////

@interface ADXNativePositionSource : NSObject

- (void)loadPositionsWithAdUnitIdentifier:(NSString *)identifier viewID:(NSString *)viewID completionHandler:(void (^)(ADXAdPositioning *positioning, NSError *error))completionHandler;
- (void)cancel;

@end
