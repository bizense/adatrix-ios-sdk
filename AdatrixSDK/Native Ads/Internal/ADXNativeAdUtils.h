//
//  ADXNativeAdUtils.h
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSTimeInterval const kUpdateVisibleCellsInterval;

@interface ADXNativeAdUtils : NSObject

+ (BOOL)addURLString:(NSString *)urlString toURLArray:(NSMutableArray *)urlArray;

@end
