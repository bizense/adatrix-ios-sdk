//
//  ADXAdatrixNativeAdAdapter.h
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeAdAdapter.h"

@interface ADXAdatrixNativeAdAdapter : NSObject <ADXNativeAdAdapter>

@property (nonatomic, strong) NSArray *impressionTrackers;
@property (nonatomic, strong) NSURL *engagementTrackingURL;

- (instancetype)initWithAdProperties:(NSMutableDictionary *)properties;

@end
