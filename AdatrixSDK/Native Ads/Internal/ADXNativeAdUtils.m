//
//  ADXNativeAdUtils.m
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeAdUtils.h"

NSTimeInterval const kUpdateVisibleCellsInterval = 0.25;

@implementation ADXNativeAdUtils

+ (BOOL)addURLString:(NSString *)urlString toURLArray:(NSMutableArray *)urlArray
{
    if (urlString.length == 0) {
        return NO;
    }

    NSURL *url = [NSURL URLWithString:urlString];
    if (url) {
        [urlArray addObject:url];
        return YES;
    } else {
        return NO;
    }
}
@end
