//
//  ADXNativeAd+Internal.h
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeAd.h"

@interface ADXNativeAd (Internal)

@property (nonatomic, readonly) NSDate *creationDate;

- (NSTimeInterval)requiredSecondsForImpression;
- (void)willAttachToView:(UIView *)view;
- (void)setVisible:(BOOL)visible;
- (NSMutableSet *)impressionTrackers;
- (NSURL *)engagementTrackingURL;

- (void)setEngagementTrackingURL:(NSURL *)engagementTrackingURL;

@end
