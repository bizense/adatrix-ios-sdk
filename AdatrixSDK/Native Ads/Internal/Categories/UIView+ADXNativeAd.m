//
//  UIView+ADXNativeAd.m
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "UIView+ADXNativeAd.h"

#import <objc/runtime.h>

static char ADXNativeAdKey;

@implementation UIView (ADXNativeAd)

- (void)adx_removeNativeAd
{
    [self adx_setNativeAd:nil];
}

- (void)adx_setNativeAd:(ADXNativeAd *)adObject
{
    objc_setAssociatedObject(self, &ADXNativeAdKey, adObject, OBJC_ASSOCIATION_ASSIGN);
}

- (ADXNativeAd *)adx_nativeAd
{
    return (ADXNativeAd *)objc_getAssociatedObject(self, &ADXNativeAdKey);
}

@end