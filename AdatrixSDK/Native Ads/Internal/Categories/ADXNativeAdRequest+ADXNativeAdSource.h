//
//  ADXNativeAdRequest+ADXNativeAdSource.h
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeAdRequest.h"

@interface ADXNativeAdRequest (ADXNativeAdSource)

- (void)startForAdSequence:(NSInteger)adSequence withCompletionHandler:(ADXNativeAdRequestHandler)handler;

@end
