//
//  UIView+ADXNativeAd.h
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADXNativeAd;

@interface UIView (ADXNativeAd)

- (void)adx_setNativeAd:(ADXNativeAd *)adObject;
- (void)adx_removeNativeAd;
- (ADXNativeAd *)adx_nativeAd;

@end
