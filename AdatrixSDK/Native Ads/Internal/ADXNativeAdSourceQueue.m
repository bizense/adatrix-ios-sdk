//
//  ADXNativeAdSourceQueue.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXNativeAdSourceQueue.h"
#import "ADXNativeAd+Internal.h"
#import "ADXNativeAdRequestTargeting.h"
#import "ADXNativeAdRequest+ADXNativeAdSource.h"
#import "ADXLogging.h"
#import "ADXNativeAdError.h"

static NSUInteger const kCacheSizeLimit = 3;
static NSTimeInterval const kMaxBackoffTimeInterval = 300;
static CGFloat const kBaseBackoffTimeMultiplier = 1.5;

@interface ADXNativeAdSourceQueue ()

@property (nonatomic, strong) NSMutableArray *adQueue;
@property (nonatomic, assign) NSUInteger backoffCounter;
@property (nonatomic, copy) NSString *adUnitIdentifier;
@property (nonatomic, copy) NSString *viewId;
@property (nonatomic, strong) ADXNativeAdRequestTargeting *targeting;
@property (nonatomic, assign) BOOL isAdLoading;

@end

@implementation ADXNativeAdSourceQueue

#pragma mark - Object Lifecycle

- (instancetype)initWithAdUnitIdentifier:(NSString *)identifier andTargeting:(ADXNativeAdRequestTargeting *)targeting
{
    self = [super init];
    if (self) {
        _adUnitIdentifier = [identifier copy];
        _targeting = targeting;
        _adQueue = [[NSMutableArray alloc] init];
    }
    return self;
}


#pragma mark - Public Methods

- (ADXNativeAd *)dequeueAd
{
    ADXNativeAd *nextAd = [self.adQueue firstObject];
    [self.adQueue removeObject:nextAd];
    [self loadAds];
    return nextAd;
}

- (ADXNativeAd *)dequeueAdWithMaxAge:(NSTimeInterval)age
{
    ADXNativeAd *nextAd = [self dequeueAd];

    while (nextAd && ![self isAdAgeValid:nextAd withMaxAge:age]) {
        nextAd = [self dequeueAd];
    }

    return nextAd;
}

- (void)addNativeAd:(ADXNativeAd *)nativeAd
{
    [self.adQueue addObject:nativeAd];
}

- (NSUInteger)count
{
    return [self.adQueue count];
}

- (void)cancelRequests
{
    [self resetBackoff];
}

#pragma mark - Internal Logic

- (BOOL)isAdAgeValid:(ADXNativeAd *)ad withMaxAge:(NSTimeInterval)maxAge
{
    NSTimeInterval adAge = [ad.creationDate timeIntervalSinceNow];

    return fabs(adAge) < maxAge;
}

#pragma mark - Ad Requests

- (void)resetBackoff
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    self.backoffCounter = 0;
}

- (void)loadAds
{
    if (self.backoffCounter == 0) {
        [self replenishCache];
    }
}

- (void)replenishCache
{
    if ([self count] >= kCacheSizeLimit || self.isAdLoading) {
        return;
    }

    self.isAdLoading = YES;
    ADXNativeAdRequest *adRequest = [ADXNativeAdRequest requestWithAdUnitIdentifier:self.adUnitIdentifier];
    adRequest.targeting = self.targeting;

    [adRequest startForAdSequence:self.currentSequence withCompletionHandler:^(ADXNativeAdRequest *request, ADXNativeAd *response, NSError *error) {
        if (response && !error) {
            self.backoffCounter = 0;

            [self addNativeAd:response];
            self.currentSequence++;
            if ([self count] == 1) {
                [self.delegate adSourceQueueAdIsAvailable:self];
            }
        } else {
            ADXLogDebug(@"%@", error);
            //increment in this failure case to prevent retrying a request that wasn't bid on.
            //currently under discussion on whether we do this or not.
            if (error.code == ADXNativeAdErrorNoInventory) {
                self.currentSequence++;
            }

            NSTimeInterval backoffTime = [self backoffTime];
            self.backoffCounter++;
            if (backoffTime < kMaxBackoffTimeInterval) {
                [self performSelector:@selector(replenishCache) withObject:nil afterDelay:backoffTime];
                ADXLogDebug(@"Scheduled the backoff to try again in %.1f seconds.", backoffTime);
            } else {
                ADXLogDebug(@"Backoff has timed out", backoffTime);
                self.backoffCounter = 0;
            }
        }
        self.isAdLoading = NO;
        [self loadAds];
    }];
}

- (NSTimeInterval)backoffTime
{
    NSTimeInterval timeInterval = 0;
    if (self.backoffCounter > 0) {
        timeInterval = powf(kBaseBackoffTimeMultiplier, self.backoffCounter - 1);
    }
    return timeInterval;
}



@end
