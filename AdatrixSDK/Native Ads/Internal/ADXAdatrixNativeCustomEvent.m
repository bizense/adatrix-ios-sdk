//
//  ADXAdatrixNativeCustomEvent.m
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXAdatrixNativeCustomEvent.h"
#import "ADXAdatrixNativeAdAdapter.h"
#import "ADXNativeAd+Internal.h"
#import "ADXNativeAdError.h"
#import "ADXLogging.h"
#import "ADXNativeAdUtils.h"

@implementation ADXAdatrixNativeCustomEvent

- (void)requestAdWithCustomEventInfo:(NSDictionary *)info
{
    ADXAdatrixNativeAdAdapter *adAdapter = [[ADXAdatrixNativeAdAdapter alloc] initWithAdProperties:[info mutableCopy]];

    if (adAdapter.properties) {
        ADXNativeAd *interfaceAd = [[ADXNativeAd alloc] initWithAdAdapter:adAdapter];
        [interfaceAd.impressionTrackers addObjectsFromArray:adAdapter.impressionTrackers];

        // Get the image urls so we can download them prior to returning the ad.
        NSMutableArray *imageURLs = [NSMutableArray array];
        for (NSString *key in [info allKeys]) {
            if ([[key lowercaseString] hasSuffix:@"image"] && [[info objectForKey:key] isKindOfClass:[NSString class]]) {
                if (![ADXNativeAdUtils addURLString:[info objectForKey:key] toURLArray:imageURLs]) {
                    [self.delegate nativeCustomEvent:self didFailToLoadAdWithError:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorInvalidServerResponse userInfo:nil]];
                }
            }
        }

        [super precacheImagesWithURLs:imageURLs completionBlock:^(NSArray *errors) {
            if (errors) {
                ADXLogDebug(@"%@", errors);
                ADXLogInfo(@"Error: data received was invalid.");
                [self.delegate nativeCustomEvent:self didFailToLoadAdWithError:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorInvalidServerResponse userInfo:nil]];
            } else {
                [self.delegate nativeCustomEvent:self didLoadAd:interfaceAd];
            }
        }];
    } else {
        [self.delegate nativeCustomEvent:self didFailToLoadAdWithError:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorInvalidServerResponse userInfo:nil]];
    }

}

@end
