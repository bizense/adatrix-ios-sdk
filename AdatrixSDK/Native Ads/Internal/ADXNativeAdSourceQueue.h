//
//  ADXNativeAdSourceQueue.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ADXNativeAdRequestTargeting;
@class ADXNativeAd;

@protocol ADXNativeAdSourceQueueDelegate;

@interface ADXNativeAdSourceQueue : NSObject

@property (nonatomic, weak) id <ADXNativeAdSourceQueueDelegate> delegate;
@property (nonatomic, assign) NSUInteger currentSequence;


- (instancetype)initWithAdUnitIdentifier:(NSString *)identifier andTargeting:(ADXNativeAdRequestTargeting *)targeting;
- (ADXNativeAd *)dequeueAdWithMaxAge:(NSTimeInterval)age;
- (NSUInteger)count;
- (void)loadAds;
- (void)cancelRequests;

@end

@protocol ADXNativeAdSourceQueueDelegate <NSObject>

- (void)adSourceQueueAdIsAvailable:(ADXNativeAdSourceQueue *)source;

@end
