//
//  ADXTableViewAdManager.m
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXTableViewAdManager.h"

#import "ADXTableViewCellImpressionTracker.h"
#import "ADXNativeAd+Internal.h"
#import "ADXLogging.h"
#import "ADXNativeAdRendering.h"
#import "UIView+ADXNativeAd.h"

@interface ADXTableViewAdManager () <ADXTableViewCellImpressionTrackerDelegate>

@property (nonatomic, strong) NSMutableSet *ads;
@property (nonatomic, strong) NSMutableSet *cells;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ADXTableViewCellImpressionTracker *impressionTracker;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation ADXTableViewAdManager

- (id)initWithTableView:(UITableView *)tableView
{
    self = [super init];
    if (self) {
        _tableView = tableView;
        _impressionTracker = [[ADXTableViewCellImpressionTracker alloc] initWithTableView:tableView
                                                                                 delegate:self];
        [_impressionTracker startTracking];

        _ads = [[NSMutableSet alloc] init];
        _cells = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [self removeAssociatedAdObjectsFromCells];

    [_impressionTracker stopTracking];
}

- (void)removeAssociatedAdObjectsFromCells
{
    for (UITableViewCell *cell in _cells) {
        [cell adx_removeNativeAd];
    }
}

- (UITableViewCell *)adCellForAd:(ADXNativeAd *)adObject cellClass:(Class)cellClass
{
    NSString *identifier = [NSString stringWithFormat:@"ADX_Cell_Class_%@", NSStringFromClass(cellClass)];
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [self.cells addObject:cell];
    }

    [self.ads addObject:adObject];
    [cell adx_setNativeAd:adObject];

    if ([cell conformsToProtocol:@protocol(ADXNativeAdRendering)]) {
        [adObject willAttachToView:cell];
        [(id<ADXNativeAdRendering>)cell layoutAdAssets:adObject];
    } else {
        ADXLogWarn(@"A cell class (%@) passed to -adCellForAd:cellClass: does not conform to the "
                  @"ADXNativeAdRendering protocol. The resultant cell will not display any ad assets.",
                  NSStringFromClass(cellClass));
    }

    return cell;
}

#pragma mark - <ADXTableViewCellImpressionTracker>

- (void)tracker:(ADXTableViewCellImpressionTracker *)tracker didDetectVisibleRowsAtIndexPaths:(NSArray *)indexPaths
{
    NSMutableSet *visibleAds = [NSMutableSet set];

    for (NSIndexPath *path in indexPaths) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:path];
        if ([self.cells containsObject:cell]) {
            ADXNativeAd *ad = [cell adx_nativeAd];

            // Edge case: if the same ad is being displayed in multiple on-screen cells,
            // simultaneously, don't set its visibility more than once (side effects).
            if (![visibleAds containsObject:ad]) {
                ad.visible = YES;
                [visibleAds addObject:ad];
            }
        }
    }

    NSMutableSet *invisibleAds = [NSMutableSet setWithSet:self.ads];
    [invisibleAds minusSet:visibleAds];

    for (ADXNativeAd *ad in invisibleAds) {
        ad.visible = NO;
    }
}

@end
