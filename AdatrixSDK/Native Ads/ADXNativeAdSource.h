//
//  ADXNativeAdSource.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADXNativeAdSourceDelegate.h"
@class ADXNativeAdRequestTargeting;

@interface ADXNativeAdSource : NSObject

@property (nonatomic, weak) id <ADXNativeAdSourceDelegate> delegate;

+ (instancetype)source;
- (void)loadAdsWithAdUnitIdentifier:(NSString *)identifier viewID:(NSString *)viewID andTargeting:(ADXNativeAdRequestTargeting *)targeting;
- (void)deleteCacheForAdUnitIdentifier:(NSString *)identifier;
- (id)dequeueAdForAdUnitIdentifier:(NSString *)identifier;


@end
