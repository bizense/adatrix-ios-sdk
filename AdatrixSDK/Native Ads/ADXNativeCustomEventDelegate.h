//
//  ADXNativeCustomEventDelegate.h
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXNativeAd;
@class ADXNativeCustomEvent;

/**
 * Instances of your custom subclass of `ADXNativeCustomEvent` will have an
 * `ADXNativeCustomEventDelegate` delegate object. You use this delegate to communicate progress
 * (such as whether an ad has loaded successfully) back to the Adatrix SDK.
 */
@protocol ADXNativeCustomEventDelegate <NSObject>

/**
 * This method is called when the ad and all required ad assets are loaded.
 *
 * @param event You should pass `self` to allow the Adatrix SDK to associate this event with the
 * correct instance of your custom event.
 * @param adObject An `ADXNativeAd` object, representing the ad that was retrieved.
 */
- (void)nativeCustomEvent:(ADXNativeCustomEvent *)event didLoadAd:(ADXNativeAd *)adObject;

/**
 * This method is called when the ad or any required ad assets fail to load.
 *
 * @param event You should pass `self` to allow the Adatrix SDK to associate this event with the
 * correct instance of your custom event.
 * @param error (*optional*) You may pass an error describing the failure.
 */
- (void)nativeCustomEvent:(ADXNativeCustomEvent *)event didFailToLoadAdWithError:(NSError *)error;

@end
