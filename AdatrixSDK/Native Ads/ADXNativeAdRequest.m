//
//  ADXNativeAdRequest.m
//  Copyright (c) 2013 Adatrix. All rights reserved.
//

#import "ADXNativeAdRequest.h"

#import "ADXAdServerURLBuilder.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXNativeAdError.h"
#import "ADXNativeAd+Internal.h"
#import "ADXNativeAdRequestTargeting.h"
#import "ADXLogging.h"
#import "ADXImageDownloadQueue.h"
#import "ADXConstants.h"
#import "ADXNativeCustomEventDelegate.h"
#import "ADXNativeCustomEvent.h"
#import "ADXInstanceProvider.h"
#import "NSJSONSerialization+ADXAdditions.h"
#import "ADXAdServerCommunicator.h"

#import "ADXAdatrixNativeCustomEvent.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

@interface ADXNativeAdRequest () <ADXNativeCustomEventDelegate, ADXAdServerCommunicatorDelegate>

@property (nonatomic, copy) NSString *adUnitIdentifier;
@property (nonatomic, copy) NSString *viewId;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) ADXAdServerCommunicator *communicator;
@property (nonatomic, copy) ADXNativeAdRequestHandler completionHandler;
@property (nonatomic, strong) ADXNativeCustomEvent *nativeCustomEvent;
@property (nonatomic, strong) ADXAdConfiguration *adConfiguration;
@property (nonatomic, assign) BOOL loading;

@end

@implementation ADXNativeAdRequest

- (id)initWithAdUnitIdentifier:(NSString *)identifier
{
    self = [super init];
    if (self) {
        _adUnitIdentifier = [identifier copy];
        _communicator = [[ADXCoreInstanceProvider sharedProvider] buildADXAdServerCommunicatorWithDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [_communicator cancel];
    [_communicator setDelegate:nil];
    [_nativeCustomEvent setDelegate:nil];
}

#pragma mark - Public

+ (ADXNativeAdRequest *)requestWithAdUnitIdentifier:(NSString *)identifier
{
    return [[self alloc] initWithAdUnitIdentifier:identifier];
}

- (void)startWithCompletionHandler:(ADXNativeAdRequestHandler)handler
{
    if (handler) {
        self.URL = [ADXAdServerURLBuilder URLWithAdUnitID:self.adUnitIdentifier
                                                   viewID:self.viewId
                                                keywords:self.targeting.keywords
                                                location:self.targeting.location
                                    versionParameterName:@"nsv"
                                                 version:ADX_SDK_VERSION
                                                 testing:NO
                                           desiredAssets:[self.targeting.desiredAssets allObjects]];

        [self assignCompletionHandler:handler];

        [self loadAdWithURL:self.URL];
    } else {
        ADXLogWarn(@"Native Ad Request did not start - requires completion handler block.");
    }
}

- (void)startForAdSequence:(NSInteger)adSequence withCompletionHandler:(ADXNativeAdRequestHandler)handler
{
    if (handler) {
        self.URL = [ADXAdServerURLBuilder URLWithAdUnitID:self.adUnitIdentifier
                                                   viewID:self.viewId
                                                keywords:self.targeting.keywords
                                                location:self.targeting.location
                                    versionParameterName:@"nsv"
                                                 version:ADX_SDK_VERSION
                                                 testing:NO
                                           desiredAssets:[self.targeting.desiredAssets allObjects]
                                              adSequence:adSequence];

        [self assignCompletionHandler:handler];

        [self loadAdWithURL:self.URL];
    } else {
        ADXLogWarn(@"Native Ad Request did not start - requires completion handler block.");
    }
}

#pragma mark - Private

- (void)assignCompletionHandler:(ADXNativeAdRequestHandler)handler
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
    // we explicitly create a block retain cycle here to prevent self from being deallocated if the developer
    // removes his strong reference to the request. This retain cycle is broken in
    // - (void)completeAdRequestWithAdObject:(ADXNativeAd *)adObject error:(NSError *)error
    // when self.completionHandler is set to nil.
    self.completionHandler = ^(ADXNativeAdRequest *request, ADXNativeAd *response, NSError *error) {
        handler(self, response, error);
    };
#pragma clang diagnostic pop
}

- (void)loadAdWithURL:(NSURL *)URL
{
    if (self.loading) {
        ADXLogWarn(@"Native ad request is already loading an ad. Wait for previous load to finish.");
        return;
    }

    ADXLogInfo(@"Starting ad request with URL: %@", self.URL);

    self.loading = YES;
    [self.communicator loadURL:URL];
}

- (void)getAdWithConfiguration:(ADXAdConfiguration *)configuration
{
    if (configuration.customEventClass) {
        ADXLogInfo(@"Looking for custom event class named %@.", configuration.customEventClass);
    }

    // Adserver doesn't return a customEventClass for Adatrix native ads
    if ([configuration.networkType isEqualToString:kAdTypeNative] && configuration.customEventClass == nil) {
        configuration.customEventClass = [ADXAdatrixNativeCustomEvent class];
        NSError *error;
        NSDictionary *classData = [NSJSONSerialization adx_JSONObjectWithData:configuration.adResponseData options:0 clearNullObjects:YES error:&error];

        configuration.customEventClassData = classData;
    }

    self.nativeCustomEvent = [[ADXInstanceProvider sharedProvider] buildNativeCustomEventFromCustomClass:configuration.customEventClass delegate:self];

    if (self.nativeCustomEvent) {
        [self.nativeCustomEvent requestAdWithCustomEventInfo:configuration.customEventClassData];
    } else if ([[self.adConfiguration.failoverURL absoluteString] length]) {
        self.loading = NO;
        [self loadAdWithURL:self.adConfiguration.failoverURL];
    } else {
        [self completeAdRequestWithAdObject:nil error:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorInvalidServerResponse userInfo:nil]];
    }
}

- (void)completeAdRequestWithAdObject:(ADXNativeAd *)adObject error:(NSError *)error
{
    self.loading = NO;

    if (!error) {
        ADXLogInfo(@"Successfully loaded native ad.");
    } else {
        ADXLogError(@"Native ad failed to load with error: %@", error);
    }

    if (self.completionHandler) {
        self.completionHandler(self, adObject, error);
        self.completionHandler = nil;
    }
}

#pragma mark - <ADXAdServerCommunicatorDelegate>

- (void)communicatorDidReceiveAdConfiguration:(ADXAdConfiguration *)configuration
{
    self.adConfiguration = configuration;

    if (configuration.adUnitWarmingUp) {
        ADXLogInfo(kADXWarmingUpErrorLogFormatWithAdUnitID, self.adUnitIdentifier);
        [self completeAdRequestWithAdObject:nil error:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorAdUnitWarmingUp userInfo:nil]];
        return;
    }

    if ([configuration.networkType isEqualToString:kAdTypeClear]) {
        ADXLogInfo(kADXClearErrorLogFormatWithAdUnitID, self.adUnitIdentifier);
        [self completeAdRequestWithAdObject:nil error:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorNoInventory userInfo:nil]];
        return;
    }

    ADXLogInfo(@"Received data from Adatrix to construct native ad.\n");
    [self getAdWithConfiguration:configuration];
}

- (void)communicatorDidFailWithError:(NSError *)error
{
    ADXLogDebug(@"Error: Couldn't retrieve an ad from Adatrix. Message: %@", error);

    [self completeAdRequestWithAdObject:nil error:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorHTTPError userInfo:nil]];
}

#pragma mark - <ADXNativeCustomEventDelegate>

- (void)nativeCustomEvent:(ADXNativeCustomEvent *)event didLoadAd:(ADXNativeAd *)adObject
{
    // Take the click tracking URL from the header if the ad object doesn't already have one.
    [adObject setEngagementTrackingURL:(adObject.engagementTrackingURL ? : self.adConfiguration.clickTrackingURL)];

    // Add the impression tracker from the header to our set.
    if (self.adConfiguration.impressionTrackingURL) {
        [adObject.impressionTrackers addObject:[self.adConfiguration.impressionTrackingURL absoluteString]];
    }

    // Error if we don't have click tracker or impression trackers.
    if (!adObject.engagementTrackingURL || adObject.impressionTrackers.count < 1) {
        [self completeAdRequestWithAdObject:nil error:[NSError errorWithDomain:AdatrixNativeAdsSDKDomain code:ADXNativeAdErrorInvalidServerResponse userInfo:nil]];
    } else {
        [self completeAdRequestWithAdObject:adObject error:nil];
    }
}

- (void)nativeCustomEvent:(ADXNativeCustomEvent *)event didFailToLoadAdWithError:(NSError *)error
{
    if ([[self.adConfiguration.failoverURL absoluteString] length]) {
        self.loading = NO;
        [self loadAdWithURL:self.adConfiguration.failoverURL];
    } else {
        [self completeAdRequestWithAdObject:nil error:error];
    }
}


@end
