//
//  Adatrix.m
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "Adatrix.h"
#import "ADXConstants.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXGeolocationProvider.h"
#import "ADXRewardedVideo.h"

@interface Adatrix ()

@property (nonatomic, strong) NSArray *globalMediationSettings;

@end

@implementation Adatrix

+ (Adatrix *)sharedInstance
{
    static Adatrix *sharedInstance = nil;
    static dispatch_once_t initOnceToken;
    dispatch_once(&initOnceToken, ^{
        sharedInstance = [[Adatrix alloc] init];
    });
    return sharedInstance;
}

- (void)setLocationUpdatesEnabled:(BOOL)locationUpdatesEnabled
{
    _locationUpdatesEnabled = locationUpdatesEnabled;
    [[[ADXCoreInstanceProvider sharedProvider] sharedADXGeolocationProvider] setLocationUpdatesEnabled:locationUpdatesEnabled];
}

- (void)start
{

}

- (NSString *)version
{
    return ADX_SDK_VERSION;
}

- (NSString *)bundleIdentifier
{
    return ADX_BUNDLE_IDENTIFIER;
}

- (void)initializeRewardedVideoWithGlobalMediationSettings:(NSArray *)globalMediationSettings delegate:(id<ADXRewardedVideoDelegate>)delegate
{
    // initializeWithDelegate: is a known private initialization method on ADXRewardedVideo. So we forward the initialization call to that class.
    [ADXRewardedVideo performSelector:@selector(initializeWithDelegate:) withObject:delegate];
    self.globalMediationSettings = globalMediationSettings;
}

- (id<ADXMediationSettingsProtocol>)globalMediationSettingsForClass:(Class)aClass
{
    NSArray *mediationSettingsCollection = self.globalMediationSettings;

    for (id<ADXMediationSettingsProtocol> settings in mediationSettingsCollection) {
        if ([settings isKindOfClass:aClass]) {
            return settings;
        }
    }

    return nil;
}

@end
