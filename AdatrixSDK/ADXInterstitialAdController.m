//
//  ADXInterstitialAdController.m
//  Adatrix
//
//  Copyright (c) 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXInterstitialAdController.h"

#import "ADXLogging.h"
#import "ADXInstanceProvider.h"
#import "ADXInterstitialAdManager.h"
#import "ADXInterstitialAdManagerDelegate.h"

@interface ADXInterstitialAdController () <ADXInterstitialAdManagerDelegate>

@property (nonatomic, strong) ADXInterstitialAdManager *manager;

+ (NSMutableArray *)sharedInterstitials;
- (id)initWithAdUnitId:(NSString *)adUnitId viewID:(NSString *)viewId;

@end

@implementation ADXInterstitialAdController

@synthesize manager = _manager;
@synthesize delegate = _delegate;
@synthesize adUnitId = _adUnitId;
@synthesize viewId = _viewId;
@synthesize keywords = _keywords;
@synthesize location = _location;
@synthesize testing = _testing;

- (id)initWithAdUnitId:(NSString *)adUnitId viewID:(NSString *)viewId
{
    if (self = [super init]) {
        self.manager = [[ADXInstanceProvider sharedProvider] buildADXInterstitialAdManagerWithDelegate:self];
        self.adUnitId = adUnitId;
        self.viewId = viewId;
    }
    return self;
}

- (void)dealloc
{
    [self.manager setDelegate:nil];
}

#pragma mark - Public

+ (ADXInterstitialAdController *)interstitialAdControllerForAdUnitId:(NSString *)adUnitId viewID:(NSString *)viewId
{
    NSMutableArray *interstitials = [[self class] sharedInterstitials];

    @synchronized(self) {
        // Find the correct ad controller based on the ad unit ID.
        ADXInterstitialAdController *interstitial = nil;
        for (ADXInterstitialAdController *currentInterstitial in interstitials) {
            if ([currentInterstitial.adUnitId isEqualToString:adUnitId]) {
                interstitial = currentInterstitial;
                break;
            }
        }

        // Create a new ad controller for this ad unit ID if one doesn't already exist.
        if (!interstitial) {
            interstitial = [[[self class] alloc] initWithAdUnitId:adUnitId viewID:viewId];
            [interstitials addObject:interstitial];
        }

        return interstitial;
    }
}

- (BOOL)ready
{
    return self.manager.ready;
}

- (void)loadAd
{
    [self.manager loadInterstitialWithAdUnitID:self.adUnitId
                                        viewID:self.viewId
                                      keywords:self.keywords
                                      location:self.location
                                       testing:self.testing];
}

- (void)showFromViewController:(UIViewController *)controller
{
    if (!controller) {
        ADXLogWarn(@"The interstitial could not be shown: "
                  @"a nil view controller was passed to -showFromViewController:.");
        return;
    }

    if (![controller.view.window isKeyWindow]) {
        ADXLogWarn(@"Attempted to present an interstitial ad in non-key window. The ad may not render properly");
    }

    [self.manager presentInterstitialFromViewController:controller];
}

#pragma mark - Internal

+ (NSMutableArray *)sharedInterstitials
{
    static NSMutableArray *sharedInterstitials;

    @synchronized(self) {
        if (!sharedInterstitials) {
            sharedInterstitials = [NSMutableArray array];
        }
    }

    return sharedInterstitials;
}

#pragma mark - ADXInterstitialAdManagerDelegate

- (ADXInterstitialAdController *)interstitialAdController
{
    return self;
}

- (id)interstitialDelegate
{
    return self.delegate;
}

- (void)managerDidLoadInterstitial:(ADXInterstitialAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(interstitialDidLoadAd:)]) {
        [self.delegate interstitialDidLoadAd:self];
    }
}

- (void)manager:(ADXInterstitialAdManager *)manager
        didFailToLoadInterstitialWithError:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(interstitialDidFailToLoadAd:)]) {
        [self.delegate interstitialDidFailToLoadAd:self];
    }
}

- (void)managerWillPresentInterstitial:(ADXInterstitialAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(interstitialWillAppear:)]) {
        [self.delegate interstitialWillAppear:self];
    }
}

- (void)managerDidPresentInterstitial:(ADXInterstitialAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(interstitialDidAppear:)]) {
        [self.delegate interstitialDidAppear:self];
    }
}

- (void)managerWillDismissInterstitial:(ADXInterstitialAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(interstitialWillDisappear:)]) {
        [self.delegate interstitialWillDisappear:self];
    }
}

- (void)managerDidDismissInterstitial:(ADXInterstitialAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(interstitialDidDisappear:)]) {
        [self.delegate interstitialDidDisappear:self];
    }
}

- (void)managerDidExpireInterstitial:(ADXInterstitialAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(interstitialDidExpire:)]) {
        [self.delegate interstitialDidExpire:self];
    }
}

- (void)managerDidReceiveTapEventFromInterstitial:(ADXInterstitialAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(interstitialDidReceiveTapEvent:)]) {
        [self.delegate interstitialDidReceiveTapEvent:self];
    }
}

#pragma mark - Deprecated

+ (NSMutableArray *)sharedInterstitialAdControllers
{
    return [[self class] sharedInterstitials];
}

+ (void)removeSharedInterstitialAdController:(ADXInterstitialAdController *)controller
{
    [[[self class] sharedInterstitials] removeObject:controller];
}

- (void)customEventDidLoadAd
{
    [self.manager customEventDidLoadAd];
}

- (void)customEventDidFailToLoadAd
{
    [self.manager customEventDidFailToLoadAd];
}

- (void)customEventActionWillBegin
{
    [self.manager customEventActionWillBegin];
}

@end
