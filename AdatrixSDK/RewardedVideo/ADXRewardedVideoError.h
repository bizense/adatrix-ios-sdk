//
//  ADXRewardedVideoError.h
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ADXRewardedVideoAdErrorUnknown = -1,

    ADXRewardedVideoAdErrorTimeout = -1000,
    ADXRewardedVideoAdErrorAdUnitWarmingUp = -1001,
    ADXRewardedVideoAdErrorNoAdsAvailable = -1100,
    ADXRewardedVideoAdErrorInvalidCustomEvent = -1200,
    ADXRewardedVideoAdErrorMismatchingAdTypes = -1300,
    ADXRewardedVideoAdErrorAdAlreadyPlayed = -1400,
    ADXRewardedVideoAdErrorInvalidAdUnitID = -1500
} ADXRewardedVideoErrorCode;

extern NSString * const AdatrixRewardedVideoAdsSDKDomain;
