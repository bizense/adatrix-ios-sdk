//
//  ADXRewardedVideoAdapter.h
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ADXAdConfiguration;
@class ADXRewardedVideoReward;

@protocol ADXRewardedVideoAdapterDelegate;
@protocol ADXMediationSettingsProtocol;

/**
 * `ADXRewardedVideoAdapter` directly communicates with the appropriate custom event to
 * load and show a rewarded video. It is also the class that handles impression
 * and click tracking. Finally, the class will report a failure to load an ad if the ad
 * takes too long to load.
 */
@interface ADXRewardedVideoAdapter : NSObject

@property (nonatomic, weak) id<ADXRewardedVideoAdapterDelegate> delegate;

- (instancetype)initWithDelegate:(id<ADXRewardedVideoAdapterDelegate>)delegate;

/**
 * Called to retrieve an ad once we get a response from the server.
 *
 * @param configuration Contains the details about the ad we are loading.
 */
- (void)getAdWithConfiguration:(ADXAdConfiguration *)configuration;

/**
 * Tells the caller whether the underlying ad network currently has an ad available for presentation.
 */
- (BOOL)hasAdAvailable;

/**
 * Plays a rewarded video ad.
 *
 * @param viewController Presents the rewarded video ad from viewController.
 */
- (void)presentRewardedVideoFromViewController:(UIViewController *)viewController;

/**
 * This method is called when another ad unit has played a rewarded video from the same network this adapter's custom event
 * represents.
 */
- (void)handleAdPlayedForCustomEventNetwork;

@end

@protocol ADXRewardedVideoAdapterDelegate <NSObject>

- (id<ADXMediationSettingsProtocol>)instanceMediationSettingsForClass:(Class)aClass;

- (void)rewardedVideoDidLoadForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoDidFailToLoadForAdapter:(ADXRewardedVideoAdapter *)adapter error:(NSError *)error;
- (void)rewardedVideoDidExpireForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoDidFailToPlayForAdapter:(ADXRewardedVideoAdapter *)adapter error:(NSError *)error;
- (void)rewardedVideoWillAppearForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoDidAppearForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoWillDisappearForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoDidDisappearForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoDidReceiveTapEventForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoWillLeaveApplicationForAdapter:(ADXRewardedVideoAdapter *)adapter;
- (void)rewardedVideoShouldRewardUserForAdapter:(ADXRewardedVideoAdapter *)adapter reward:(ADXRewardedVideoReward *)reward;

@end