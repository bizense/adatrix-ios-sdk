//
//  ADXRewardedVideoAdapter.m
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import "ADXRewardedVideoAdapter.h"

#import "ADXAdConfiguration.h"
#import "ADXAnalyticsTracker.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXRewardedVideoError.h"
#import "ADXRewardedVideoCustomEvent.h"
#import "ADXInstanceProvider.h"
#import "ADXLogging.h"
#import "ADXTimer.h"

@interface ADXRewardedVideoAdapter () <ADXRewardedVideoCustomEventDelegate>

@property (nonatomic, strong) ADXRewardedVideoCustomEvent *rewardedVideoCustomEvent;
@property (nonatomic, strong) ADXAdConfiguration *configuration;
@property (nonatomic, strong) ADXTimer *timeoutTimer;
@property (nonatomic, assign) BOOL hasTrackedImpression;
@property (nonatomic, assign) BOOL hasTrackedClick;
// Once an ad successfully loads, we want to block sending more successful load events.
@property (nonatomic, assign) BOOL hasSuccessfullyLoaded;
// Since we only notify the application of one success per load, we also only notify the application of one expiration per success.
@property (nonatomic, assign) BOOL hasExpired;

@end

@implementation ADXRewardedVideoAdapter

- (instancetype)initWithDelegate:(id<ADXRewardedVideoAdapterDelegate>)delegate
{
    if (self = [super init]) {
        _delegate = delegate;
    }

    return self;
}

- (void)dealloc
{
    // The rewarded video system now no longer holds references to the custom event. The custom event may have a system
    // that holds extra references to the custom event. Let's tell the custom event that we no longer need it.
    [_rewardedVideoCustomEvent handleCustomEventInvalidated];

    [_timeoutTimer invalidate];

    // Make sure the custom event isn't released synchronously as objects owned by the custom event
    // may do additional work after a callback that results in dealloc being called
    [[ADXCoreInstanceProvider sharedProvider] keepObjectAliveForCurrentRunLoopIteration:_rewardedVideoCustomEvent];
}

- (void)getAdWithConfiguration:(ADXAdConfiguration *)configuration
{
    ADXLogInfo(@"Looking for custom event class named %@.", configuration.customEventClass);

    self.configuration = configuration;

    self.rewardedVideoCustomEvent = [[ADXInstanceProvider sharedProvider] buildRewardedVideoCustomEventFromCustomClass:configuration.customEventClass delegate:self];

    if (self.rewardedVideoCustomEvent) {
        [self startTimeoutTimer];
        [self.rewardedVideoCustomEvent requestRewardedVideoWithCustomEventInfo:configuration.customEventClassData];
    } else {
        NSError *error = [NSError errorWithDomain:AdatrixRewardedVideoAdsSDKDomain code:ADXRewardedVideoAdErrorInvalidCustomEvent userInfo:nil];
        [self.delegate rewardedVideoDidFailToLoadForAdapter:self error:error];
    }
}

- (BOOL)hasAdAvailable
{
    return [self.rewardedVideoCustomEvent hasAdAvailable];
}

- (void)presentRewardedVideoFromViewController:(UIViewController *)viewController
{
    [self.rewardedVideoCustomEvent presentRewardedVideoFromViewController:viewController];
}

- (void)handleAdPlayedForCustomEventNetwork
{
    [self.rewardedVideoCustomEvent handleAdPlayedForCustomEventNetwork];
}

#pragma mark - Private

- (void)startTimeoutTimer
{
    NSTimeInterval timeInterval = (self.configuration && self.configuration.adTimeoutInterval >= 0) ?
    self.configuration.adTimeoutInterval : REWARDED_VIDEO_TIMEOUT_INTERVAL;

    if (timeInterval > 0) {
        self.timeoutTimer = [[ADXCoreInstanceProvider sharedProvider] buildADXTimerWithTimeInterval:timeInterval
                                                                                           target:self
                                                                                         selector:@selector(timeout)
                                                                                          repeats:NO];

        [self.timeoutTimer scheduleNow];
    }
}

- (void)timeout
{
    NSError *error = [NSError errorWithDomain:AdatrixRewardedVideoAdsSDKDomain code:ADXRewardedVideoAdErrorTimeout userInfo:nil];
    [self.delegate rewardedVideoDidFailToLoadForAdapter:self error:error];
}

- (void)didStopLoading
{
    [self.timeoutTimer invalidate];
}

#pragma mark - Metrics

- (void)trackImpression
{
    [[[ADXCoreInstanceProvider sharedProvider] sharedADXAnalyticsTracker] trackImpressionForConfiguration:self.configuration];
}

- (void)trackClick
{
    [[[ADXCoreInstanceProvider sharedProvider] sharedADXAnalyticsTracker] trackClickForConfiguration:self.configuration];
}

#pragma mark - ADXRewardedVideoCustomEventDelegate

- (id<ADXMediationSettingsProtocol>)instanceMediationSettingsForClass:(Class)aClass
{
    return [self.delegate instanceMediationSettingsForClass:aClass];
}

- (void)rewardedVideoDidLoadAdForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    // Don't report multiple successful loads. Backing ad networks may replenish their caches triggering multiple successful load
    // callbacks.
    if (self.hasSuccessfullyLoaded) {
        return;
    }

    self.hasSuccessfullyLoaded = YES;
    [self didStopLoading];
    [self.delegate rewardedVideoDidLoadForAdapter:self];
}

- (void)rewardedVideoDidFailToLoadAdForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent error:(NSError *)error
{
    // Detach the custom event from the adapter. An ad *may* end up, after some time, loading successfully
    // from the underlying network, but we don't want to bubble up the event to the application since we
    // are reporting a timeout here.
    [self.rewardedVideoCustomEvent handleCustomEventInvalidated];
    self.rewardedVideoCustomEvent = nil;
    
    [self didStopLoading];
    [self.delegate rewardedVideoDidFailToLoadForAdapter:self error:error];
}

- (void)rewardedVideoDidExpireForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    // Only allow one expire per custom event to match up with one successful load callback per custom event.
    if (self.hasExpired) {
        return;
    }

    self.hasExpired = YES;
    [self.delegate rewardedVideoDidExpireForAdapter:self];
}

- (void)rewardedVideoDidFailToPlayForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent error:(NSError *)error
{
    [self.delegate rewardedVideoDidFailToPlayForAdapter:self error:error];
}

- (void)rewardedVideoWillAppearForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    [self.delegate rewardedVideoWillAppearForAdapter:self];
}

- (void)rewardedVideoDidAppearForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    if ([self.rewardedVideoCustomEvent enableAutomaticImpressionAndClickTracking] && !self.hasTrackedImpression) {
        self.hasTrackedImpression = YES;
        [self trackImpression];
    }

    [self.delegate rewardedVideoDidAppearForAdapter:self];
}

- (void)rewardedVideoWillDisappearForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    [self.delegate rewardedVideoWillDisappearForAdapter:self];
}

- (void)rewardedVideoDidDisappearForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    [self.delegate rewardedVideoDidDisappearForAdapter:self];
}

- (void)rewardedVideoWillLeaveApplicationForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    [self.delegate rewardedVideoWillLeaveApplicationForAdapter:self];
}

- (void)rewardedVideoDidReceiveTapEventForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent
{
    if ([self.rewardedVideoCustomEvent enableAutomaticImpressionAndClickTracking] && !self.hasTrackedClick) {
        self.hasTrackedClick = YES;
        [self trackClick];
    }

    [self.delegate rewardedVideoDidReceiveTapEventForAdapter:self];
}

- (void)rewardedVideoShouldRewardUserForCustomEvent:(ADXRewardedVideoCustomEvent *)customEvent reward:(ADXRewardedVideoReward *)reward
{
    if (reward) {
        [self.delegate rewardedVideoShouldRewardUserForAdapter:self reward:reward];
    }
}

@end
