//
//  ADXRewardedVideoAdManager.h
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ADXRewardedVideoReward;
@protocol ADXRewardedVideoAdManagerDelegate;

/**
 * `ADXRewardedVideoAdManager` represents a rewarded video for a single ad unit ID. This is the object that
 * `ADXRewardedVideo` uses to load and present the ad.
 */
@interface ADXRewardedVideoAdManager : NSObject

@property (nonatomic, weak) id<ADXRewardedVideoAdManagerDelegate> delegate;
@property (nonatomic, readonly) NSString *adUnitID;
@property (nonatomic, readonly) NSString *viewID;
@property (nonatomic, strong) NSArray *mediationSettings;

- (instancetype)initWithAdUnitID:(NSString *)adUnitID viewID:(NSString *)viewID delegate:(id<ADXRewardedVideoAdManagerDelegate>)delegate;

/**
 * Returns the custom event class type.
 */
- (Class)customEventClass;

/**
 * Loads a rewarded video ad with the ad manager's ad unit ID.
 *
 * If this method is called when an ad is already available and we haven't already played a video for the last time we loaded an ad,
 * the object will simply notify the delegate that an ad loaded.
 *
 * However, if an ad has been played for the last time a load was issued and load is called again, the method will request a new ad.
 */
- (void)loadRewardedVideoAd;

/**
 * Tells the caller whether the underlying ad network currently has an ad available for presentation.
 */
- (BOOL)hasAdAvailable;

/**
 * Plays a rewarded video ad.
 *
 * @param viewController Presents the rewarded video ad from viewController.
 */
- (void)presentRewardedVideoAdFromViewController:(UIViewController *)viewController;

/**
 * This method is called when another ad unit has played a rewarded video from the same network this ad manager's custom event
 * represents.
 */
- (void)handleAdPlayedForCustomEventNetwork;

@end

@protocol ADXRewardedVideoAdManagerDelegate <NSObject>

- (void)rewardedVideoDidLoadForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoDidFailToLoadForAdManager:(ADXRewardedVideoAdManager *)manager error:(NSError *)error;
- (void)rewardedVideoDidExpireForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoDidFailToPlayForAdManager:(ADXRewardedVideoAdManager *)manager error:(NSError *)error;
- (void)rewardedVideoWillAppearForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoDidAppearForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoWillDisappearForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoDidDisappearForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoDidReceiveTapEventForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoWillLeaveApplicationForAdManager:(ADXRewardedVideoAdManager *)manager;
- (void)rewardedVideoShouldRewardUserForAdManager:(ADXRewardedVideoAdManager *)manager reward:(ADXRewardedVideoReward *)reward;

@end
