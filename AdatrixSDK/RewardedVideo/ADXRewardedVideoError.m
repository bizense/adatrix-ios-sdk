//
//  ADXRewardedVideoError.m
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import "ADXRewardedVideoError.h"

NSString * const AdatrixRewardedVideoAdsSDKDomain = @"AdatrixRewardedVideoAdsSDKDomain";