//
//  ADXRewardedVideoReward.m
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import "ADXRewardedVideoReward.h"

NSString *const kADXRewardedVideoRewardCurrencyTypeUnspecified = @"ADXAdatrixRewardedVideoRewardCurrencyTypeUnspecified";
NSInteger const kADXRewardedVideoRewardCurrencyAmountUnspecified = NSIntegerMin;

@implementation ADXRewardedVideoReward

- (instancetype)initWithCurrencyType:(NSString *)currencyType amount:(NSNumber *)amount
{
    if (self = [super init]) {
        _currencyType = [currencyType copy];
        _amount = amount;
    }

    return self;
}

- (instancetype)initWithCurrencyAmount:(NSNumber *)amount
{
    return [self initWithCurrencyType:kADXRewardedVideoRewardCurrencyTypeUnspecified amount:amount];
}

@end
