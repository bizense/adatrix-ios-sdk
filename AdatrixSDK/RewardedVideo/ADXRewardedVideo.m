//
//  ADXRewardedVideo.m
//  AdatrixSDK
//
//  Copyright (c) 2015 Adatrix. All rights reserved.
//

#import "ADXRewardedVideo.h"
#import "ADXLogging.h"
#import "ADXRewardedVideoAdManager.h"
#import "ADXInstanceProvider.h"
#import "ADXRewardedVideoError.h"

static ADXRewardedVideo *gSharedInstance = nil;

@interface ADXRewardedVideo () <ADXRewardedVideoAdManagerDelegate>

@property (nonatomic, strong) NSMutableDictionary *rewardedVideoAdManagers;
@property (nonatomic, weak) id<ADXRewardedVideoDelegate> delegate;

+ (ADXRewardedVideo *)sharedInstance;

@end

@implementation ADXRewardedVideo

- (instancetype)init
{
    if (self = [super init]) {
        _rewardedVideoAdManagers = [[NSMutableDictionary alloc] init];
    }

    return self;
}

+ (void)loadRewardedVideoAdWithAdUnitID:(NSString *)adUnitID viewID:(NSString *)viewID withMediationSettings:(NSArray *)mediationSettings
{
    ADXRewardedVideo *sharedInstance = [[self class] sharedInstance];

    if (![adUnitID length]) {
        NSError *error = [NSError errorWithDomain:AdatrixRewardedVideoAdsSDKDomain code:ADXRewardedVideoAdErrorInvalidAdUnitID userInfo:nil];
        [sharedInstance.delegate rewardedVideoAdDidFailToLoadForAdUnitID:adUnitID error:error];
        return;
    }

    ADXRewardedVideoAdManager *adManager = sharedInstance.rewardedVideoAdManagers[adUnitID];

    if (!adManager) {
        adManager = [[ADXInstanceProvider sharedProvider] buildRewardedVideoAdManagerWithAdUnitID:adUnitID viewID:viewID delegate:sharedInstance];
        sharedInstance.rewardedVideoAdManagers[adUnitID] = adManager;
    }

    adManager.mediationSettings = mediationSettings;
    
    [adManager loadRewardedVideoAd];
}

+ (BOOL)hasAdAvailableForAdUnitID:(NSString *)adUnitID
{
    ADXRewardedVideo *sharedInstance = [[self class] sharedInstance];
    ADXRewardedVideoAdManager *adManager = sharedInstance.rewardedVideoAdManagers[adUnitID];

    return [adManager hasAdAvailable];
}

+ (void)presentRewardedVideoAdForAdUnitID:(NSString *)adUnitID fromViewController:(UIViewController *)viewController
{
    ADXRewardedVideo *sharedInstance = [[self class] sharedInstance];
    ADXRewardedVideoAdManager *adManager = sharedInstance.rewardedVideoAdManagers[adUnitID];

    if (!adManager) {
        ADXLogWarn(@"The rewarded video could not be shown: "
                  @"no ads have been loaded for adUnitID: %@", adUnitID);

        return;
    }

    if (!viewController) {
        ADXLogWarn(@"The rewarded video could not be shown: "
                  @"a nil view controller was passed to -presentRewardedVideoAdForAdUnitID:fromViewController:.");

        return;
    }

    if (![viewController.view.window isKeyWindow]) {
        ADXLogWarn(@"Attempting to present a rewarded video ad in non-key window. The ad may not render properly.");
    }

    [adManager presentRewardedVideoAdFromViewController:viewController];
}

#pragma mark - Private

+ (ADXRewardedVideo *)sharedInstance
{
    static dispatch_once_t once;

    dispatch_once(&once, ^{
        gSharedInstance = [[self alloc] init];
    });

    return gSharedInstance;
}

// This is private as we require the developer to initialize rewarded video through the Adatrix object.
+ (void)initializeWithDelegate:(id<ADXRewardedVideoDelegate>)delegate
{
    ADXRewardedVideo *sharedInstance = [[self class] sharedInstance];

    // Do not allow calls to initialize twice.
    if (sharedInstance.delegate) {
        ADXLogWarn(@"Attempting to initialize ADXRewardedVideo when it has already been initialized.");
    } else {
        sharedInstance.delegate = delegate;
    }
}

#pragma mark - ADXRewardedVideoAdManagerDelegate

- (void)rewardedVideoDidLoadForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdDidLoadForAdUnitID:)]) {
        [self.delegate rewardedVideoAdDidLoadForAdUnitID:manager.adUnitID];
    }
}

- (void)rewardedVideoDidFailToLoadForAdManager:(ADXRewardedVideoAdManager *)manager error:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdDidFailToLoadForAdUnitID:error:)]) {
        [self.delegate rewardedVideoAdDidFailToLoadForAdUnitID:manager.adUnitID error:error];
    }
}

- (void)rewardedVideoDidExpireForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdDidExpireForAdUnitID:)]) {
        [self.delegate rewardedVideoAdDidExpireForAdUnitID:manager.adUnitID];
    }
}

- (void)rewardedVideoDidFailToPlayForAdManager:(ADXRewardedVideoAdManager *)manager error:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdDidFailToPlayForAdUnitID:error:)]) {
        [self.delegate rewardedVideoAdDidFailToPlayForAdUnitID:manager.adUnitID error:error];
    }
}

- (void)rewardedVideoWillAppearForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdWillAppearForAdUnitID:)]) {
        [self.delegate rewardedVideoAdWillAppearForAdUnitID:manager.adUnitID];
    }
}

- (void)rewardedVideoDidAppearForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdDidAppearForAdUnitID:)]) {
        [self.delegate rewardedVideoAdDidAppearForAdUnitID:manager.adUnitID];
    }
}

- (void)rewardedVideoWillDisappearForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdWillDisappearForAdUnitID:)]) {
        [self.delegate rewardedVideoAdWillDisappearForAdUnitID:manager.adUnitID];
    }
}

- (void)rewardedVideoDidDisappearForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdDidDisappearForAdUnitID:)]) {
        [self.delegate rewardedVideoAdDidDisappearForAdUnitID:manager.adUnitID];
    }

    // Since multiple ad units may be attached to the same network, we should notify the custom events (which should then notify the application)
    // that their ads may not be available anymore since another ad unit might have "played" their ad. We go through and notify all ad managers
    // that are of the type of ad that is playing now.
    Class customEventClass = manager.customEventClass;
    
    for (id key in self.rewardedVideoAdManagers) {
        ADXRewardedVideoAdManager *adManager = self.rewardedVideoAdManagers[key];
        
        if (adManager != manager && adManager.customEventClass == customEventClass) {
            [adManager handleAdPlayedForCustomEventNetwork];
        }
    }
}

- (void)rewardedVideoDidReceiveTapEventForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdDidReceiveTapEventForAdUnitID:)]) {
        [self.delegate rewardedVideoAdDidReceiveTapEventForAdUnitID:manager.adUnitID];
    }
}

- (void)rewardedVideoWillLeaveApplicationForAdManager:(ADXRewardedVideoAdManager *)manager
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdWillLeaveApplicationForAdUnitID:)]) {
        [self.delegate rewardedVideoAdWillLeaveApplicationForAdUnitID:manager.adUnitID];
    }
}

- (void)rewardedVideoShouldRewardUserForAdManager:(ADXRewardedVideoAdManager *)manager reward:(ADXRewardedVideoReward *)reward
{
    if ([self.delegate respondsToSelector:@selector(rewardedVideoAdShouldRewardForAdUnitID:reward:)]) {
        [self.delegate rewardedVideoAdShouldRewardForAdUnitID:manager.adUnitID reward:reward];
    }
}

@end
