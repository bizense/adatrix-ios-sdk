//
//  ADXConstants.m
//  AdatrixSDK
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#import "ADXConstants.h"

CGSize const ADATRIX_BANNER_SIZE = { .width = 320.0f, .height = 50.0f };
CGSize const ADATRIX_MEDIUM_RECT_SIZE = { .width = 300.0f, .height = 250.0f };
CGSize const ADATRIX_LEADERBOARD_SIZE = { .width = 728.0f, .height = 90.0f };
CGSize const ADATRIX_WIDE_SKYSCRAPER_SIZE = { .width = 160.0f, .height = 600.0f };
