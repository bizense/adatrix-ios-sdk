//
//  ADXAdConversionTracker.m
//  Adatrix
//
//  Created by Andrew He on 2/4/11.
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXAdConversionTracker.h"
#import "ADXConstants.h"
#import "ADXGlobal.h"
#import "ADXLogging.h"
#import "ADXIdentityProvider.h"
#import "ADXCoreInstanceProvider.h"
#import "ADXAPIEndpoints.h"

#define ADATRIX_CONVERSION_DEFAULTS_KEY @"com.bizense.conversion"

@interface ADXAdConversionTracker ()

@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, assign) NSInteger statusCode;

- (NSURL *)URLForAppID:(NSString *)appID;

@end

@implementation ADXAdConversionTracker

@synthesize responseData = _responseData;
@synthesize statusCode = _statusCode;

+ (ADXAdConversionTracker *)sharedConversionTracker
{
    static ADXAdConversionTracker *sharedConversionTracker;

    @synchronized(self)
    {
        if (!sharedConversionTracker)
            sharedConversionTracker = [[ADXAdConversionTracker alloc] init];
        return sharedConversionTracker;
    }
}


- (void)reportApplicationOpenForApplicationID:(NSString *)appID
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:ADATRIX_CONVERSION_DEFAULTS_KEY]) {
        ADXLogInfo(@"Tracking conversion");
        NSMutableURLRequest *request = [[ADXCoreInstanceProvider sharedProvider] buildConfiguredURLRequestWithURL:[self URLForAppID:appID]];
        self.responseData = [NSMutableData data];
        [NSURLConnection connectionWithRequest:request delegate:self];
    }
}

#pragma mark - <NSURLConnectionDataDelegate>

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.statusCode = [(NSHTTPURLResponse *)response statusCode];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //NOOP
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.statusCode == 200 && [self.responseData length] > 0) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ADATRIX_CONVERSION_DEFAULTS_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark -
#pragma mark Internal

- (NSURL *)URLForAppID:(NSString *)appID
{
    NSString *path = [NSString stringWithFormat:@"%@?v=%@&udid=%@&zone=%@&av=%@",
                      [ADXAPIEndpoints baseURLStringWithPath:ADATRIX_API_PATH_CONVERSION testing:NO],
                      ADX_SERVER_VERSION,
                      [ADXIdentityProvider identifier],
                      appID,
                      [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                      ];

    return [NSURL URLWithString:path];
}
@end
