//
//  ADXAdView.m
//  Adatrix
//
//  Created by Nafis Jamal on 1/19/11.
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import "ADXAdView.h"
#import "ADXClosableView.h"
#import "ADXBannerAdManager.h"
#import "ADXInstanceProvider.h"
#import "ADXBannerAdManagerDelegate.h"
#import "ADXLogging.h"

@interface ADXAdView () <ADXBannerAdManagerDelegate>

@property (nonatomic, strong) ADXBannerAdManager *adManager;
@property (nonatomic, weak) UIView *adContentView;
@property (nonatomic, assign) CGSize originalSize;
@property (nonatomic, assign) ADXNativeAdOrientation allowedNativeAdOrientation;

@end

@implementation ADXAdView
@synthesize location = _location;
@synthesize adManager = _adManager;
@synthesize adUnitId = _adUnitId;
@synthesize viewId = _viewId;
@synthesize keywords = _keywords;
@synthesize delegate = _delegate;
@synthesize originalSize = _originalSize;
@synthesize ignoresAutorefresh = _ignoresAutorefresh;
@synthesize testing = _testing;
@synthesize adContentView = _adContentView;
@synthesize allowedNativeAdOrientation = _allowedNativeAdOrientation;

#pragma mark -
#pragma mark Lifecycle

- (id)initWithAdUnitId:(NSString *)adUnitId viewID:(NSString *)viewId size:(CGSize)size
{
    CGRect f = (CGRect){{0, 0}, size};
    if (self = [super initWithFrame:f])
    {
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
        self.originalSize = size;
        self.allowedNativeAdOrientation = ADXNativeAdOrientationAny;
        self.adUnitId = (adUnitId) ? adUnitId : DEFAULT_PUB_ID;
        self.viewId = (viewId) ? viewId : DEFAULT_VIEW_ID;
        self.adManager = [[ADXInstanceProvider sharedProvider] buildADXBannerAdManagerWithDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    self.adManager.delegate = nil;
}

#pragma mark -

- (void)setAdContentView:(UIView *)view
{
    [self.adContentView removeFromSuperview];
    _adContentView = view;
    [self addSubview:view];
}

- (BOOL)ignoresAutorefresh
{
    return _ignoresAutorefresh;
}

- (void)setIgnoresAutorefresh:(BOOL)ignoresAutorefresh
{
    if (_ignoresAutorefresh != ignoresAutorefresh) {
        _ignoresAutorefresh = ignoresAutorefresh;
    }

    if (_ignoresAutorefresh) {
        [self.adManager stopAutomaticallyRefreshingContents];
    } else {
        [self.adManager startAutomaticallyRefreshingContents];
    }
}

- (CGSize)adContentViewSize
{
    // ADXClosableView represents an MRAID ad.
    if (!self.adContentView || [self.adContentView isKindOfClass:[ADXClosableView class]]) {
        return self.originalSize;
    } else {
        return self.adContentView.bounds.size;
    }
}

- (void)rotateToOrientation:(UIInterfaceOrientation)newOrientation
{
    [self.adManager rotateToOrientation:newOrientation];
}

- (void)loadAd
{
    [self.adManager loadAd];
}

- (void)refreshAd
{
    [self loadAd];
}

- (void)forceRefreshAd
{
    [self.adManager forceRefreshAd];
}

- (void)stopAutomaticallyRefreshingContents
{
    [self.adManager stopAutomaticallyRefreshingContents];
}

- (void)startAutomaticallyRefreshingContents
{
    [self.adManager startAutomaticallyRefreshingContents];
}

- (void)lockNativeAdsToOrientation:(ADXNativeAdOrientation)orientation
{
    self.allowedNativeAdOrientation = orientation;
}

- (void)unlockNativeAdsOrientation
{
    self.allowedNativeAdOrientation = ADXNativeAdOrientationAny;
}

- (ADXNativeAdOrientation)allowedNativeAdsOrientation
{
    return self.allowedNativeAdOrientation;
}

#pragma mark - <ADXBannerAdManagerDelegate>

- (ADXAdView *)banner
{
    return self;
}

- (id<ADXAdViewDelegate>)bannerDelegate
{
    return self.delegate;
}

- (CGSize)containerSize
{
    return self.originalSize;
}

- (UIViewController *)viewControllerForPresentingModalView
{
    return [self.delegate viewControllerForPresentingModalView];
}

- (void)invalidateContentView
{
    [self setAdContentView:nil];
}

- (void)managerDidFailToLoadAd
{
    if ([self.delegate respondsToSelector:@selector(adViewDidFailToLoadAd:)]) {
        // make sure we are not released synchronously as objects owned by us
        // may do additional work after this callback
        [[ADXCoreInstanceProvider sharedProvider] keepObjectAliveForCurrentRunLoopIteration:self];

        [self.delegate adViewDidFailToLoadAd:self];
    }
}

- (void)managerDidLoadAd:(UIView *)ad
{
    [self setAdContentView:ad];
    if ([self.delegate respondsToSelector:@selector(adViewDidLoadAd:)]) {
        [self.delegate adViewDidLoadAd:self];
    }
}

- (void)userActionWillBegin
{
    if ([self.delegate respondsToSelector:@selector(willPresentModalViewForAd:)]) {
        [self.delegate willPresentModalViewForAd:self];
    }
}

- (void)userActionDidFinish
{
    if ([self.delegate respondsToSelector:@selector(didDismissModalViewForAd:)]) {
        [self.delegate didDismissModalViewForAd:self];
    }
}

- (void)userWillLeaveApplication
{
    if ([self.delegate respondsToSelector:@selector(willLeaveApplicationFromAd:)]) {
        [self.delegate willLeaveApplicationFromAd:self];
    }
}

# pragma mark - Deprecated Custom Events Mechanism

- (void)customEventDidLoadAd
{
    [self.adManager customEventDidLoadAd];
}

- (void)customEventDidFailToLoadAd
{
    [self.adManager customEventDidFailToLoadAd];
}

- (void)customEventActionWillBegin
{
    [self.adManager customEventActionWillBegin];
}

- (void)customEventActionDidEnd
{
    [self.adManager customEventActionDidEnd];
}

@end
