//
//  ADXConstants.h
//  Adatrix
//
//  Copyright 2015 Adatrix, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ADX_DEBUG_MODE               1

#define DEFAULT_PUB_ID              @"16/17/41"
#define DEFAULT_VIEW_ID             [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]
#define ADX_SERVER_VERSION           @"6"
#define ADX_BUNDLE_IDENTIFIER        @"com.bizense.adatrix"

#define ADX_SDK_VERSION              @"1.0"


// Sizing constants.
extern CGSize const ADATRIX_BANNER_SIZE;
extern CGSize const ADATRIX_MEDIUM_RECT_SIZE;
extern CGSize const ADATRIX_LEADERBOARD_SIZE;
extern CGSize const ADATRIX_WIDE_SKYSCRAPER_SIZE;

// Miscellaneous constants.
#define MINIMUM_REFRESH_INTERVAL            10.0
#define DEFAULT_BANNER_REFRESH_INTERVAL     60
#define BANNER_TIMEOUT_INTERVAL             10
#define INTERSTITIAL_TIMEOUT_INTERVAL       30
#define REWARDED_VIDEO_TIMEOUT_INTERVAL     30

// Feature Flags
#define SESSION_TRACKING_ENABLED            1
