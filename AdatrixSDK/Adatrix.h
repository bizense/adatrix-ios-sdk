//
//  Adatrix.h
//  Adatrix
//
//  Copyright (c) 2014 Adatrix. All rights reserved.
//

#if __has_include("ADXNativeAdSampleTableViewCell.h")
    #import "ADXNativeAdSampleTableViewCell.h"
#endif

#import "ADXAdConversionTracker.h"
#import "ADXAdView.h"
#import "ADXBannerCustomEvent.h"
#import "ADXBannerCustomEventDelegate.h"
#import "ADXConstants.h"
#import "ADXInterstitialAdController.h"
#import "ADXInterstitialCustomEvent.h"
#import "ADXInterstitialCustomEventDelegate.h"
#import "ADXNativeAd.h"
#import "ADXNativeAdAdapter.h"
#import "ADXNativeAdConstants.h"
#import "ADXNativeCustomEvent.h"
#import "ADXNativeCustomEventDelegate.h"
#import "ADXNativeAdError.h"
#import "ADXNativeAdRendering.h"
#import "ADXNativeAdRequest.h"
#import "ADXNativeAdRequestTargeting.h"
#import "ADXTableViewAdManager.h"
#import "ADXCollectionViewAdPlacer.h"
#import "ADXTableViewAdPlacer.h"
#import "ADXClientAdPositioning.h"
#import "ADXServerAdPositioning.h"
#import "ADXNativeAdDelegate.h"
#import "ADXMediationSettingsProtocol.h"
#import "ADXRewardedVideo.h"
#import "ADXRewardedVideoReward.h"
#import "ADXRewardedVideoCustomEvent.h"
#import "ADXRewardedVideoError.h"

// Import these frameworks for module support.
#import <AdSupport/AdSupport.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>
#import <StoreKit/StoreKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <UIKit/UIKit.h>

#define AdatrixKit [Adatrix sharedInstance]

@interface Adatrix : NSObject

/**
 * Returns the Adatrix singleton object.
 *
 * @return The Adatrix singleton object.
 */
+ (Adatrix *)sharedInstance;

/**
 * A Boolean value indicating whether the Adatrix SDK should use Core Location APIs to automatically
 * derive targeting information for location-based ads.
 *
 * When set to NO, the SDK will not attempt to determine device location. When set to YES, the
 * SDK will periodically try to listen for location updates in order to request location-based ads.
 * This only occurs if location services are enabled and the user has already authorized the use
 * of location services for the application. The default value is YES.
 *
 * @param enabled A Boolean value indicating whether the SDK should listen for location updates.
 */
@property (nonatomic, assign) BOOL locationUpdatesEnabled;

/** @name Rewarded Video */
/**
 * Initializes the rewarded video system.
 *
 * This method should only be called once. It should also be called prior to requesting any rewarded video ads.
 * Once the global mediation settings and delegate are set, they cannot be changed.
 *
 * @param globalMediationSettings Global configurations for all rewarded video ad networks your app supports.
 *
 * @param delegate The delegate that will receive all events related to rewarded video.
 */
- (void)initializeRewardedVideoWithGlobalMediationSettings:(NSArray *)globalMediationSettings delegate:(id<ADXRewardedVideoDelegate>)delegate;

/**
 * Retrieves the global mediation settings for a given class type.
*
 * @param aClass The type of mediation settings object you want to receive from the collection.
 */
- (id<ADXMediationSettingsProtocol>)globalMediationSettingsForClass:(Class)aClass;

- (void)start;
- (NSString *)version;
- (NSString *)bundleIdentifier;

@end
